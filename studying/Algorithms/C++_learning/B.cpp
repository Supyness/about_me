#include <vector>
#include <iostream>

long long Prefix_Summ(std::vector<int>& tree, int pos) {
  long long res = 0;
  int tmp = pos;
  while (tmp >= 0) {
    res += tree[tmp];
    tmp = (tmp & (tmp + 1)) - 1;
  }
  return res;
}

void Add_to_elem(std::vector<int>& tree, int val, int size, int index) {
  int tmp = index;
  while (tmp < size) {
    tree[tmp] += val;
    tmp |= (tmp + 1);
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n; std::cin >> n;
  std::vector<int> arr_ch(n, 0);
  std::vector<int> arr_nech(n, 0);
  for (int i = 0; i < n; ++i) std::cin >> arr_ch[i];
  for (int i = 1; i < n; i += 2) {
    arr_nech[i] = arr_ch[i];
    arr_ch[i] = 0;
  }
  std::vector<int> fenvik_ch(n, 0);
  std::vector<int> fenvik_nech(n, 0);
  for (int i = 0; i < n; ++i) {
    if (i % 2 == 0) {
      Add_to_elem(fenvik_ch, arr_ch[i], n, i);
    }
    else {
      Add_to_elem(fenvik_nech, arr_nech[i], n, i);
    }
  }
  int m; std::cin >> m;
  for (int i = 0; i < m; ++i) {
    int query; std::cin >> query;
    if (query == 0) {
      int pos, val; std::cin >> pos >> val;
      if ((pos - 1) % 2 == 0) {
        Add_to_elem(fenvik_ch, val - arr_ch[pos - 1], n, pos - 1);
        arr_ch[pos - 1] = val;
      }
      else {
        Add_to_elem(fenvik_nech, val - arr_nech[pos - 1], n, pos - 1);
        arr_nech[pos - 1] = val;
      }
    }
    else {
      int l, r; std::cin >> l >> r;
      if ((l - 1) % 2 == 0) std::cout << (Prefix_Summ(fenvik_ch, r - 1) - Prefix_Summ(fenvik_ch, l - 2)) - (Prefix_Summ(fenvik_nech, r - 1) - Prefix_Summ(fenvik_nech, l - 2)) << std::endl;
      else std::cout << (Prefix_Summ(fenvik_nech, r - 1) - Prefix_Summ(fenvik_nech, l - 2)) - (Prefix_Summ(fenvik_ch, r - 1) - Prefix_Summ(fenvik_ch, l - 2)) << std::endl;
    }
  }
}