#include <vector>
#include <string>
#include <iostream>

using std::vector;
using std::string;

class BigInt {
 private:
  vector<char> arr;
 public:
  bool is_negative = false;
  BigInt(vector<char> nums) {
    arr.clear();
    for (char i : nums) arr.push_back(i);
    if (arr[arr.size() - 1] == '-') is_negative = true;
    else is_negative = false;
  }
  BigInt(int number) {
    bool flag = true;
    if (number < 0) {
      flag = false;
      is_negative = true;
      number = -number;
    }
    else is_negative = false;
    arr.clear();
    while (number > 0) {
      arr.push_back((number % 10) + '0');
      number /= 10;
    }
    if (!flag) arr.push_back('-');
  }
  bool operator ==(BigInt& num) {
    if (this->arr.size() == num.arr.size()) {
      for (int i = 0; i < num.arr.size(); ++i) {
        if (this->arr[i] != num.arr[i]) return false;
      }
      return true;
    }
    return false;
  }
  bool operator !=(BigInt& num) {
    return !(num == *this);
  }
  bool operator >(BigInt& num) {
    if (this->is_negative && !num.is_negative) return false;
    else if (!this->is_negative && num.is_negative) return true;
    else if (this->is_negative && num.is_negative) {
      if (this->arr.size() > num.arr.size()) return false;
      else if (this->arr.size() < num.arr.size()) return true;
      else {
        for (int i = int(num.arr.size() - 1); i > -1; --i) {
          if (this->arr[i] > num.arr[i]) return false;
          else if (this->arr[i] < num.arr[i]) return true;
        }
      }
    }
    else {
      if (this->arr.size() < num.arr.size()) return false;
      else if (this->arr.size() > num.arr.size()) return true;
      else {
        for (int i = int(num.arr.size()) - 1; i > -1; --i) {
          if (this->arr[i] > num.arr[i]) return true;
          else if (this->arr[i] < num.arr[i]) return false;
        }
      }
    }
    return false;
  }
  bool operator < (BigInt& num) {
    return !((*this == num) || (*this > num));
  }
  bool operator <= (BigInt& num) {
    return ((*this < num) || (*this == num));
  }
  bool operator >= (BigInt& num) {
    return ((*this > num) || (*this == num));
  }
  string toString() {
    string s;
    for (int i = int(this->arr.size()) - 1; i > -1; --i) s += this->arr[i];
    return s;
  }
  vector<char> multiply_vector(vector<char>& num1, char t, int degree) {
    int cof = t - '0';
    int addible = 0;
    vector<char> result;
    for (int i = 0; i < degree; ++i) result.push_back('0');
    for (int i = 0; i < int(num1.size()); ++i) {
      int num = num1[i] - '0';
      if (num * cof + addible > 9) {
        result.push_back((num * cof + addible) % 10 + '0');
        addible = (num * cof + addible) / 10;
      }
      else {
        result.push_back((num * cof + addible) + '0');
        addible = 0;
      }
    }
    if (addible != 0) result.push_back(addible + '0');
    return result;
  }
  vector<char> sum_of_vectors(vector<char>& num1, vector<char>& num2) {
    vector<char> result((num1.size() > num2.size() ? num1.size() : num2.size()));
    int addible = 0;
    for (size_t i = 0; i < (num1.size() < num2.size() ? num1.size() : num2.size()); ++i) {
      int first_digit = num1[i] - '0';
      int second_digit = num2[i] - '0';
      if (first_digit + second_digit + addible > 9) {
        result[i] = (first_digit + second_digit + addible - 10) + '0';
        addible = 1;
      }
      else {
        result[i] = (first_digit + second_digit + addible) + '0';
        addible = 0;
      }
    }
    size_t i = (num1.size() < num2.size() ? num1.size() : num2.size());
    if (num1.size() < num2.size()) {
      while (i < num2.size()) {
        int second_digit = num2[i] - '0';
        if (second_digit + addible > 9) {
          result[i] = (second_digit + addible) + '0';
          addible = 1;
        }
        else {
          result[i] = (addible + second_digit) + '0';
          addible = 0;
        }
        ++i;
      }
    }
    else {
      while (i < num1.size()) {
        int first_digit = num1[i] - '0';
        if (first_digit + addible > 9) {
          result[i] = (first_digit + addible) + '0';
          addible = 1;
        }
        else {
          result[i] = (addible + first_digit) + '0';
          addible = 0;
        }
        ++i;
      }
    }
    if (addible != 0) result.push_back(addible + '0');
    return result;
  }
  BigInt& operator+=(BigInt& num2) {
    vector<char> result = sum_of_vectors(this->arr, num2.arr);
    this->arr = result;
    return *this;
  }
  BigInt& operator-=(BigInt& num2) {
    bool flag = true;
    if (*this < num2) {
      flag = false;
      std::swap(*this, num2);
    }
    vector<char> result((this->arr.size() > num2.arr.size() ? this->arr.size() : num2.arr.size()));
    int addible = 0;
    for (size_t i = 0; i < (this->arr.size() < num2.arr.size() ? this->arr.size() : num2.arr.size()); ++i) {
      int first_digit = this->arr[i] - '0';
      int second_digit = num2.arr[i] - '0';
      if (first_digit - second_digit + addible < 0) {
        result[i] = (first_digit - second_digit + addible + 10) + '0';
        addible = -1;
      }
      else {
        result[i] = (first_digit - second_digit + addible) + '0';
        addible = 0;
      }
    }
    size_t i = (this->arr.size() < num2.arr.size() ? this->arr.size() : num2.arr.size());
    if (this->arr.size() < num2.arr.size()) {
      while (i < num2.arr.size()) {
        int second_digit = num2.arr[i] - '0';
        if (second_digit + addible > 9) {
          result[i] = (second_digit + addible) + '0';
          addible = -1;
        }
        else {
          result[i] = (addible + second_digit) + '0';
          addible = 0;
        }
        ++i;
      }
    }
    else {
      while (i < this->arr.size()) {
        int first_digit = this->arr[i] - '0';
        if (first_digit + addible > 9) {
          result[i] = (first_digit + addible) + '0';
          addible = -1;
        }
        else {
          result[i] = (addible + first_digit) + '0';
          addible = 0;
        }
        ++i;
      }
    }
    i = result.size() - 1;
    while (i > 0 && result[i] == '0') {
      result.pop_back();
      --i;
    }
    if (result.empty()) result.push_back('0');
    if (!flag) {
      std::swap(*this, num2);
    }
    this->arr = result;
    if (!flag) {
      *this = -*this;
      return *this;
    };
    return *this;
  }
  BigInt& operator*= (BigInt& num) {
    vector<char> result;
    if (num.arr.size() < this->arr.size()) {
      for (int i = 0; i < int(num.arr.size()); ++i) {
        vector<char> tmp = multiply_vector(this->arr, num.arr[i], i);
        result = sum_of_vectors(tmp, result);
      }
      this->arr = result;
    }
    else {
      for (int i = 0; i < int(this->arr.size()); ++i) {
        vector<char> tmp = multiply_vector(num.arr, this->arr[i], i);
        result = sum_of_vectors(tmp, result);
      }
      this->arr = result;
    }
    return *this;
  }
  BigInt& operator ++() {
    BigInt tmp = BigInt(1);
    *this += tmp;
    return *this;
  }
  BigInt& operator --() {
    BigInt tmp = BigInt(1);
    *this -= tmp;
    return *this;
  }
  BigInt operator --(int k) {
    BigInt tmp = BigInt(this->arr);
    --(*this);
    return tmp;
  }
  BigInt operator ++(int k) {
    BigInt tmp = BigInt(this->arr);
    ++(*this);
    return tmp;
  }

  friend std::istream& operator >> (std::istream& in, BigInt &num);
  friend std::ostream& operator << (std::ostream& out, BigInt &num);
  friend BigInt operator-(BigInt& num);
};

std::istream& operator >> (std::istream& in, BigInt &num) {
  char t;
  num.arr.clear();
  while (in.get(t) && !std::isspace(t) && !std::iscntrl(t)) {
    num.arr.push_back(t);
  }
  if (num.arr[0] == '-') num.is_negative = true;
  else num.is_negative = false;
  std::reverse(num.arr.begin(), num.arr.end());
  return in;
}
std::ostream& operator << (std::ostream& out, BigInt &num) {
  for (int i = int(num.arr.size() - 1); i > -1; --i) {
    out << num.arr[i];
  }
  return out;
}

BigInt operator-(BigInt& num) {
  BigInt tmp = num;
  if (tmp.is_negative) {
    tmp.is_negative = false;
    tmp.arr.pop_back();
  }
  else {
    tmp.is_negative = true;
    tmp.arr.push_back('-');
  }
  return tmp;
}

BigInt operator+(BigInt& num1, BigInt& num2) {
  if (num1.is_negative && num2.is_negative) {
    BigInt tmp = -num1;
//    num1 = -num1;
    BigInt tmp1 = -num2;
    tmp += tmp1;
//    num2 = -num2;
    return -tmp;
  }
  else if (num1.is_negative && !num2.is_negative){
    BigInt tmp = num2;
    BigInt tmp1 = -num1;
    tmp -= tmp1;
//    num1 = -num1;
    return tmp;
  }
  else if (!num1.is_negative && num2.is_negative) {
    BigInt tmp = num1;
    BigInt tmp1 = -num2;
    tmp -= tmp1;
//    num2 = -num2;
    return tmp;
  }
  else {
    BigInt tmp = num1;
    tmp += num2;
    return tmp;
  }
}

BigInt operator*(BigInt& num1, BigInt& num2) {
  if (num1.is_negative && num2.is_negative) {
    BigInt tmp = -num1;
//    num1 = -num1;
    BigInt tmp1 = -num2;
    tmp *= tmp1;
//    num2 = -num2;
    return tmp;
  }
  else if (num1.is_negative && !num2.is_negative){
    BigInt tmp = num2;
    BigInt tmp1 = -num1;
    tmp *= tmp1;
//    num1 = -num1;
    return -tmp;
  }
  else if (!num1.is_negative && num2.is_negative) {
    BigInt tmp = num1;
    BigInt tmp1 = -num2;
    tmp *= tmp1;
//    num2 = -num2;
    return -tmp;
  }
  else {
    BigInt tmp = num1;
    tmp *= num2;
    return tmp;
  }
}

BigInt operator-(BigInt& num1, BigInt& num2) {
  if (num1.is_negative && num2.is_negative) {
    BigInt tmp = -num2;
    BigInt tmp1 = -num1;
    tmp -= tmp1;
//    num1 = -num1;
//    num2 = -num2;
    return tmp;
  }
  else if (num1.is_negative && !num2.is_negative) {
    BigInt tmp = -num1;
//    num1 = -num1;
    BigInt tmp1 = tmp + num2;
    tmp = -tmp1;
    return tmp;
  }
  else if (!num1.is_negative && num2.is_negative) {
    BigInt tmp = num1;
    BigInt tmp1 = -num2;
    tmp += tmp1;
//    num2 = -num2;
    return tmp;
  }
  else {
    BigInt tmp = num1;
    tmp -= num2;
    return tmp;
  }
}

int main() {
  BigInt num1, num2;
  std::cin >> num1 >> num2;
  BigInt ss = 1;
  BigInt num3 = num1 - num2, num4 = num1 + num2;
  BigInt num5 = num1 * num2;
  num3 = num1--;
  num4 = num2++;
  std::cout << num3 << ' ' << num4 << ' ' << num1 << ' ' << num2;
}