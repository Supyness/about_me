#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::string;

struct Heap;
struct Node;

struct Node {
  long long value;
  Node *parent = nullptr;
  vector<Node *> childs = {};
  size_t index_of_query;
  Heap *heap;
  size_t node_level = 0;
};

struct Heap {
  vector<Node *> roots = {};
};

void Merge_trees(Node* first_tree_index, Node* second_tree_index) {
  if (first_tree_index->value > second_tree_index->value) {
    Node* tmp = first_tree_index;
    first_tree_index = second_tree_index;
    second_tree_index = tmp;
  }
  if (first_tree_index->value <= second_tree_index->value) {
    second_tree_index->heap->roots[second_tree_index->node_level] = nullptr;
    first_tree_index->childs.push_back(second_tree_index);
    second_tree_index->parent = first_tree_index;
    ++first_tree_index->node_level;
    if (first_tree_index->heap->roots.size() == first_tree_index->node_level) {
      first_tree_index->heap->roots[first_tree_index->node_level - 1] = nullptr;
      first_tree_index->heap->roots.push_back(first_tree_index);
    }
    else if (first_tree_index->heap->roots[first_tree_index->node_level] == nullptr) {
      first_tree_index->heap->roots[first_tree_index->node_level] = first_tree_index;
      first_tree_index->heap->roots[first_tree_index->node_level - 1] = nullptr;
    }

    else {
      while (first_tree_index->node_level + 1 <= first_tree_index->heap->roots.size() && first_tree_index->heap->roots[first_tree_index->node_level] != nullptr && first_tree_index->heap->roots[first_tree_index->node_level] != first_tree_index) {
        Merge_trees(first_tree_index, first_tree_index->heap->roots[first_tree_index->node_level]);
      }
      /*if (first_tree_index->node_level + 1 == first_tree_index->heap->roots.size()) {
        first_tree_index->heap->roots.push_back(first_tree_index);
        first_tree_index->heap->roots[first_tree_index->node_level] = nullptr;
      }*/
    }
  }
}

void MakeHeap(long long value, size_t query_number, Heap *which_heap, vector<Node*>& node_indexes) {
  Node *node = new Node;
  node->value = value;
  node->index_of_query = query_number;
  node->heap = which_heap;
  if (which_heap->roots.size() != 0 && which_heap->roots[0] != nullptr) Merge_trees(which_heap->roots[0], node);
  else {
    if (which_heap->roots.size() == 0) which_heap->roots.push_back(node);
    else which_heap->roots[0] = node;
  }
  node_indexes[query_number] = node;
}

void Merge_heaps(Heap* first_heap_index, Heap* second_heap_index) {
  for (int i = 0; i < second_heap_index->roots.size(); ++i) {
    if (i + 1 > first_heap_index->roots.size()) {
      first_heap_index->roots.push_back(second_heap_index->roots[i]);
      if (second_heap_index->roots[i] != nullptr) second_heap_index->roots[i]->heap = first_heap_index;
    }
    else if (second_heap_index->roots[i] != nullptr && first_heap_index->roots[i] != nullptr) {
      second_heap_index->roots[i]->heap = first_heap_index;
      Merge_trees(first_heap_index->roots[i], second_heap_index->roots[i]);
    }
    else if (second_heap_index->roots[i] != nullptr && first_heap_index->roots[i] == nullptr) {
      first_heap_index->roots[i] = second_heap_index->roots[i];
      second_heap_index->roots[i]->heap = first_heap_index;
    }
  }
}

void extractMin(Heap* heap, vector<Node*>& node_indexes, Node* min_root = nullptr) {
  if (min_root == nullptr) {
    Node *min_root1;
    long long min_root_val = LLONG_MAX;
    long long index = LLONG_MAX;
    for (Node *i: heap->roots) {
      if (i != nullptr) {
        if (min_root_val > i->value) {
          min_root_val = i->value;
          min_root1 = i;
          index = i->index_of_query;
        }
        if (min_root_val == i->value && index > i->index_of_query) {
          min_root1 = i;
          index = i->index_of_query;
        }
      }
    }
    min_root = min_root1;
  }
  for (Node* i : min_root->childs) {
    i->heap = min_root->heap;
    i->parent = nullptr;
  }
  if (min_root->node_level + 1 >= min_root->heap->roots.size()) min_root->heap->roots.resize(min_root->node_level);
  else min_root->heap->roots[min_root->node_level] = nullptr;
  node_indexes[min_root->index_of_query] = nullptr;
  for (Node* i : min_root->childs)
  {
    if (i->heap->roots[i->node_level] == nullptr) {
      i->heap->roots[i->node_level] = i;
    }
    else {
      Merge_trees(i->heap->roots[i->node_level], i);
    }
  }
}

void sift_up(Node* elem) {
  while (elem->parent != nullptr) {
    if (elem->value < elem->parent->value) {
      vector<Node*> elemtmp;
      for (Node* i : elem->parent->childs) {
        if (i != elem) {
          elemtmp.push_back(i);
        }
      }
      std::swap(elem->node_level, elem->parent->node_level);
      elem->parent->childs = elem->childs;
      elem->childs = elemtmp;
      elem->childs.push_back(elem->parent);
      elem->parent = elem->parent->parent;
      for (Node* i : elem->childs) {
        i->parent = elem;
      }
    }
    else {
      break;
    }
  }
  if (elem->parent == nullptr) {
    while (elem->heap->roots.size() <= elem->node_level) {
      elem->heap->roots.push_back(nullptr);
    }
    elem->heap->roots[elem->node_level] = elem;
  }
}

void delete_element(Node* elem, vector<Node*>& node_indexes) {
  if (elem != nullptr) {
    elem->value = -LLONG_MAX;
    sift_up(elem);
    extractMin(elem->heap, node_indexes, elem);
  }
}

void min_element(Heap* main_heap) {
  long long min1 = LLONG_MAX;
  for (Node* i : main_heap->roots) {
    if (i != nullptr) min1 = std::min(min1, i->value);
  }
  std::cout << min1 << '\n';
}

void ask(int m, vector<Heap*>& all_heaps, vector<Node*>& node_indexes) {
  int query;
  size_t counter = 0;
  for (int i = 0; i < m; ++i) {
    cin >> query;
    if (query == 0) {
      int heap_index, elem;
      cin >> heap_index >> elem;
      MakeHeap(elem, counter, all_heaps[heap_index - 1], node_indexes);
      ++counter;
    }
    if (query == 1) {
      int heap_index1, heap_index2;
      cin >> heap_index2 >> heap_index1;
      Merge_heaps(all_heaps[heap_index1 - 1], all_heaps[heap_index2 - 1]);
      all_heaps[heap_index2 - 1]->roots = {};
    }
    if (query == 2) {
      int node_index;
      cin >> node_index;
      delete_element(node_indexes[node_index - 1], node_indexes);
    }
    if (query == 3) {
      int index, value;
      cin >> index >> value;
      --index;
      if (value <= node_indexes[index]->value) {
        node_indexes[index]->value = value;
        sift_up(node_indexes[index]);
      }
      else {
        Heap* heap = node_indexes[index]->heap;
        size_t query_number = node_indexes[index]->index_of_query;
        delete_element(node_indexes[index], node_indexes);
        MakeHeap(value, query_number, heap, node_indexes);
      }
    }
    if (query == 4) {
      int heap_index;
      cin >> heap_index;
      min_element(all_heaps[heap_index - 1]);
    }
    if (query == 5) {
      int heap_index; cin >> heap_index;
      extractMin(all_heaps[heap_index - 1], node_indexes);
    }
  }
}

int main() {
  int n, m;
  cin >> n >> m;
  vector<Heap*> all_heaps;
  vector<Node*> node_indexes(1000000, nullptr);
  for (int i = 0; i < n; i++) {
    Heap* heap = new Heap;
    all_heaps.push_back(heap);
  }
  ask(m, all_heaps, node_indexes);
}