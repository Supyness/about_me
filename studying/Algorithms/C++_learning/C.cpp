#include <iostream>
#include <vector>

using std::vector;

void build_tree(vector<int>& main_array, vector<vector<int>>& tree, int ver, int l, int r) {
  if (l == r) {
    tree[ver][0] = main_array[l];
    tree[ver][1] = l;
    tree[ver][2] = l;
  }
  else {
    int mid = (l + r) >> 1;
    build_tree(main_array, tree, ver << 1, l, mid);
    build_tree(main_array, tree, (ver << 1) + 1, mid + 1, r);
    tree[ver][0] = std::max(tree[ver << 1][0], tree[(ver << 1) + 1][0]);
    tree[ver][1] = tree[ver << 1][1];
    tree[ver][2] = tree[(ver << 1) + 1][2];
  }
}
int Maximum(vector<vector<int>>& tree, int v, int treel, int treer, int l, int r) {
  if (l > r) return 0;
  if (l == treel && r == treer) return tree[v][0];
  int mid = (treel + treer) >> 1;
  return std::max(Maximum(tree, v << 1, treel, mid, l, std::min(r, mid)), Maximum(tree, (v << 1) + 1, mid + 1, treer, std::max(l, mid + 1), r));
}

void Update(vector<vector<int>>& tree, int v, int treel, int treer, int position, int val) {
  if (treel == treer) {
    tree[v][0] = val;
  }
  else {
    int mid = (treel + treer) >> 1;
    if (position <= mid) Update(tree, v << 1, treel, mid, position, val);
    else Update(tree, (v << 1) + 1, mid + 1, treer, position, val);
    tree[v][0] = std::max(tree[v << 1][0], tree[(v << 1) + 1][0]);
  }
}

int Get_Ver(int v, int treel, int treer, int pos) {
  if (treel == treer) {
    return v;
  }
  int mid = (treer + treel) / 2;
  if (pos > mid) Get_Ver(2 * v + 1, mid + 1, treer, pos);
  else Get_Ver(2 * v, treel, mid, pos);
}

vector<int> Find_Max_Root(vector<vector<int>>& tree, int ver, int l, int r, int maximum) {
  if (ver == 0) return {-1, l, r};
  else if (tree[ver][0] >= maximum && r == l) return {ver, r, l};
  else if (ver % 2 == 0 && tree[ver + 1][0] >= maximum) return {ver + 1, tree[ver + 1][1], tree[ver + 1][2]};
  else {
    if (ver % 2 == 0) return Find_Max_Root(tree, ver / 2, tree[ver / 2][1], tree[ver / 2][2], maximum);
    if (ver % 2 == 1) return Find_Max_Root(tree, ver / 2, tree[ver / 2][1], tree[ver / 2][2], maximum);
  }
}

int Find_Min_Higher(vector<vector<int>>& tree, int ver, int l, int r, int val) {
  if (l == r) return l;
  int mid = (l + r) / 2;
  if (tree[2 * ver][0] >= val) Find_Min_Higher(tree, 2 * ver, l, mid, val);
  else Find_Min_Higher(tree, 2 * ver + 1, mid + 1, r, val);
}

void Min_Right(vector<vector<int>>& tree, int v, int treel, int treer, int pos, int val) {
  int ver = Get_Ver(v, treel, treer, pos);
  vector<int> ver_max = Find_Max_Root(tree, ver, pos, pos, val);
  if (ver_max[0] == -1) std::cout << -1 << std::endl;
  else {
    std::cout << Find_Min_Higher(tree, ver_max[0], ver_max[1], ver_max[2], val) + 1 << "\n";
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  vector<int> arr(n);
  for (int i = 0; i < n; ++i) std::cin >> arr[i];
  vector<vector<int>> tree(4 * n, {0, 0, 0});
  build_tree(arr, tree, 1, 0, n - 1);
  for (int i = 0; i < m; ++i) {
    int k, j, z; std::cin >> k >> j >> z;
    if (k == 0) {
      Update(tree, 1, 0, n - 1, j - 1, z);
    }
    else {
      Min_Right(tree, 1, 0, n - 1, j - 1, z);
    }
  }
}