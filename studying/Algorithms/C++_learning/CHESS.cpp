#include <iostream>
#include <vector>
#include <string>

int game_coonditions_amount = 8; //количество состояний игры (шах, мат, ничья и т.д.)
int size_of_board = 8; //размер доски
std::vector<std::string> all_conditions = {"NOT DEFINE", "INALID", "COMMON", "CHECK", "CHECKMATE", "DRAW", "STALEMATE", "REPEATED"}; //вектор состояний
std::vector<std::string> colors = {"WHITE", "BLACK", "EMPTY"}; //вектор цветов фигур (EMPTY добавил для единообразия, чтобы пустую клетку определять просто пустым цветом, это удобно)

class Color { //цвета фигур
 public:
  Color(const int number) : current_color(colors[number]) {};
 private:
  std::string current_color = "WHITE";
};

class position_type { //контролирует состояние после текущего хода
 public:
  position_type(int number) : current_condition(all_conditions[number]) {};
  std::string return_condition() {}; //возвращает текущее состояние доски
 private:
  std::string current_condition = "NOT_DEFINE"; //текущее состояние доски
};

class Piece {
 public:
  Piece(const Color& color) : color(color) {}; //еще один прикол с пустым цветом в том, что не надо хранить поле if_empty а надо лишь проверить color.current_color == EMPTY
  /*Move if_can_move();*/   //Move А вот и первая пизда с твоими определниями классов, как я тебе сделаю так, чтобы для Square, Piece, Move они были определены друг для друга, если они внутри себя используют друг друга)
  bool is_empty() {
    return if_empty;//или же return color.current_color == "EMPTY")))
  }
 private:
  Color color; //цвет фигуры
  bool if_empty = false; //стоит ли фигуры и по умолчанию я бы сделал yes чтобы потом не мучаться с определением для пустых клеток ибо их больше
};

class Square { //клетка доски
 public:
  Square(const int row, const int col) : row(row), col(col) {}; //тут надо как-то посмотреть на текущую доску и передать ссылку фигуры с клетки в piece и ее цвет
  Square& operator=(const Square& cell) {};
  bool operator==(const Square& cell) {
    return (col == cell.col && row == cell.row);
  }
 private:
  Piece* piece; //определяет фигуру стоящуюю на клетке
  int row, col; //определяют координаты клетки
  Color color = Color(2); //цвет фигуры на клетке (изначально пустой цвет пусть будет)
};

class Move { //контролирует ход
 public:
  void make_move(std::vector<std::vector<Square*>>& board); //сделать ход по нынешней доске
 private:
  bool is_correct = true;
};

class Position { //контролирует текущее состояние доски
 public:
  Position(); //конструктор default
  Position(std::vector<std::vector<Square*>>& board); //конструктор от текущего состояния доски
  ~Position();
  void set_start_position(); //с какаого вида доски начинать?
  position_type what_position_type(); //вернуть состояние текущей доски
  void define_position_type();//это убрать состояние доски или че?
  Color whose_move();//Чей сейчас ход
  std::vector<std::vector<Square*>> what_board(); //вернуть текущую доску
  Move is_move_correct(const Move& mv);//правильный ли текущий ход
  void move(const Move& mv);//Если ход правильный, то сделать его
  //остальное можно проверять в функции what_position_type
 private:
  std::vector<std::vector<Square*>> board = std::vector<std::vector<Square*>>(size_of_board, std::vector<Square*>(size_of_board, nullptr)); //(8, std::vector<Square*>(8, nullptr)); //Доска
  Color move_color; //цвет перемещаемой фигуры
  position_type game_state = position_type(0); //изначально задаем game_state как NOT_DEFINE
  static std::vector<Piece> black_pieces; //все фигуры черных
  static std::vector<Piece> white_pieces; //все фигуры обелых
  Square* black_king_square; //где стоит черный корооль
  Square* white_king_square; //где стоит белый король
};

class Response {
 private:
  Color figure_color;
  position_type current_condition; //текущее состояние доски
  std::vector<std::vector<Square*>> board = std::vector<std::vector<Square*>>(size_of_board, std::vector<Square*>(size_of_board,
                                                                                                                  nullptr));
};

class Game { //контролирует всю игру
 public:
  Game();//default конструктор
  ~Game();
  void create_game();//начинает игру со стартовым расположением фигур
  Response move(int x1, int x2, int x3, int x4 /*да уж с вашей реализацией токо 4 координаты, к сожалению*/); //если у нас есть такая же функция в position, то не вижу смысла в Game, которая не
  /*отвечает за сам ход добавлять такую же функцию*/

  /*опять две странные функции, зачем они нужны, если такие же есть в Position, который и отвечает за ход, в то время как Game нет*/

  Position* create_new_position(const Move& mv); //переделывает доску, опираясь на последний сделанный ход
  void check_move_repeating(); //что-то непонятное
 private:
  Position* current_position; //текущее состояние доски
  std::vector<Position*> position_history; //история всех досок
};

int main() {
  std::vector<int> arr(5);
}