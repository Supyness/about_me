#pragma optimize( "", off )
#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;

bool Sortt(std::pair<int, int> a, std::pair<int, int> b) {
  return a.second < b.second;
}

int Get_Ver(int v, int treel, int treer, int pos) {
  if (treel == treer) {
    return v;
  }
  int mid = (treer + treel) / 2;
  if (pos > mid) Get_Ver(2 * v + 1, mid + 1, treer, pos);
  else Get_Ver(2 * v, treel, mid, pos);
}

void build_tree(vector<int>& main_array, vector<int>& tree, int ver, int l, int r) {
  if (l == r) {
    tree[ver] = main_array[l];
  }
  else {
    int mid = (l + r) >> 1;
    build_tree(main_array, tree, ver << 1, l, mid);
    build_tree(main_array, tree, (ver << 1) + 1, mid + 1, r);
    tree[ver] = tree[ver << 1] + tree[(ver << 1) + 1];
  }
}
unsigned long long Summ(vector<int>& tree, int v, int treel, int treer, int l, int r) {
  if (l > r) return 0;
  if (l == treel && r == treer) return tree[v];
  int mid = (treel + treer) >> 1;
  return Summ(tree, v << 1, treel, mid, l, std::min(r, mid)) + Summ(tree, (v << 1) + 1, mid + 1, treer, std::max(l, mid + 1), r);
}

void Update(vector<int>& tree, int v, int treel, int treer, int position, int val) {
  if (treel == treer) {
    tree[v] = val;
  }
  else {
    int mid = (treel + treer) >> 1;
    if (position <= mid) Update(tree, v << 1, treel, mid, position, val);
    else Update(tree, (v << 1) + 1, mid + 1, treer, position, val);
    tree[v] = tree[v << 1] + tree[(v << 1) + 1];
  }
}

unsigned long long answer(vector<int>& tree, int n, vector<std::pair<int, int>>& sections) {
  unsigned long long counter = 0;
  for (int i = 0; i < n; ++i) {
    int position = Get_Ver(1, 0, n - 1, sections[i].second);
    counter += Summ(tree, 1, 0, n - 1, 0, sections[i].second) - 1;
    Update(tree, 1, 0, n - 1, sections[i].second, tree[position] - 1);
  }
  return counter;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n; std::cin >> n;
  vector<std::pair<int, int>> arr(n);
  vector<int> ends(n, 0);
  for (int i = 0; i < n; ++i) {
    int v1, v2; std::cin >> v1 >> v2;
    arr[i] = {v1, v2};
  }
  std::stable_sort(arr.begin(), arr.end(), Sortt);
  int number = 0;
  vector<std::pair<int, int>> arr1(n);
  arr1[0].first = arr[0].first;
  arr1[0].second = 0;
  for (int i = 1; i < n; ++i) {
    if (arr[i].second > arr[i - 1].second) {
      ++number;
    }
    arr1[i].first = arr[i].first;
    arr1[i].second = number;
  }
  std::sort(arr1.begin(), arr1.end());
  for (int i = 0; i < n; ++i) {
    ends[arr1[i].second]++;
  }
  unsigned long long kol_povt = 0;
  unsigned long long kol = 0;
  for (int i = 0; i < n - 1; ++i) {
    if (arr1[i] == arr1[i + 1]) kol++;
    else {
      kol_povt += kol * (kol + 1) / 2;
      kol = 0;
    }
  }
  kol_povt += kol * (kol + 1) / 2;
  int i = 0;
  int ind = 0;
  while (i < arr1.size() - 1) {
    if (arr1[i].first == arr1[i + 1].first && arr1[i].second <= arr1[i + 1].second) ++i;
    else {
      int t = 0;
      for (int j = ind; j < (i + ind) / 2 + (i + ind) % 2; ++j) {
        std::swap(arr1[j], arr1[i - t]);
        ++t;
      }
      ++i;
      ind = i;
    }
  }
  if (i - ind >= 1) {
    int t = 0;
    for (int j = ind; j < (i + ind) / 2 + (i + ind) % 2; ++j) {
      std::swap(arr1[j], arr1[i - t]);
      ++t;
    }
  }
  /*if ((arr1[arr1.size() - 1].first == arr1[arr1.size() - 2].first) && (arr1[arr1.size() - 2].second <= arr1[arr1.size() - 1].second)) {
    std::swap(arr1[arr1.size() - 1], arr1[arr1.size() - 2]);
  }*/
  vector<int> tree(4 * ends.size());
  build_tree(ends, tree, 1, 0, ends.size() - 1);
  std::cout << answer(tree, n, arr1) - kol_povt;
}