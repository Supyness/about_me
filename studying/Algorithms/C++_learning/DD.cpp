#include <iostream>
#include <vector>

using std::vector;

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n; std::cin >> n;
  vector<std::pair<int, int>> arr(n);
  vector<int> ends(n, 0);
  for (int i = 0; i < n; ++i) {
    int v1, v2; std::cin >> v1 >> v2;
    arr[i] = {v1, v2};
  }
  int counter = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if ((arr[i].first <= arr[j].first && arr[i].second > arr[j].second) || (arr[i].first < arr[j].first && arr[i].second >= arr[j].second)) ++counter;
      if ((arr[i].first >= arr[j].first && arr[i].second < arr[j].second) || (arr[i].first > arr[j].first && arr[i].second <= arr[j].second)) ++counter;
    }
  }
  std::cout << counter;
}
