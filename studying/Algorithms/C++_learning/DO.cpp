#include <iostream>
#include <vector>

using std::vector;

void build_tree(vector<int>& main_array, vector<int>& tree, int ver, int l, int r) {
  if (l == r) {
    tree[ver] = main_array[l];
  }
  else {
    int mid = (l + r) >> 1;
    build_tree(main_array, tree, ver << 1, l, mid);
    build_tree(main_array, tree, (ver << 1) + 1, mid + 1, r);
    tree[ver] = tree[ver << 1] + tree[(ver << 1) + 1];
  }
}
int Summ(vector<int>& tree, int v, int treel, int treer, int l, int r) {
  if (l > r) return 0;
  if (l == treel && r == treer) return tree[v];
  int mid = (treel + treer) >> 1;
  return Summ(tree, v << 1, treel, mid, l, std::min(r, mid)) + Summ(tree, (v << 1) + 1, mid + 1, treer, std::max(l, mid + 1), r);
}

void Update(vector<int>& tree, int v, int treel, int treer, int position, int val) {
  if (treel == treer) {
    tree[v] = val;
  }
  else {
    int mid = (treel + treer) >> 1;
    if (position <= mid) Update(tree, v << 1, treel, mid, position, val);
    else Update(tree, (v << 1) + 1, mid + 1, treer, position, val);
    tree[v] = tree[v << 1] + tree[(v << 1) + 1];
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n; std::cin >> n;
  vector<int> arr_nech(n + 1, 0);
  vector<int> tree_nech(n << 2, 0);
  vector<int> arr_ch(n + 1, 0);
  vector<int> tree_ch(n << 2, 0);
  for (int i = 0; i < n; ++i) std::cin >> arr_ch[i];
  for (int i = 1; i < n; i += 2) {
    arr_nech[i] = arr_ch[i];
    arr_ch[i] = 0;
  }
  build_tree(arr_ch, tree_ch, 1, 0, n - 1);
  build_tree(arr_nech, tree_nech, 1, 0, n - 1);
  int k; std::cin >> k;
  for (int i = 0; i < k; ++i) {
    int num, x, y; std::cin >> num >> x >> y;
    if (num == 0) {
      if (x % 2 == 0) {
        Update(tree_nech, 1, 0, n - 1, x - 1, y);
      }
      else Update(tree_ch, 1, 0, n - 1, x - 1, y);
    }
    if (num == 1) {
      if (x % 2 == 0) {
        std::cout << Summ(tree_nech, 1, 0, n - 1, x - 1, y - 1) - Summ(tree_ch, 1, 0, n - 1, x - 1, y - 1) << std::endl;
      }
      else {
        std::cout << Summ(tree_ch, 1, 0, n - 1, x - 1, y - 1) - Summ(tree_nech, 1, 0, n - 1, x - 1, y - 1) << std::endl;
      }
    }
  }
}