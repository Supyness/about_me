#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

int Prefix_Sum(int k, vector<int>& tree) {
  int res = 0;
  while (k >= 0) {
    res += tree[k];
    k = (k & (k + 1)) - 1;
  }
  return res;
}

void Add_To_Elem(int v, int val, int size, vector<int>& tree) {
  while (v < size) {
    tree[v] += val;
    v = v | (v + 1);
  }
}

void Modify_Elem(int v, int val, int size, vector<int>& tree) {
  int number = Prefix_Sum(v, tree) - Prefix_Sum(v - 1, tree);
  Add_To_Elem(v, val - number, size, tree);
}

int main() {
  int n, m; cin >> n >> m;
  vector<int> fenvik_tree(n, 0);
  for (int i = 0; i < n; ++i) {
    int value; cin >> value;
    Modify_Elem(i, value, n, fenvik_tree);
  }
  for (int i = 0; i < m; ++i) {
    int l, r; cin >> l >> r;
    cout << Prefix_Sum(r - 1, fenvik_tree) - Prefix_Sum(l - 2, fenvik_tree);
  }
}