#pragma optimize( "", off )
#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

long long Prefix_Sum(long long x, long long y, long long z, vector<vector<vector<long long>>>& tree) {
  long long res = 0;
  long long p = x;
  while (p >= 0) {
    long long t = y;
    while (t >= 0) {
      long long k = z;
      while (k >= 0) {
        res += tree[p][t][k];
        k = (k & (k + 1)) - 1;
      }
      t = (t & (t + 1)) - 1;
    }
    p = (p & (p + 1)) - 1;
  }
  return res;
}

void Add_To_Elem(long long x, long long y, long long z, long long val, long long size, vector<vector<vector<long long>>>& tree) {
  long long p = x;
  while (p < size) {
    long long t = y;
    while (t < size) {
      long long k = z;
      while (k < size) {
        tree[p][t][k] += val;
        k = k | (k + 1);
      }
      t = t | (t + 1);
    }
    p = p | (p + 1);
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n; cin >> n;
  vector<vector<vector<long long>>> fenvik_tree(n, vector<vector<long long>>(n, vector<long long>(n, 0)));
  long long query; cin >> query;
  while (query != 3) {
    if (query == 1) {
      long long x, y, z, value; cin >> x >> y >> z >> value;
      Add_To_Elem(x, y, z, value, n, fenvik_tree);
    }
    else {
      long long x1, y1, z1, x2, y2, z2; cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
      cout << Prefix_Sum(x2, y2, z2, fenvik_tree) - Prefix_Sum(x1 - 1, y1 - 1, z1 - 1, fenvik_tree) -
      Prefix_Sum(x2, y1 - 1, z2, fenvik_tree) - Prefix_Sum(x2, y2, z1 - 1, fenvik_tree)
      - Prefix_Sum(x1 - 1, y2, z2, fenvik_tree) + Prefix_Sum(x1 - 1, y1 - 1, z2, fenvik_tree)
      + Prefix_Sum(x1 - 1, y2, z1 - 1, fenvik_tree) + Prefix_Sum(x2, y1 - 1, z1 - 1, fenvik_tree)<< '\n';
    }
    cin >> query;
  }
}