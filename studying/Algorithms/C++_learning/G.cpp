#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
/*#pragma GCC optimize("Ofast,no-stack-protector")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,avx2,tune=native")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("fast-math")
#pragma GCC optimize("section-anchors")
#pragma GCC optimize("profile-values,profile-reorder-functions,tracer")
#pragma GCC optimize("vpt")
#pragma GCC optimize("rename-registers")
#pragma GCC optimize("move-loop-invariants")
#pragma GCC optimize("unswitch-loops")
#pragma GCC optimize("function-sections")
#pragma GCC optimize("data-sections")
#pragma GCC optimize("branch-target-load-optimize")
#pragma GCC optimize("branch-target-load-optimize2")
#pragma GCC optimize("btr-bb-exclusive")
#pragma optimize(" ", off )

void compress_cods(std::vector<long long>& cods) {
  std::sort(cods.begin(), cods.end());
  cods.erase(std::unique(cods.begin(), cods.end()), cods.end());
}

class Fenvik_tree_x {
 public:
  std::vector<std::pair<std::vector<long long>, std::vector<long long>>> fenvik_x;
  Fenvik_tree_x(size_t size) {
    fenvik_x = std::vector<std::pair<std::vector<long long>, std::vector<long long>>>(size);
  }
  void add_tree(long long index, std::set<long long>& y_s) {
    for (long long it : y_s) {
      fenvik_x[index].second.push_back(it);
    }
    fenvik_x[index].first = std::vector<long long>(y_s.size(), 0);
  }
  void Add_element(long long x, long long y, long long val) {
    long long x_tmp = x;
    while (x_tmp < fenvik_x.size()) {
      long long y_tmp = y;
      long long l = 0;
      long long r = fenvik_x[x_tmp].second.size();
      while (r - l > 1) {
        long long m = (r + l) / 2;
        if (fenvik_x[x_tmp].second[m] <= y_tmp) l = m;
        else r = m;
      }
      y_tmp = l;
      while (y_tmp < fenvik_x[x_tmp].second.size()) {
        fenvik_x[x_tmp].first[y_tmp] += val;
        y_tmp |= (y_tmp + 1);
      }
      x_tmp |= (x_tmp + 1);
    }
  }
  long long Get_Sum(long long x, long long y) {
    long long res = 0;
    long long x_tmp = x;
    while (x_tmp >= 0) {
      long long y_tmp = y;
      long long l = 0;
      long long r = fenvik_x[x_tmp].second.size();
      while (r - l > 1) {
        long long m = (r + l) / 2;
        if (fenvik_x[x_tmp].second[m] <= y_tmp) l = m;
        else r = m;
      }
      y_tmp = l;
      if (fenvik_x[x_tmp].second[y_tmp] <= y) {
        while (y_tmp >= 0) {
          res += fenvik_x[x_tmp].first[y_tmp];
          y_tmp = (y_tmp & (y_tmp + 1)) - 1;
        }
      }
      x_tmp = (x_tmp & (x_tmp + 1)) - 1;
    }
    return res;
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n; std::cin >> n;
  std::vector<long long> x_cods(n);
  std::vector<long long> y_cods(n);
  std::vector<std::vector<long long>> cods_and_values(n);
  std::vector<std::vector<long long>> cods_and_values_not_sorted(n);
  for (long long i = 0; i < n; ++i) {
    long long x, y, val; std::cin >> x >> y >> val;
    x_cods[i] = x;
    y_cods[i] = y;
    cods_and_values[i] = {x, y, val};
    cods_and_values_not_sorted[i] = {x, y, val};
  }
  std::sort(cods_and_values.begin(), cods_and_values.end());
  long long counter = 0;
  long long num_now = cods_and_values[0][0];
  for (long long i = 0; i < n; ++i) {
    if (cods_and_values[i][0] == num_now) cods_and_values[i][0] = counter;
    else {
      num_now = cods_and_values[i][0];
      ++counter;
      cods_and_values[i][0] = counter;
    }
  }
  compress_cods(x_cods);
  Fenvik_tree_x fenvik_main = Fenvik_tree_x(x_cods.size());
  std::vector<std::set<long long>> x_for_y(x_cods.size());
  for (long long i = 0; i < n; ++i) {
    long long tmp = cods_and_values[i][0];
    while (tmp < x_cods.size()) {
      x_for_y[tmp].insert(cods_and_values[i][1]);
      tmp = (tmp | (tmp + 1));
    }
  }
  for (long long i = 0; i < x_cods.size(); ++i) {
    fenvik_main.add_tree(i, x_for_y[i]);
  }
  for (long long i = 0; i < n; ++i) {
    fenvik_main.Add_element(cods_and_values[i][0], cods_and_values[i][1], cods_and_values[i][2]);
  }
  long long k; std::cin >> k;
  for (long long i = 0; i < k; ++i) {
    char query; std::cin >> query;
    if (query == 'g') {
      for (long long j = 0; j < 2; ++j) std::cin >> query;
      long long x, y;
      std::cin >> x >> y;
      long long l = 0;
      long long r = x_cods.size();
      while (r - l > 1) {
        long long m = (r + l) / 2;
        if (x_cods[m] <= x) l = m;
        else r = m;
      }
      if (x_cods[l] <= x) {
        x = l;
        std::cout << fenvik_main.Get_Sum(x, y) << '\n';
      }
      else std::cout << 0 << '\n';
    }
    else {
      for (long long j = 0; j < 5; ++j) std::cin >> query;
      long long num, value;
      std::cin >> num >> value;
      long long x = cods_and_values_not_sorted[num - 1][0];
      long long y = cods_and_values_not_sorted[num - 1][1];
      long long l = 0;
      long long r = x_cods.size();
      while (r - l > 1) {
        long long m = (r + l) / 2;
        if (x_cods[m] <= x) l = m;
        else r = m;
      }
      x = l;
      int val = value - cods_and_values_not_sorted[num - 1][2];
      cods_and_values_not_sorted[num - 1][2] = value;
      fenvik_main.Add_element(l, y, val);
    }
  }
}*/
int main() {
  std::vector<char> t;
  t.push_back((6 -5) + '0');
  std::cout << t[0];
}