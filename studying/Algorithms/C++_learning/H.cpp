#include <iostream>
#include <vector>
#include <cmath>

using std::vector;

int Second_RMQ(vector<vector<std::pair<int, int>>>& table, int l, int r, vector<int>& logs, int times) {
  int t = logs[r - l];
  if (times == 0) {
    std::pair<int, int> k;
    if (table[t][l].first < table[t][r - (1 << t)].first) {
      k = table[t][l];
    } else {
      k = table[t][r - (1 << t)];
    }
    if (k.second > l && k.second + 1 < r) return std::min(Second_RMQ(table, l, k.second, logs, 1), Second_RMQ(table, k.second + 1, r, logs, 1));
    else if (k.second > l && k.second + 1 >= r) return Second_RMQ(table, l, k.second, logs, 1);
    else return Second_RMQ(table, k.second + 1, r, logs, 1);
  }
  if (times == 1) {
    if (table[t][l].first < table[t][r - (1 << t)].first) {
      std::pair<int, int> k = table[t][l];
      return k.first;
    } else {
      std::pair<int, int> k = table[t][r - (1 << t)];
      return  k.first;
    }
  }
}

void build(vector<vector<std::pair<int, int>>>& table, vector<int>& arr, int n, int degree_size) {
  for (int i = 0; i < n; ++i) {
    table[0][i].first = arr[i];
    table[0][i].second = i;
  }
  for (int i = 1; i < degree_size; ++i) {
    for (int j = 0; j + (1 << i) <= n; ++j) {
      if (table[i - 1][j].first < table[i - 1][j + (1 << (i - 1))].first) {
        table[i][j] = table[i - 1][j];
      }
      else {
        table[i][j] = table[i - 1][j + (1 << (i - 1))];
      }
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  int i;
  vector<int> logs(n + 5);
  logs[1] = 0;
  for (i = 2; i < n + 5; ++i) {
    logs[i] = logs[i - 1];
    if ((i & (i - 1)) == 0) ++logs[i];
  }
  vector<vector<std::pair<int, int>>> table(logs[n] + 1, vector<std::pair<int, int>> (n, {0, 0}));
  vector<int> arr(n);
  for (i = 0; i < n; ++i) std::cin >> arr[i];
  build(table, arr, n, logs[n] + 1);
  for (i = 0; i < m; ++i) {
    int l, r; std::cin >> l >> r;
    std::cout << Second_RMQ(table, l - 1, r, logs, 0) << '\n';
  }
}