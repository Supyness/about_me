#include <iostream>
#include <vector>
#pragma GCC optimize("Ofast,no-stack-protector")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,avx2,tune=native")
#pragma GCC optimize("unroll-loops")
#pragma GCC optimize("fast-math")
#pragma GCC optimize("section-anchors")
#pragma GCC optimize("profile-values,profile-reorder-functions,tracer")
#pragma GCC optimize("vpt")
#pragma GCC optimize("rename-registers")
#pragma GCC optimize("move-loop-invariants")
#pragma GCC optimize("unswitch-loops")
#pragma GCC optimize("function-sections")
#pragma GCC optimize("data-sections")
#pragma GCC optimize("branch-target-load-optimize")
#pragma GCC optimize("branch-target-load-optimize2")
#pragma GCC optimize("btr-bb-exclusive")

using std::vector;

void build_tree(vector<long long>& main_array, vector<std::pair<long long, bool>>& tree, long long ver, long long l, long long r) {
  if (l == r) {
    tree[ver].first = main_array[l];
  }
  else {
    long long mid = (l + r) >> 1;
    build_tree(main_array, tree, ver << 1, l, mid);
    build_tree(main_array, tree, (ver << 1) + 1, mid + 1, r);
    tree[ver].first = std::min(tree[ver * 2].first, tree[ver * 2 + 1].first);
  }
}

void push(vector<std::pair<long long, bool>>& tree, long long v) {
  if (tree[v].second) {
    tree[v << 1] = tree[(v << 1) + 1] = tree[v];
    tree[v].second = false;
  }
}

long long Minimum(vector<std::pair<long long, bool>>& tree, long long v, long long treel, long long treer, long long l, long long r) {
  if (l > r) return LLONG_MAX;
  if (l == treel && r == treer) {
    return tree[v].first;
  }
  push(tree, v);
  long long mid = (treel + treer) >> 1;
  return std::min(Minimum(tree, v << 1, treel, mid, l, std::min(r, mid)), Minimum(tree, (v << 1) + 1, mid + 1, treer, std::max(l, mid + 1), r));
}
void Update(vector<std::pair<long long, bool>>& tree, long long v, long long treel, long long treer, long long l, long long r, long long val) {
  if (l > r) return;
  if (treel == l && treer == r) {
    tree[v].first = val;
    tree[v].second = true;
  }
  else {
    push(tree, v);
    long long mid = (treel + treer) / 2;
    Update(tree, v << 1, treel, mid, l, std::min(r, mid), val);
    Update(tree, (v << 1) + 1, mid + 1, treer, std::max(l, mid + 1), r, val);
    tree[v].first = std::min(tree[v << 1].first, tree[(v << 1) + 1].first);
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n; std::cin >> n;
  vector<long long> arr(n);
  for (long long i = 0; i < n; ++i) {
    long long r, g, b; std::cin >> r >> g >> b;
    arr[i] = r + g + b;
  }
  vector<std::pair<long long, bool>> tree(4 * n, {LLONG_MAX, false});
  build_tree(arr, tree, 1, 0, n - 1);
  long long k; std::cin >> k;
  for (long long i = 0; i < k; ++i) {
    long long c, d, r, g, b, e, f; std::cin >> c >> d >> r >> g >> b >> e >> f;
    Update(tree, 1, 0, n - 1, c, d, r + g + b);
    std::cout << Minimum(tree, 1, 0, n - 1, e, f) << ' ';
  }
}