#include <iostream>
#include <vector>

using std::vector;

struct Node {
  long long val = 0;
  long long left = 0;
  long long right = 1000000000;
  Node* leftson = nullptr;
  Node* rightson = nullptr;
};

void add_ver(Node* main_tree, long long val) {
  if (main_tree->left != main_tree -> right) {
    main_tree->val += val;
    long long mid = (main_tree->left + main_tree->right) / 2;
    if (val > mid) {
      if (main_tree->rightson == nullptr) {
        Node *node = new Node;
        main_tree->rightson = node;
        node->left = mid + 1;
        node->right = main_tree->right;
        add_ver(node, val);
      }
      else {
        add_ver(main_tree->rightson, val);
      }
    } else {
      if (main_tree->leftson == nullptr) {
        Node *node = new Node;
        main_tree->leftson = node;
        node->left = main_tree->left;
        node->right = mid;
        add_ver(node, val);
      }
      else {
        add_ver(main_tree->leftson, val);
      }
    }
  }
  else main_tree->val += val;
}

long long Summ(Node* tree, long long l, long long r) {
  if (l > r) return 0;
  if (tree->left == l && tree->right == r) return tree->val;
  long long mid = (tree->left +  tree->right) / 2;
  if (tree->leftson == nullptr && tree->rightson == nullptr) return 0;
  else if (tree->leftson == nullptr && tree->rightson != nullptr) return Summ(tree->rightson, std::max(l, mid + 1), r);
  else if (tree->leftson != nullptr && tree->rightson == nullptr) return Summ(tree->leftson, tree->left, std::min(r, mid));
  else return Summ(tree->leftson, tree->left, std::min(r, mid)) + Summ(tree->rightson, std::max(l, mid + 1), r);
}

int main() {
  int n; std::cin >> n;
  vector<vector<int>> coors(n);
  int miny = 200000000;
  for (int i = 0; i < n; ++i) {
    vector<int> arr(4);
    for (int j = 0; j < 4; ++j) {
      std::cin >> arr[j];
    }
    miny = std::min(miny, arr[1]);
    std::swap(arr[1], arr[2]);
    coors[i] = arr;
  }
  for (int i = 0; i < n; ++i) {
    coors[i][2] -= miny;
    coors[i][3] -= miny;
  }
}