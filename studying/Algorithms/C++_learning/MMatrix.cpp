#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using std::vector;
using std::string;

int base = 1e9;

class BigInteger {
 private:
  vector<long long> arr;
  bool is_negative = false;
 public:
  [[nodiscard]] bool is_minus() const {
    return is_negative;
  }
  void remove_zeros() {
    while (arr.size() > 1 && arr.back() == 0) {
      arr.pop_back();
    }
    if (arr.size() == 1 && arr.back() == 0) is_negative = false;
  }
  void change_row() {
    if (arr.empty()) {
      arr.push_back(0);
      return;
    }
    arr.push_back(arr.back());
    for (int i = int(arr.size()) - 2; i > 0; --i) arr[i] = arr[i - 1];
    arr[0] = 0;
  }
  BigInteger() {
    arr.push_back(0);
  }
  BigInteger(int num) {
    arr.clear();
    if (num < 0) {
      is_negative = true;
      num = -num;
    }
    if (num == 0) {
      arr = {0};
    }
    while (num != 0) {
      arr.push_back(num % base);
      num /= base;
    }
  }
  BigInteger operator-() const {
    BigInteger tmp = *this;
    tmp.is_negative = (is_negative + 1) % 2;
    tmp.remove_zeros();
    return tmp;
  }
  BigInteger &operator=(const BigInteger &num) {
    if (*this != num) {
      arr = num.arr;
      is_negative = num.is_negative;
    }
    return *this;
  }
  bool abs_compare_lower(const BigInteger &num) const {
    if (arr.size() != num.arr.size()) return arr.size() < num.arr.size();
    for (int i = arr.size() - 1; i >= 0; --i) {
      if (arr[i] != num.arr[i]) return arr[i] < num.arr[i];
    }
    return true;
  }
  bool operator!=(const BigInteger &num) const {
    return !(num == *this);
  }
  bool operator>(const BigInteger &num) const {
    if (is_negative && !num.is_negative) return false;
    else if (!is_negative && num.is_negative) return true;
    else if (is_negative && num.is_negative) {
      if (arr.size() > num.arr.size()) return false;
      else if (arr.size() < num.arr.size()) return true;
      else {
        for (int i = int(num.arr.size() - 1); i > -1; --i) {
          if (arr[i] > num.arr[i]) return false;
          else if (arr[i] < num.arr[i]) return true;
        }
      }
    } else {
      if (arr.size() < num.arr.size()) return false;
      else if (arr.size() > num.arr.size()) return true;
      else {
        for (int i = int(num.arr.size()) - 1; i > -1; --i) {
          if (arr[i] > num.arr[i]) return true;
          else if (arr[i] < num.arr[i]) return false;
        }
      }
    }
    return false;
  }
  bool operator<(const BigInteger &num) const {
    return !((*this == num) || (*this > num));
  }
  bool operator<=(const BigInteger &num) const {
    return ((*this < num) || (*this == num));
  }
  bool operator>=(const BigInteger &num) const {
    return ((*this > num) || (*this == num));
  }
  [[nodiscard]] string toString() const {
    string s;
    if (is_negative) s += '-';
    for (int i = int(arr.size()) - 1; i > -1; --i) {
      string tmp = std::to_string(arr[i]);
      if (tmp.size() < 9 && i != int(arr.size()) - 1) {
        while (tmp.size() < 9) tmp = '0' + tmp;
      }
      s += tmp;
    }
    return s;
  }
  BigInteger &operator*=(const BigInteger &num) {
    /*vector<int> result;
    result.resize(arr.size() + num.arr.size());
    for (int i = 0; i < int(arr.size()); ++i) {
      int addible = 0;
      for (int j = 0; j < int(num.arr.size()) || addible != 0; ++j) {
        long long current = result[i + j] + arr[i] * 1LL * (j < int(num.arr.size()) ? num.arr[j] : 0) + addible;
        result[i + j] = static_cast<int>(current % base);
        addible = static_cast<int>(current / base);
      }
    }
    arr = result;
    is_negative = (is_negative + num.is_negative) % 2;
    remove_zeros();
    return *this;*/
    is_negative = (is_negative + num.is_negative) % 2;
    vector<long long> numbers(arr.size() + num.arr.size());
    for (int i = 0; i < int(num.arr.size()); ++i) {
      for (int j = 0; j < int(arr.size()); ++j) {
        numbers[i + j] += arr[j] * num.arr[i];
        numbers[i + j + 1] += numbers[i + j] / base;
        numbers[i + j] %= base;
      }
    }
    arr = numbers;
    remove_zeros();
    return *this;
  }

  /*static BigInteger pr(const BigInteger &num1, const BigInteger &num2, int l1, int r1, int l2, int r2) {
    int n = std::max(r1 - l1, r2 - l2);
    if (n == 0) {
      BigInteger tmp;
      long long tmp1 = num1.arr[l1] * num2.arr[l2];
      tmp.arr.resize(l1 + 1, 0);
      tmp.arr[l1] = tmp1 % base;
      tmp.arr[l1 + 1] = (tmp1 / base) % base;
    } else {
      return pr(num1, num2, l1, r1 / 2, l2, r2 / 2) + (pr(num1, num2, l1, r1 / 2, l2, r2 / 2))
    }
  }
  BigInteger &operator*=(const BigInteger &num) {
    string num1 = toString(), num2 = num.toString();
    int sz = std::max(num1.size(), num2.size());
    if (sz == 1) {
      *this = arr[0] * num.arr[0];
    }
  }*/
  BigInteger &operator+=(const BigInteger &num) {
    if (!((is_negative + num.is_negative) % 2)) {
      long long addible = 0;
      int size = std::max(int(arr.size()), int(num.arr.size())) + 1;
      for (int i = 0; i < size; ++i) {
        if (i >= int(arr.size())) {
          arr.push_back(0);
        }
        long long num1 = arr[i];
        long long num2 = (i < int(num.arr.size()) ? num.arr[i] : 0);
        arr[i] = (num1 + num2 + addible) % base;
        addible = (num1 + num2 + addible) / base;
        if ((i >= int(num.arr.size()) - 1) && (addible == 0)) break;
      }
    } else if (abs_compare_lower(num)) {
      is_negative = !is_negative;
      long long addible = 0;
      for (int i = 0; i < int(num.arr.size()); ++i) {
        if (i >= int(arr.size())) arr.push_back(0);
        if (num.arr[i] >= arr[i] + addible) {
          arr[i] = num.arr[i] - arr[i] - addible;
          addible = 0;
        }
        else {
          arr[i] = base + num.arr[i] - arr[i] - addible;
          addible = 1;
        }
      }
    }
    else {
      long long addible = 0;
      for (int i = 0; i < int(arr.size()); ++i) {
        if ((i >= int(num.arr.size())) && (addible == 0)) break;
        long long num1 = (i < int(num.arr.size()) ? num.arr[i] : 0);
        if (arr[i] >= num1 + addible) {
          arr[i] = arr[i] - num1 - addible;
          addible = 0;
        }
        else {
          arr[i] = base + arr[i] - num1 - addible;
          addible = 1;
        }
      }
    }
    remove_zeros();
    return *this;
  }
  BigInteger& operator-=(const BigInteger& num) {
    if (((is_negative + num.is_negative)) % 2) {
      long long addible = 0;
      int size = std::max(int(arr.size()), int(num.arr.size())) + 1;
      for (int i = 0; i < size; ++i) {
        if (i >= int(arr.size())) {
          arr.push_back(0);
        }
        long long num1 = arr[i];
        long long num2 = (i < int(num.arr.size()) ? num.arr[i] : 0);
        arr[i] = (num1 + num2 + addible) % base;
        addible = (num1 + num2 + addible) / base;
        if ((i >= int(num.arr.size()) - 1) && (addible == 0)) break;
      }
    } else if (abs_compare_lower(num)) {
      is_negative = !is_negative;
      long long addible = 0;
      for (int i = 0; i < int(num.arr.size()); ++i) {
        if (i >= int(arr.size())) arr.push_back(0);
        if (num.arr[i] >= arr[i] + addible) {
          arr[i] = num.arr[i] - arr[i] - addible;
          addible = 0;
        }
        else {
          arr[i] = base + num.arr[i] - arr[i] - addible;
          addible = 1;
        }
      }
    }
    else {
      long long addible = 0;
      for (int i = 0; i < int(arr.size()); ++i) {
        if ((i >= int(num.arr.size())) && (addible == 0)) break;
        long long num1 = (i < int(num.arr.size()) ? num.arr[i] : 0);
        if (arr[i] >= num1 + addible) {
          arr[i] = arr[i] - num1 - addible;
          addible = 0;
        }
        else {
          arr[i] = base + arr[i] - num1 - addible;
          addible = 1;
        }
      }
    }
    remove_zeros();
    return *this;
  }
  /*vector<long long> del(const BigInteger& num) {
    bool flag = (is_negative + num.is_negative) % 2;
    vector<long long> tmp(num.arr.size());
    vector<long long> result(arr.size());
    vector<long long> counter;
    long long ub = 0;
    long long bb = 0;
    long long cb = 0;
    for (size_t i = 0; i < arr.size(); ++i) {
      tmp.insert(tmp.begin(), arr[arr.size() - 1 - i]);
      tmp[tmp.size() - 2] += tmp[tmp.size() - 1] * base;
      tmp.pop_back();
      ub = tmp[tmp.size() - 1] / num.arr[num.arr.size() - 1];
      bb = tmp[tmp.size() - 1] / (num.arr[num.arr.size() - 1] + 1);
      cb = (ub + bb) / 2;
      while (ub != bb) {
        counter = tmp;
        for (size_t j = 0; j < num.arr.size(); ++j) {
          counter[j] -= cb * num.arr[j];
          if (counter[j] < 0) {
            if (j != num.arr.size() - 1) {
              counter[j + 1] -= std::abs(counter[j]) / base;
              if (std::abs(counter[j]) % base) --counter[j + 1];
              counter[j] %= base;
              counter[j] += base;
            }
          }
        }
        if (counter[num.arr.size() - 1] < 0) {
          if (ub - bb == 1) ub = bb;
          else {
            ub = cb;
            cb = (bb + ub) / 2;
          }
        }
        else {
          if (ub - bb == 1) {
            if (cb == ub) bb = ub;
            else cb = ub;
          }
          else {
            bb = cb;
            cb = (bb + ub) / 2;
          }
        }
      }
      result[i] = bb;
      for (size_t j = 0; j < num.arr.size(); ++j) {
        tmp[j] -= bb * num.arr[j];
        if (tmp[j] < 0) {
          tmp[j + 1] -= std::abs(tmp[j]) / base;
          if (std::abs(tmp[j]) % base != 0) --tmp[j + 1];
          tmp[j] %= base;
          tmp[j] += base;
        }
      }
    }
    std::reverse(result.begin(), result.end());
    arr = result;
    is_negative = flag;
    return tmp;
  }*/
  BigInteger &operator/=(const BigInteger &num) {
    bool flag = (is_negative + num.is_negative) % 2;
    is_negative = false;
    BigInteger div = (num < 0 ? -num : num);
    BigInteger new_div = 0;
    int counter = int(arr.size()) - 1;
    while (new_div < div) {
      new_div *= base;
      new_div += static_cast<int>(arr[counter--]);
    }
    BigInteger result = 0;

    while (counter >= -1) {
      long long l, r;
      if (new_div.arr.size() - div.arr.size() >= 2) {
        l = base - 1;
        r = base;
      }
      else if (new_div.arr.size() - div.arr.size() == 1) {
        l = base / (div.arr[div.arr.size() - 1] + 1);
        r = base;
      }
      else if (new_div.arr.size() == div.arr.size()) {
        l = new_div.arr[new_div.arr.size() - 1] / (div.arr[div.arr.size() - 1] + 1);
        r = (new_div.arr[new_div.arr.size() - 1] + 1) / div.arr[div.arr.size() - 1] + 1;
      }
      else {
        l = 0;
        r = 1;
      }
      while (l < r - 1) {
        long long m = (l + r) / 2;
        if (div * m <= new_div) l = m;
        else r = m;
      }
      result *= base;
      result += l;
      if (counter != -1) {
        new_div -= div * l;
        new_div *= base;
        new_div += static_cast<int>(arr[counter]);
      }
      --counter;
    }
    *this = result;
    is_negative = flag;
    remove_zeros();
    return *this;
  }
  BigInteger &operator%=(const BigInteger &num) {
    /*arr = del(num);
    remove_zeros();
    return *this;*/
    return (*this = (*this - num * (*this / num)));
  }
  BigInteger &operator++() {
    return (*this += 1);
  }
  BigInteger &operator--() {
    return (*this -= 1);
  }
  BigInteger operator++(int) {
    BigInteger tmp = *this;
    *this += 1;
    return tmp;
  }
  BigInteger operator--(int) {
    BigInteger tmp = *this;
    *this -= 1;
    return tmp;
  }
  explicit operator bool() const {
    if (*this == 0) return false;
    else return true;
  }
  /*explicit operator int() const {
    string s;
    for (int i : arr) s += std::to_string(i);
    return std::stoi(s);
  }*/
  ~BigInteger() = default;
  friend std::istream &operator>>(std::istream &in, BigInteger &num);
  friend std::ostream &operator<<(std::ostream &out, const BigInteger &num);
  friend bool operator==(const BigInteger &num1, const BigInteger &num2);
  friend BigInteger operator+(const BigInteger &num1, const BigInteger &num2);
  friend BigInteger operator-(const BigInteger &num1, const BigInteger &num2);
  friend BigInteger operator*(const BigInteger &num1, const BigInteger &num2);
  friend BigInteger operator/(const BigInteger &num1, const BigInteger &num2);
  friend BigInteger operator%(const BigInteger &num1, const BigInteger &num2);
};

bool operator==(const BigInteger &num1, const BigInteger &num2) {
  if (num1.arr.size() != num2.arr.size() || num1.is_negative != num2.is_negative) return false;
  else {
    for (int i = 0; i < int(num2.arr.size()); ++i) {
      if (num2.arr[i] != num1.arr[i]) return false;
    }
  }
  return true;
}

std::istream &operator>>(std::istream &in, BigInteger &num) {
  in.tie(nullptr);
  bool flag = false;
  num.arr.clear();
  char t;
  in.get(t);
  while (std::isspace(t)) in.get(t);
  if (t == '-') {
    flag = true;
    in.get(t);
  }
  while (!std::isspace(t) && !in.eof()) {
    num *= 10;
    num += (t - '0');
    in.get(t);
  }
  num.is_negative = flag;
  return in;
}
std::ostream &operator<<(std::ostream &out, const BigInteger &num) {
  out.tie(nullptr);
  out << num.toString();
  return out;
}

BigInteger operator+(const BigInteger &num1, const BigInteger &num2) {
  BigInteger tmp = num1;
  tmp += num2;
  return tmp;
}
BigInteger operator-(const BigInteger &num1, const BigInteger &num2) {
  BigInteger tmp = num1;
  tmp -= num2;
  return tmp;
}
BigInteger operator*(const BigInteger &num1, const BigInteger &num2) {
  BigInteger tmp = num1;
  tmp *= num2;
  return tmp;
}
BigInteger operator/(const BigInteger &num1, const BigInteger &num2) {
  BigInteger tmp = num1;
  tmp /= num2;
  return tmp;
}
BigInteger operator%(const BigInteger &num1, const BigInteger &num2) {
  BigInteger tmp = num1;
  tmp %= num2;
  return tmp;
}

BigInteger gcd(BigInteger num1, BigInteger num2) {
  if (num1.is_minus()) num1 = -num1;
  if (num2.is_minus()) num2 = -num2;
  while (num1 != 0 && num2 != 0) {
    if (num1 > num2) num1 %= num2;
    else num2 %= num1;
  }
  return num1 + num2;
}

class Rational {
 private:
  BigInteger numenator;
  BigInteger denominator;
 public:
  void irreducible() {
    if (numenator == 0) {
      denominator = 1;
    }
    else {
      BigInteger del = gcd(numenator, denominator);
      if (del >= 1) {
        numenator /= del;
        denominator /= del;
      } else {
        denominator = (denominator.is_minus() ? -1 : 1);
      }
      if (denominator < 0) {
        numenator = -numenator;
        denominator = -denominator;
      }
    }
  }
  Rational() : numenator(0), denominator(1) {
  }
  Rational(int num) {
    numenator = num;
    denominator = 1;
  }
  Rational(const BigInteger &num1) {
////    std::cerr << num1 << std::endl;
    numenator = num1;
    denominator = 1;
  }
  bool operator!=(const Rational &num) const {
    return !(*this == num);
  }
  bool operator>(const Rational &num) const {
    if (numenator.is_minus() && !num.numenator.is_minus()) return false;
    else if (!numenator.is_minus() && num.numenator.is_minus()) return true;
    else if (numenator.is_minus() && num.numenator.is_minus()) {
      return (numenator * num.denominator < num.numenator * denominator);
    } else {
      return (numenator * num.denominator > num.numenator * denominator);
    }
  }
  bool operator<(const Rational &num) const {
    return !((*this == num) || (*this > num));
  }
  bool operator<=(const Rational &num) const {
    return ((*this < num) || (*this == num));
  }
  bool operator>=(const Rational &num) const {
    return ((*this > num) || (*this == num));
  }
  Rational operator-() const {
    Rational tmp = 0;
    tmp -= *this;
////    std::cerr << tmp.toString() << std::endl;
    return tmp;
  }
  [[nodiscard]] string toString() const {
    string s;
    s += numenator.toString();
    if (denominator > 1) {
      s += '/';
      s += denominator.toString();
    }
    return s;
  }
  Rational &operator=(const Rational &num) = default;
  Rational &operator+=(const Rational &num) {
    numenator = numenator * num.denominator + num.numenator * denominator;
    denominator *= num.denominator;
    irreducible();
    return *this;
  }
  Rational &operator-=(const Rational &num) {
    numenator = numenator * num.denominator - num.numenator * denominator;
    denominator *= num.denominator;
    irreducible();
    return *this;
  }
  Rational &operator/=(const Rational &num) {
    numenator *= num.denominator;
    denominator *= num.numenator;
    irreducible();
    return *this;
  }
  Rational &operator*=(const Rational &num) {
    numenator *= num.numenator;
    denominator *= num.denominator;
    irreducible();
    return *this;
  }
  [[nodiscard]] string asDecimal(size_t precision = 0) const {
    BigInteger num = (numenator < 0 ? -numenator : numenator);
    for (size_t i = 0; i < precision; ++i) {
      num *= 10;
    }
    string s = (num / denominator).toString();
    if (s.size() < precision + 1) {
      s = string(precision + 1 - s.size(), '0') + s;
    }
    string s1 = s.substr(0, s.size() - precision);
    if (precision != 0) {
      s1 += '.';
      s1 += s.substr(s.size() - precision, precision);
    }
    if (numenator.is_minus()) s1 = "-" + s1;
    return s1;
  }
  explicit operator double() const {
    double tmp = std::stod(asDecimal(16));
    return tmp;
  }
  /*explicit operator int() const {
    return int(numenator);
  }*/
  ~Rational() = default;
  friend bool operator==(const Rational &num, const Rational &num1);
  friend Rational operator+(const Rational &num1, const Rational &num2);
  friend Rational operator-(const Rational &num1, const Rational &num2);
  friend Rational operator*(const Rational &num1, const Rational &num2);
  friend Rational operator/(const Rational &num1, const Rational &num2);
};

bool operator==(const Rational &num, const Rational &num1) {
  if (num.numenator == num1.numenator && num.denominator == num1.denominator) return true;
  return false;
}
std::ostream& operator<<(std::ostream& out, const Rational& num) {
  out.tie(nullptr);
  out << double(num);
  return out;
}
std::istream &operator>>(std::istream &in, Rational &num) {
  in.tie(nullptr);
  num = 0;
  bool flag = false;
  char t;
  in.get(t);
  while (std::isspace(t)) in.get(t);
  if (t == '-') {
    flag = true;
    in.get(t);
  }
  while (!std::isspace(t) && !in.eof()) {
    num *= 10;
    num += (t - '0');
    in.get(t);
  }
  if (flag) num *= -1;
////  std::cerr << num.toString() << std::endl;
  return in;
}
Rational operator+(const Rational &num1, const Rational &num2) {
  Rational tmp = num1;
  tmp += num2;
  tmp.irreducible();
  return tmp;
}
Rational operator-(const Rational &num1, const Rational &num2) {
  Rational tmp = num1;
  tmp -= num2;
  tmp.irreducible();
  return tmp;
}
Rational operator*(const Rational &num1, const Rational &num2) {
  Rational tmp = num1;
  tmp *= num2;
  tmp.irreducible();
  return tmp;
}
Rational operator/(const Rational &num1, const Rational &num2) {
  Rational tmp = num1;
  tmp /= num2;
  tmp.irreducible();
  return tmp;
}

template<size_t N>
class Residue;
template <size_t N>
std::ostream& operator<<(std::ostream& out, Residue<N>& num);

template <size_t N, size_t D, bool flag = false>
struct IsPrime {
  static const bool isprime = (N % D != 0) && (IsPrime<N, D + 1, (D * D >= N)>::isprime);
};

template <size_t N, size_t D>
struct IsPrime <N, D, true>{
  static const bool isprime = true;
};

template<size_t N>
class Residue {
 private:
  size_t Number;
 public:
  Residue(): Number(0) {}
  Residue(int k): Number((k >= 0) ? (k % N) : ((N - -k % N) % N)) {}
  Residue(const Residue<N>& num): Number(num.Number) {}
  Residue<N>& operator+=(const Residue<N>& Num) {
////    std::cerr << "+=" << Number << ' ' << Num.Number << ' ' << N << std::endl;
    Number = (Number + Num.Number) % N;
////    std::cerr << Number << std::endl;
    return *this;
  }
  Residue<N>& operator-=(const Residue<N>& Num) {
////    std::cerr << "-=" << Number << ' ' << Num.Number << ' ' << N << std::endl;
    Number += N;
    Number -= Num.Number;
    Number %= N;
////    std::cerr << Number << std::endl;
    return *this;
  }
  Residue<N>& operator*=(const Residue<N>& Num) {
////    std::cerr << "*=" << Number << ' ' << Num.Number << ' ' << N << std::endl;
    Number = (Number * Num.Number) % N;
////    std::cerr << Number << std::endl;
    return *this;
  }
  template<typename = typename std::enable_if<IsPrime<N, 2>::isprime>>
  Residue<N>& operator/=(const Residue<N>& num1) {
////    std::cerr << "/=" << Number << ' ' << num1.Number << ' ' << N << std::endl;
    size_t result = 1;
    size_t tmp = N - 2;
    while (tmp) {
      if (tmp & 1) {
        result = (result * num1.Number) % N;
      }
      if (tmp != 1)  result = (result * result) % N;
      tmp >>= 1;
    }
    this->Number *= result;
    this->Number %= N;
////    std::cerr << Number << std::endl;
    return *this;
  }
  explicit operator int() const {
    return Number;
  }
  bool operator==(const Residue<N>& num) const {
////    std::cerr << Number << ' ' << num.Number << std::endl;
    return (Number == num.Number);
  }
  bool operator!=(const Residue<N>& num) const {
    return (Number != num.Number);
  }
  ~Residue() = default;
  friend std::ostream &operator<<<N>(std::ostream &out, Residue<N> &num);
};

template<size_t N>
std::ostream &operator<<(std::ostream &out, Residue<N> &num) {
  out << num.Number;
  return out;
}

template<size_t N>
Residue<N> operator+(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp += num2);
}
template<size_t N>
Residue<N> operator-(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp -= num2);
}
template<size_t N>
Residue<N> operator*(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp *= num2);
}
template<size_t N>
Residue<N> operator/(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp /= num2);
}


template<size_t M, size_t N = M, typename Field = Rational>
class Matrix {
 private:
  vector<vector<Field>> matrix;
  [[nodiscard]] std::pair<Matrix<M, N, Field>, size_t> triangulation() const {
    Matrix<M, N, Field> matrix1 = *this;
    size_t amount_of_swaps = 0;
    for (size_t i = 0; i < std::min(N, M); ++i) {
      size_t row = i;
      while (row < M - 1 && matrix1[row][i] == Field(0)) ++row;
      if (row != i) {
        std::swap(matrix1[row], matrix1[i]);
        ++amount_of_swaps;
      }
      if (matrix1[i][i] != 0)
        for (size_t j = i + 1; j < M; ++j) {
          Field dividor = matrix1[j][i] / matrix1[i][i];
          for (size_t z = i; z < N; ++z) {
            matrix1[j][z] -= matrix1[i][z] * dividor;
          }
        }
    }
    return {matrix1, amount_of_swaps};
  }
 public:
  Matrix(const vector<vector<Field>> &matrix1) : matrix(matrix1) {}
  template<typename = typename std::enable_if<N == M>>
  Matrix(): matrix(vector(M, vector(M, Field(0)))) {
////    std::cerr << "XXX";
    for (size_t i = 0; i < M; ++i) matrix[i][i] = Field(1);
  }
  Matrix(const Field &num) : matrix(vector(M, vector(N, num))) {}
  Matrix(std::initializer_list<std::initializer_list<int>> arr): matrix(vector(M, vector(N, Field()))) {
    size_t row = 0;
    for (auto it : arr) {
      size_t column = 0;
      for (auto itt = it.begin(); itt != it.end(); ++itt) matrix[row][column++] = Field(*itt);
      ++row;
    }
  }
  ~Matrix() = default;
  Matrix<M, N, Field> &operator=(const Matrix<M, N, Field> &matrix1) {
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        matrix[i][j] = matrix1[i][j];
      }
    }
    return *this;
  }
  Matrix<M, N, Field> &operator+=(const Matrix<M, N, Field> &matrix1) {
////    std::cerr << 10 << std::endl;
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        matrix[i][j] += matrix1[i][j];
      }
    }
    return *this;
  }
  Matrix<M, N, Field> &operator-=(const Matrix<M, N, Field> &matrix1) {
////    std::cerr << 9 << std::endl;
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        matrix[i][j] -= matrix1[i][j];
      }
    }
    return *this;
  }
  Matrix<M, N, Field> &operator*=(const Field &num) {
//    std::cerr << 8 << std::endl;
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        matrix[i][j] *= num;
      }
    }
    return *this;
  }
  template<typename = typename std::enable_if<N == M>>
  [[nodiscard]] Field det() const {
//    std::cerr << 7 << std::endl;
    std::pair<Matrix<M, N, Field>, int> matrix_det = triangulation();
    Field determinator = (matrix_det.second % 2 ? -1 : 1);
    for (size_t i = 0; i < M; ++i) {
      determinator *= matrix_det.first[i][i];
    }
    return determinator;
  }
  template<typename = typename std::enable_if<N == M>>
  [[nodiscard]] size_t rank() const {
//    std::cerr << 6 << std::endl;
    std::pair<Matrix<M, N, Field>, int> matrix_det = triangulation();
    size_t row = 0;
    for (size_t column = 0; column < N; ++column) {
      if (matrix_det.first[row][column] != 0) ++row;
    }
    return row;
  }
  [[nodiscard]] Matrix<N, M, Field> transposed() const {
//    std::cerr << 5 << std::endl;
    vector<vector<Field>> matrix1(N, vector(M, Field()));
    for (size_t i = 0; i < M; ++i)
      for (size_t j = 0; j < N; ++j)
        matrix1[j][i] = matrix[i][j];
    Matrix<M, M, Field> tmp(matrix1);
    return matrix1;
  }
  template<typename = typename std::enable_if<N == M>>
  Matrix<M, N, Field> &operator*=(const Matrix<M, N, Field> &matrix1) {
//    std::cerr << 4 << std::endl;
    Matrix<M, N, Field> matrix_new = *this;
    *this = matrix_new * matrix1;
    return *this;
  }
  bool operator==(const Matrix<M, N, Field> &matrix1) const {
//    std::cerr << 3 << std::endl;
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        if (matrix[i][j] != matrix1[i][j]) return false;
      }
    }
    return true;
  }
  template<typename = typename std::enable_if<N == M>>
  Matrix<M, N, Field> &invert() {
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
//        std::cerr << matrix[i][j] << ' ';
      }
//      std::cerr << std::endl;
    }
//    std::cerr << 2 << std::endl;
    Matrix<M, M, Field> E;
    for (size_t i = 0; i < M; ++i) {
      size_t row = i;
      while (row < M - 1 && matrix[row][i] == Field(0)) ++row;
      if (row != i) {
        std::swap(matrix[row], matrix[i]);
        std::swap(E[row], E[i]);
      }
      if (matrix[i][i] != Field(0)) {
        Field tmp = matrix[i][i];
        for (size_t j = 0; j < M; ++j) {
          matrix[i][j] /= tmp;
          E[i][j] /= tmp;
        }
        for (size_t j = i + 1; j < M; ++j) {
          Field dividor = matrix[j][i] / matrix[i][i];
          for (size_t z = 0; z < N; ++z) {
            matrix[j][z] -= dividor * matrix[i][z];
            E[j][z] -= dividor * E[i][z];
          }
        }
      } else return *this;
    }
    for (size_t j = M - 1; j >= 1; --j) {
      for (size_t z = 0; z < j; ++z) {
        Field dividor = matrix[z][j];
        matrix[z][j] = Field(0);
        for (size_t k = 0; k < M; ++k) {
          E[z][k] -= dividor * E[j][k];
        }
      }
    }
    *this = E;
    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        std::cout << E[i][j] << ' ';
      }
      std::cout << std::endl;
    }
    return *this;
  }
  bool operator!=(const Matrix<M, N, Field> &matrix1) const {
    return !(*this == matrix1);
  }
  template<typename = typename std::enable_if<N == M>>
  [[nodiscard]] Field trace() const {
//    std::cerr << 1 << std::endl;
    Field tmp(0);
    for (size_t i = 0; i < N; ++i) tmp += matrix[i][i];
    return tmp;
  }
  [[nodiscard]] vector<Field> getRow(size_t row) const {
//    std::cerr << 0 << std::endl;
    return matrix[row];
  }
  [[nodiscard]] vector<Field> getColumn(size_t column) const {
//    std::cerr << -1 << std::endl;
    vector<Field> tmp(M);
    for (size_t i = 0; i < M; ++i) {
      tmp[i] = matrix[i][column];
    }
    return tmp;
  }
  template<typename = typename std::enable_if<N == M>>
  Matrix<M, N, Field> inverted() {
//    std::cerr << -2 << std::endl;
    Matrix<M, N, Field> tmp = *this;
    return tmp.invert();
  }
  vector<Field> &operator[](size_t ind) {
    return matrix[ind];
  }
  vector<Field> operator[](size_t ind) const {
    return matrix[ind];
  }
};

template<size_t M, size_t N, typename Field = Rational>
Matrix<M, N, Field> operator+(const Matrix<M, N, Field> &matrix1, const Matrix<M, N, Field> &matrix2) {
//  std::cerr << -3 << std::endl;
  Matrix<M, N, Field> matrix = matrix1;
  return (matrix += matrix2);
}
template<size_t M, size_t N, typename Field = Rational>
Matrix<M, N, Field> operator-(const Matrix<M, N, Field> &matrix1, const Matrix<M, N, Field> &matrix2) {
//  std::cerr << -4 << std::endl;
  Matrix<M, N, Field> matrix = matrix1;
  return (matrix -= matrix2);
}
template<size_t M, size_t N, size_t K, typename Field = Rational>
Matrix<M, K, Field> operator*(const Matrix<M, N, Field> &matrix1, const Matrix<N, K, Field> &matrix2) {
//  std::cerr << -5 << std::endl;
  Matrix<M, K, Field> tmp(Field(0));
  for (size_t i = 0; i < M; ++i)
    for (size_t j = 0; j < K; ++j) {
      for (size_t z = 0; z < N; ++z) {
        tmp[i][j] += matrix1[i][z] * matrix2[z][j];
      }
    }
  return tmp;
}
template<size_t M, size_t N, typename Field = Rational>
Matrix<M, N, Field> operator*(const Field &num, const Matrix<M, N, Field> &matrix1) {
  Matrix<M, N, Field> matrix = matrix1;
  return (matrix *= num);
}

template<size_t M, typename Field = Rational>
using SquareMatrix = Matrix<M, M, Field>;

int main() {
  vector<vector<Rational>> tmp =
      {{87, 15, -69, -56, 58, -85, 52, -59, -72, 88, 60, -48, -43, -80, 6, -91, -44, -72, -47, 9},
       {-8, 10, 26, -10, -24, 31, -96, 58, 81, -73, 95, 15, 82, -37, -10, 22, -64, -35, -52, -77},
       {-65, 36, -53, -32, -42, 69, -42, 85, -80, -52, 25, -59, 57, 49, -71, 21, 59, 5, 39, -91},
       {32, -29, -57, 5, -22, -92, -7, 85, 74, 30, 64, -64, -71, 21, 1, -35, -25, -54, -73, -44},
       {-31, 38, 74, 13, -86, 35, 94, 18, 67, 40, -60, 32, -33, 83, -45, -95, -41, -1, -65, -26},
       {-97, 73, -57, 7, 86, 45, 50, 52, 73, 69, 74, -90, 33, 33, 96, -58, -1, -21, -66, 45},
       {-75, -83, -30, 67, 30, 59, -66, -46, -54, 82, -52, 60, -31, 1, -58, -17, 53, -19, 87, 3},
       {83, 51, 53, -30, -68, -96, -71, 65, 90, -55, -22, 51, 92, 50, -61, -14, -18, 80, 58, 41},
       {-4, -54, -48, -51, 1, -93, -28, -83, 52, -84, 7, 69, 80, 81, -98, 1, -93, -45, -95, -45},
       {53, -78, 16, 10, 44, -55, 76, 83, 78, -80, 47, 45, -28, -79, 12, 42, -100, 77, 73, 81},
       {-53, 92, 39, -96, -46, 44, -68, 87, 96, 48, 60, -55, -8, 26, -84, 65, -23, -44, 98, 6},
       {18, 48, -27, 45, 14, 61, -38, -38, 47, -70, -97, 50, -17, 13, 72, 54, -3, 92, 83, -41},
       {91, -9, 94, 81, -61, -66, -25, 84, -57, 55, -55, -99, -51, -71, -56, -41, -74, -90, -84, 16},
       {-76, -51, -63, 83, 21, 91, -92, -97, 23, 66, -61, 69, 96, 8, -10, -75, 25, -9, -18, 44},
       {63, -80, -21, -86, -40, -89, 23, -24, -6, 10, -67, 49, 25, 18, 49, 26, 68, -48, 55, -95},
       {-92, -62, -68, 0, 59, 81, 20, 81, -86, 88, -66, 65, -23, 70, 98, -80, -61, -74, 32, -89},
       {29, 83, 22, 55, -75, 34, -92, 9, 69, 79, 89, 77, 81, -74, -87, -95, -95, -92, 99, 29},
       {-98, 47, -30, 87, -71, -75, -51, -40, -22, 50, 52, 29, -2, -69, -18, -20, -73, -91, 17, -86},
       {-89, -24, -81, 79, -65, 63, -58, -23, 22, -25, -31, 70, -44, 97, -90, 5, 9, -84, -19, -63},
       {-36, 17, -30, 51, 78, -1, -85, -9, 6, 97, 16, 4, 25, 4, 78, 2, -62, -87, 80, 51}};
  Matrix<20, 20> matrix(tmp);
  matrix = matrix.inverted();
}