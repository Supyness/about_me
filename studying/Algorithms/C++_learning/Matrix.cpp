#include "biginteger.h"
#include <set>

using std::vector;
/*class Matrix {
 private:
  vector<vector<Rational>> matrix;
  size_t n, m;
  void resize_matrix(size_t size1, size_t size2) {
    matrix = vector<vector<Rational>>(size1, vector<Rational>(size2, 0));
    n = size1;
    m = size2;
  }
 public:
  Matrix() {
    n = 1;
    m = 1;
    matrix = {{0}};
  }
  Matrix(vector<vector<int>>& sp) {
    this->resize_matrix(sp.size(), sp[0].size());
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = 0; j < m; ++j)
        matrix[i][j] = Rational(sp[i][j]);
    }
  }
  vector<Rational>& operator[](const size_t i) {
    return matrix[i];
  }
  vector<Rational> operator[](const size_t i) const {
    return matrix[i];
  }
  Matrix& operator+=(const Matrix &matrix1) {
    if (n == matrix1.n && m == matrix1.m) {
      for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
          matrix[i][j] += matrix1[i][j];
        }
      }
    }
    return *this;
  }
  Matrix& operator-=(const Matrix &matrix1) {
    if (n == matrix1.n && m == matrix1.m) {
      for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
          matrix[i][j] -= matrix1[i][j];
        }
      }
    }
    return *this;
  }
  Matrix& operator*=(const int num) {
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = 0; j < m; ++j) {
        matrix[i][j] *= num;
      }
    }
    return *this;
  }
  Matrix& operator*=(const Matrix &matrix1) {
    if (n == matrix1.n && m == matrix1.m && n == m) {
      Matrix tmp;
      tmp.resize_matrix(n, n);
      for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
          Rational tmp1 = 0;
          for (int z = 0; z < n; ++z) {
            tmp1 += matrix[i][z] * matrix1[j][z];
          }
          tmp[i][j] = tmp1;
        }
      }
      *this = tmp;
    }
    return *this;
  }
  Matrix transponed() {
    Matrix tmp;
    tmp.resize_matrix(n, m);
    for (int i = 0; i < m; ++i) {
      for (int j = 0; j < n; ++i) {
        tmp[j][i] = matrix[i][j];
      }
    }
    return tmp;
  }
  Rational det(int now_column = 0, std::set<int> line = {}) {
    Rational tmp = 0;
    if (now_column == n - 1) {
      for (int i = 0; i < n; ++i) {
        if (line.find(i) == line.end()) return matrix[i][now_column];
      }
    }
    std::set<int> tmp1 = line;
    for (int i = 0; i < n; ++i) {
      line = tmp1;
      if (line.find(i) == line.end()) {
        line.insert(i);
        int k = std::distance(line.begin(), line.find(i));
        if (k < 0) k = 0;
        Rational tk = det(now_column + 1, line);
        tmp += matrix[i][now_column] * ((k + i) % 2 != 0 ? -1 : 1) *tk;
        std::cout << tmp.toString() << std::endl;
      }
    }
    return tmp;
  }
  bool operator!=(const Matrix &matrix2) const {
    return !(*this == matrix2);
  }
  friend bool operator==(const Matrix &matrix1, const Matrix &matrix2);
  friend Matrix operator*(const Matrix& matrix1, const Matrix& matrix2);
};
bool operator==(const Matrix &matrix1, const Matrix &matrix2) {
  if (matrix1.n == matrix2.n && matrix1.m == matrix2.m) {
    for (size_t i = 0; i < matrix1.n; ++i) {
      for (size_t j = 0; j < matrix1.m; ++j) {
        if (matrix1[i][j] != matrix2[i][j]) return false;
      }
    }
    return true;
  }
  return false;
}
Matrix operator*(const Matrix& matrix1, const Matrix& matrix2) {
  if (matrix1.m == matrix2.n) {
    Matrix tmp;
    tmp.resize_matrix(matrix1.n, matrix2.m);
    for (int i = 0; i < matrix1.n; ++i) {
      for (int j = 0; j < matrix2.m; ++j) {
        Rational tmp1 = 0;
        for (int z = 0; z < matrix1.m; ++z) {
          tmp1 += matrix1[i][z] * matrix2[j][z];
        }
        tmp[i][j] = tmp1;
      }
    }
    return tmp;
  }
}*/

int main() {
}