#include <iostream>

template <size_t N, size_t D, bool flag = false>
struct IsPrime {
  static const bool isprime = (N % D != 0) && (IsPrime<N, D + 1, (D * D >= N)>::isprime);
};

template <size_t N, size_t D>
struct IsPrime <N, D, true>{
  static const bool isprime = true;
};

template<size_t N>
class Residue {
 private:
  size_t Number = int(N);
 public:
  Residue(int k) {
    Number = k % N;
  }
  Residue<N>& operator+=(const Residue<N>& Num) {
    Number = (Number + Num.Number) % N;
    return *this;
  }
  Residue<N>& operator-=(const Residue<N>& Num) {
    Number = (Number + N - Num) % N;
    return *this;
  }
  Residue<N>& operator*=(const Residue<N>& Num) {
    Number = Number * Num.Number % N;
    return *this;
  }
  template<typename = typename std::enable_if<IsPrime<N, 2>::isprime>>
  Residue<N>& operator/=(const Residue<N>& num1) {
    size_t result = 1;
    size_t tmp = N - 2;
    while (tmp) {
      if (tmp & 1) {
        result = result * num1.Number % N;
      }
      result = result * result % N;
      tmp >>= 1;
    }
    this->Number *= result;
    this->Number %= N;
    return *this;
  }
  explicit operator int() const {
    return int(Number);
  }
};
template<size_t N>
Residue<N> operator+(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp += num2);
}
template<size_t N>
Residue<N> operator-(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp -= num2);
}
template<size_t N>
Residue<N> operator*(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp *= num2);
}
template<size_t N>
Residue<N> operator/(const Residue<N>& num1, const Residue<N>& num2) {
  Residue<N> tmp = num1;
  return (tmp /= num2);
}
