#include <iostream>
#include <vector>
#include <xmemory>
#include <memory>

template<size_t N>
class StackStorage {
 private:
  char _memory[N];
  size_t _capacity = 0;
 public:
  char *allocate(size_t cap) {
    if (_memory + N >= _memory + _capacity + cap) {
      auto tmp(_memory + _capacity);
      _capacity += cap;
      return _memory + _capacity;
    } else {
      throw std::out_of_range("out_of_range");
    }
  }
  void deallocate(const char *ptr, size_t cap) noexcept {
    if (ptr <= _memory + N) {
      if (ptr + cap == _memory + _capacity) {
        _capacity -= cap;
      }
    } else {
      throw std::out_of_range("out_of_range");
    }
  }
};

template<typename T, size_t N>
class StackAllocator {
 private:
  std::shared_ptr<StackStorage<N>> _storage;
 public:
  typedef T value_type;
  typedef T *pointer;
  typedef size_t size_type;
  StackAllocator() = default;
  StackAllocator(StackStorage<N> &_second) : _storage(std::make_shared<StackStorage<N>>(_second)) {};
  StackAllocator(const StackAllocator<T, N> &_second) : _storage(_second._storage) {};
  template<class U>
  struct rebind {
    typedef StackAllocator<U, N> other;
  };
  template<typename U, size_t M>
  friend class StackAllocator;
  template<class U>
  explicit StackAllocator(const StackAllocator<U, N> &_second)
      : _storage(_second._storage), value_type(_second.valut_type), pointer(_second.pointer) {};
  StackAllocator<T, N> &operator=(const StackAllocator<T, N> &);
  ~StackAllocator();
  pointer allocate(size_t cap) {
    return reinterpret_cast<T *>((_storage->allocate(cap * sizeof(T))));
  };
  void deallocate(pointer p, size_type cap) {
    _storage->deallocate(reinterpret_cast<char *>(p), cap * sizeof(T));
  };
};

template<typename T, size_t N>
StackAllocator<T, N>::~StackAllocator() {
  _storage.reset();
}

template<typename T, size_t N>
StackAllocator<T, N> &StackAllocator<T, N>::operator=(const StackAllocator<T, N> &_second) {
  _storage = _second._storage;
  return *this;
}
template<typename T>
struct Basenode {
  Basenode *prev;
  Basenode *next;
  Basenode() : next(nullptr), prev(nullptr) {};
};
template<typename T>
struct Node : Basenode<T> {
  T value;
  Node(const T &value) : value(value), Basenode<T>() {};
  Node() = default;
};

template<typename T, typename Alloc>
class List {
 public:
  typedef T value_type;
  typedef Alloc allocator_type;
  typedef size_t size_type;
  template<bool is_const>
  class CommonIterator;
  template<typename CommonIterator>
  class Common_reverse_iterator;
  using iterator = CommonIterator<false>;
  using const_iterator = CommonIterator<true>;
  using reverse_iterator = Common_reverse_iterator<iterator>;
  using const_reverse_iterator = Common_reverse_iterator<const_iterator>;
  Alloc get_allocator() {
    return alloc;
  }
  List() : _size(0) {};
  List(T &num) {
    Basenode<T> *prev = fakenode;
    Basenode<T> *next = fakenode;
    auto mem = std::allocator_traits<Alloc>::alloc(alloc, 1);
    Node<T> *new_node = std::allocator_traits<Alloc>::construct(mem, num, *prev, *next);
    fakenode->prev = new_node;
    fakenode->next = new_node;
    _size = 1;
    pri();
  }
  size_t size() {
    return _size;
  }
  void pri() {
    Node<T>* node = fakenode->next;
    while (node != fakenode) {
      std::cout << node->value << std::endl;
      node = node->next;
    }
  }
  void push_back(T &value) {
    Basenode<T> *prev = fakenode->prev;
    Basenode<T> *next = fakenode;
    auto mem = std::allocator_traits<Alloc>::alloc(alloc, 1);
    Node<T> *new_node = std::allocator_traits<Alloc>::construct(mem, value, *prev, *next);
    fakenode->prev = new_node;
    new_node->prev->next = new_node;
    pri();
  }
  template<bool is_const>
  class CommonIterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = T;
    using difference_type = long long;
    using pointer = typename std::conditional<is_const, const T *, T *>::type;
    using reference = typename std::conditional<is_const, const T &, T &>::type;
    CommonIterator(Node<T> *pos) : pos(pos) {};
    CommonIterator() : pos(Basenode<T>()) {};
    CommonIterator(const CommonIterator &first) : pos(first.pos) {};
    ~CommonIterator() = default;
    reference operator*() const { return *pos; }
    CommonIterator &operator++() {
      pos = pos->next;
      return *this;// evgeniy krut 2003
    }
    CommonIterator operator++(int) const {
      CommonIterator tmp = *this;
      ++(*this);
      return tmp;
    }
    pointer operator->() const {
      return pos;
    };
    CommonIterator &operator--() {
      pos = pos->prev;
      return *this;
    }
    CommonIterator operator--(int) const {
      CommonIterator tmp = *this;
      --(*this);
      return tmp;
    }
    operator const_iterator() const {
      return const_iterator(pos);
    }
    friend bool operator==(const CommonIterator &first, const CommonIterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const CommonIterator &first, const CommonIterator &second) {
      return !(first == second);
    }
   private:
    Node<T> *pos;
  };
  template<typename CommonIterator>
  class Common_reverse_iterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    Common_reverse_iterator(CommonIterator pos) : pos(pos) {};
    typename CommonIterator::reference operator*() const { return *pos; }
    Common_reverse_iterator &operator++() {
      --pos;
      return *this;
    }
    Common_reverse_iterator operator++(int) const {
      Common_reverse_iterator tmp = *this;
      ++(*this);
      return tmp;
    }
    Common_reverse_iterator &operator--() {
      ++pos;
      return *this;
    }
    Common_reverse_iterator &operator--(int) const {
      Common_reverse_iterator tmp = *this;
      --(*this);
      return tmp;
    }
    typename CommonIterator::pointer operator->() const {
      return pos.operator->();
    };
    operator const_reverse_iterator() const {
      return const_reverse_iterator(pos);
    }
    friend bool operator==(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return !(first.pos == second.pos);
    }
   private:
    CommonIterator pos;
  };
  iterator begin() {
    return iterator(fakenode->next);
  }
  iterator end() {
    return iterator(fakenode);
  }
  [[nodiscard]] const_iterator end() const {
    return cend();
  }
  [[nodiscard]] const_iterator begin() const {
    return cbegin();
  }
  [[nodiscard]] const_iterator cbegin() const {
    return const_iterator(fakenode->next);
  }
  [[nodiscard]] const_iterator cend() const {
    return const_iterator(fakenode);
  }
  reverse_iterator rbegin() {
    return reverse_iterator(fakenode->prev);
  }
  const_reverse_iterator crbegin() {
    return const_reverse_iterator(cend() - 1);
  }
  reverse_iterator rend() {
    return reverse_iterator(fakenode);
  }
  [[nodiscard]] const_reverse_iterator rend() const {
    return crend();
  }
  const_reverse_iterator crend() {
    return const_reverse_iterator(cbegin() - 1);
  }
  [[nodiscard]] const_reverse_iterator rbegin() const {
    return crbegin();
  }
 private:
  Basenode<T> *fakenode = std::allocator_traits<Alloc>(alloc, 1);
  typename std::allocator_traits<allocator_type>::template rebind_alloc<Node> alloc;
  size_t _size;
};

int main() {
  StackStorage<100'000> storage;
  StackAllocator<int, 100'000> alloc(storage);
  std::vector<int, StackAllocator<int, 100'000>> v(alloc);
}