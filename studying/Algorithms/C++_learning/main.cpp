#include <iostream>
#include <string>

char string_compare(std::string s1, std::string s2) {
  int j = s1.size() - 1;
  while (s1[j] != '.') {
    if (s1[j] == '0') j--;
    else break;
  }
  if (s1[j] == '.') {
    s1 = s1.substr(0, j);
  }
  else s1 = s1.substr(0, j + 1);
  j = s2.size() - 1;
  while (s2[j] != '.') {
    if (s2[j] == '0') j--;
    else break;
  }
  if (s2[j] == '.') s2 = s2.substr(0, j);
  else s2 = s2.substr(0, j + 1);
  if (s1[0] == '-' && s2[0] != '-') {
    if (s1 == "-0" && s2 == "0") return '=';
    return '<';
  }
  if (s1[0] != '-' && s2[0] == '-') {
    if (s1 == "0" && s2 == "-0") return '=';
    return '>';
  }
  if (s1[0] != '-' && s2[0] != '-') {
    for (int i = 0; i < std::min(s1.size(), s2.size()); ++i) {
      if (s1[i] != '.' || s2[i] != '.') {
        if (s1[i] == '.' && s2[i] != '.') {
          return '<';
        } else if (s1[i] != '.' && s2[i] == '.') {
          return '>';
        } else if (s1[i] < s2[i]) {
          return '<';
        } else if (s1[i] > s2[i]) {
          return '>';
        }
      }
    }
    if (s1.size() < s2.size()) {
      return '<';
    } else if (s1.size() == s2.size()) return '=';
    else return '>';
  }

  if (s1[0] == '-' && s2[0] == '-') {
    for (int i = 1; i < std::min(s1.size(), s2.size()); ++i) {
      if (s1[i] != '.' || s2[i] != '.') {
        if (s1[i] == '.' && s2[i] != '.') {
          return '>';
        } else if (s1[i] != '.' && s2[i] == '.') {
          return '<';
        } else if (s1[i] < s2[i]) {
          return '>';
        } else if (s1[i] > s2[i]) {
          return '<';
        }
      }
    }
    if (s1.size() < s2.size()) {
      return '>';
    } else if (s1.size() == s2.size()) return '=';
    else return '<';
  }
}

int main() {
  std::string s1, s2;
  std::getline(std::cin, s1);
  std::getline(std::cin, s2);
  std::cout << string_compare(s1, s2);
}