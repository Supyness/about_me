#include <iostream>
#include <deque>

#define ll long long
int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);
  std::deque<std::pair<ll, std::pair<ll, ll>>> S;
  ll n;
  std::cin >> n;
  ll MaxSquare = 0;
  for (ll i = 0; i < n; i++) {
    ll num;
    std::cin >> num;
    if (S.empty()) S.push_back({num, {0, i}});
    else if (S.back().first < num) {
      //std::cout << MaxSquare << ' ' << S.back().first << ' ' << S.back().second.first << ' ' << S.back().second.second << ' ' << 0 << std::endl;
      S.push_back({num, {i, i}});
    } else if (S.back().first == num) {
      //std::cout << MaxSquare << ' ' << S.back().first << ' ' << S.back().second.first << ' ' << S.back().second.second << ' ' << 1 << std::endl;
      S.push_back({num, {S.back().second.first, i}});
    } else {
      while (!S.empty() && num < S.back().first) {
        MaxSquare = std::max(MaxSquare, S.back().first * (i - S.back().second.first));
        //std::cout << MaxSquare << ' ' << S.back().first << ' ' << S.back().second.first << ' ' << S.back().second.second << ' ' << 2 << std::endl;
        S.pop_back();
      }
      if (S.empty()) S.push_back({num, {0, i}});
      else S.push_back({num, {S.back().second.second + 1, i}});
    }
    //for (auto it : S) std::cout << it.first << ' ';
    //std::cout << std::endl;
    //std::cout << MaxSquare << ' ' << S.back().first << std::endl;
  }
  ll sz = int(S.size());
  for (ll i = 0; i < sz; i++) {
    //std::cout << S.back().first << ' ' << S.back().second.first << ' ' << S.back().second.second << std::endl;
    MaxSquare = std::max(MaxSquare, S.back().first * (n - S.back().second.first));
    S.pop_back();
  }
  std::cout << MaxSquare;
}