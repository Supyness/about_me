#include <iostream>
#include <string>

struct Node {
  int val;
  Node *next;
  Node *prev;
  size_t size = 0;
};
Node *top = nullptr;
Node *front = nullptr;

void push_b(Node &deq, int x) {
  deq.size++;
  Node *n = new Node();
  n -> val = x;
  n -> next = nullptr;
  n -> prev = top;
  if (front == nullptr) {
    front = n;
    top = n;
  }
  else {
    top -> next = n;
    top = n;
  }
}

void push_f(Node &deq, int x) {
  deq.size++;
  Node *n = new Node();
  n -> val = x;
  n -> next = front;
  n -> prev = nullptr;
  if (front == nullptr) top = n;
  front -> prev = n;
  front = n;
}
void pop_b(Node &deq) {
  deq.size--;
  Node *del = top;
  top = top -> prev;
  delete del;
}

void pop_f(Node &deq) {
  deq.size--;
  Node *del = front;
  front = front -> next;
  delete del;
}

bool is_empty() {
  if (front == nullptr || top == nullptr) {
    front = nullptr;
    top = nullptr;
    return true;
  }
  else return false;
}

int siz(Node &deq) {
  return deq.size;
}
int last() {
  if (!is_empty()) return top -> val;
  else return -1;
}
int first() {
  if (!is_empty()) return front -> val;
  else return -1;
}



int main() {
  Node deque;
  std::string q; std::cin >> q;
  while (q != "exit") {
    if (q == "pushb") {
      int k; std::cin >> k;
      push_b(deque, k);
    }
    if (q == "pushf") {
      int k; std::cin >> k;
      push_f(deque, k);
    }
    if (q == "popb") {
      std::cout << last() << std::endl;
      pop_b(deque);
    }
    if (q == "popf") {
      std::cout << first() << std::endl;
      pop_f(deque);
    }
    if (q == "first") {
      std::cout << first() << std::endl;
    }
    if (q == "last") {
      std::cout << last() << std::endl;
    }
    if (q == "size") {
      std::cout << siz(deque) << std::endl;
    }
    if (q == "is_empty") {
      std::cout << is_empty() << std::endl;
    }
    std::cin >> q;
  }
}
