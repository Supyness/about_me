#include <vector>
#include <iostream>
#include <algorithm>

double evklid(std::pair<int, int>& p1, std::pair<int, int>& p2) {
  return sqrt((p1.first - p2.first) * (p1.first - p2.first) + (p1.second - p2.second) * (p1.second - p2.second)) / 2;
}

std::pair<int, double> func(std::vector<std::pair<int, int>>& sp, double dist, std::vector<int>& new_sp) {
  int index = 0;
  double dist1 = 1e9;
  for (int i = 0; i < sp.size() - 1; ++i) {
    if (new_sp[i] == new_sp[i + 1]) {
      double dist2 = evklid(sp[i], sp[i + 1]);
      if (dist2 < dist1) {
        dist1 = dist2;
        index = i;
      }
    }
  }
  if (dist1 < dist) {
    return {-1, dist1};
  }
  else {
    return {index, dist1};
  }
}

int main() {
  int n; std::cin >> n;
  std::vector<std::pair<int, int>> sp;
  for (int i = 0; i < n; ++i) {
    std::pair<int, int> p;
    std::cin >> p.first;
    std::cin >> p.second;
    sp.push_back(p);
  }
  std::sort(sp.begin(), sp.end());
  std::vector<int> new_sp(n, 1);
  double dist = 0;
  int index = 0;
  std::pair<int, double> pair = func(sp, dist, new_sp);
  index = pair.first;
  dist = pair.second;
  std::cout << index << ' ' << dist;
  while (index != -1) {
    new_sp[index] = 2;
    pair = func(sp, dist, new_sp);
    std::cout << pair.first << ' ' << pair.second << std::endl;
    index = pair.first;
    dist = pair.second;
  }
  for (auto it : new_sp) std::cout << it << ' ';
}