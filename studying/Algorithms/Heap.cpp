#include <iostream>
#include <vector>
#include <string>

void siftup(long long u, std::vector<long long>& mas, bool lowhigh) {
  if (lowhigh) {
    while (u != 1) {
      if (mas[u] > mas[u / 2]) {
        std::swap(mas[u], mas[u / 2]);
        u /= 2;
      } else break;
    }
  }
  else {
    while (u != 1) {
      if (mas[u] < mas[u / 2]) {
        std::swap(mas[u], mas[u / 2]);
        u /= 2;
      } else break;
    }
  }
}

void siftdown(long long u, std::vector<long long>& mas, bool lowhigh, int size) {
  if (lowhigh) {
    while (2 * u < size) {
      long long v = 2 * u;
      if (2 * u + 1 < size && mas[2 * u + 1] > mas[2 * u]) {
        v = 2 * u + 1;
      }
      if (mas[v] > mas[u]) {
        std::swap(mas[v], mas[u]);
      } else break;
      u = v;
    }
  }
  else {
    while (2 * u < size) {
      long long v = 2 * u;
      if (2 * u + 1 < size && mas[2 * u + 1] < mas[2 * u]) {
        v = 2 * u + 1;
      }
      if (mas[v] < mas[u]) {
        std::swap(mas[v], mas[u]);
      } else break;
      u = v;
    }
  }
}
void GetMin(std::vector<int>& sizes, std::vector<std::vector<long long>>& heap) {
  while (heap[0][1] == heap[2][1] && sizes[2] > 1 && sizes[0] > 1) {
    --sizes[0];
    --sizes[2];
    heap[0][1] = heap[0][sizes[0]];
    heap[2][1] = heap[2][sizes[2]];
    siftdown(1, heap[0], 0, sizes[0]);
    siftdown(1, heap[2], 0, sizes[2]);
  }
  heap[3][sizes[3]] = heap[0][1];
  siftup(sizes[3], heap[3], 1);
  ++sizes[3];
  std::cout << heap[0][1] << std::endl;
  --sizes[0];
  heap[0][1] = heap[0][sizes[0]];
  siftdown(1, heap[0], 0, sizes[0]);
}

void GetMax(std::vector<int>& sizes, std::vector<std::vector<long long>>& heap) {
  while ((heap[1][1] == heap[3][1]) && (sizes[1] > 1) && (sizes[3] > 1)) {
    --sizes[1];
    --sizes[3];
    heap[1][1] = heap[1][sizes[1]];
    heap[3][1] = heap[3][sizes[3]];
    siftdown(1, heap[1], 1, sizes[1]);
    siftdown(1, heap[3], 1, sizes[3]);
  }
  heap[2][sizes[2]] = heap[1][1];
  siftup(sizes[2], heap[2], 0);
  ++sizes[2];
  std::cout << heap[1][1] << std::endl;
  --sizes[1];
  heap[1][1] = heap[1][sizes[1]];
  siftdown(1, heap[1], 1, sizes[1]);
}


int main() {
  int n; std::cin >> n;
  std::vector<int> sz(4, 1);
  std::string query;
  std::vector<std::vector<long long>> heaps(4, std::vector<long long>(n + 1, 0));
  for (int i = 0; i < n; i++) {
    std::cin >> query;
    if (query.substr(0, 6) == "Insert") {
      long long number = std::stoi(query.substr(7, query.size() - 8));
      heaps[0][sz[0]] = number;
      heaps[1][sz[1]] = number;
      ++sz[0];
      ++sz[1];
      siftup(sz[0] - 1, heaps[0], 0);
      siftup(sz[1] - 1, heaps[1], 1);
    }
    if (query.substr(0, 6) == "GetMin") {
      GetMin(sz, heaps);
    }
    if (query.substr(0, 6) == "GetMax") {
      GetMax(sz, heaps);
    }
    /*for (auto it : heaps) {
      for (auto itt : it) std::cout << itt << ' ';
      std::cout << std::endl;
    }
    for (auto it : sz) std::cout << it << ' ';
    std::cout << std::endl;*/
  }
}