#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iostream>
#include "Sequence.h"
using namespace std;

template <class T> class LinkedList {
 private:
  class Node {
    friend class LinkedList<T>;
   private:
    Node* next;
    T value;
    Node(): next(nullptr) {};
    ~Node() {}
  };
  Node* head;
  int count;
 public:
  LinkedList() {
    head = nullptr;
    count = 0;
  }
  LinkedList(int count) {
    head = nullptr;
    this->count = count;
  }
  LinkedList(T* items, int count) {
    LinkedList<T> list;
    head = nullptr;
    this->count = 0;
    for (int i = 0; i < count; i++) {
      Node* add = new Node();
      add->next = nullptr;
      add->value = items[i];
      if (head == nullptr) {
        head = add;
      }
      else {
        Node* current;
        for (current = head; current->next != nullptr; current = current->next) {
          current->next = add;
        }
      }
      this->count += 1;
    }
  }
  LinkedList(const LinkedList<T> & list) {
    Node* new_head = new Node();
    new_head->value = list.head->value;
    head = new_head;
    count = list.GetLength();
    Node* current = new_head;
    for (int i = 1; i < list.GetLength(); i++) {
      Node* add = new Node();
      add->next = nullptr;
      add->value = list.Get(i);
      current->next = add;
      current = current->next;
    }
  }
  ~LinkedList() {
    Node* tmp = head;
    Node* del = head;
    while (del != nullptr) {
      tmp = tmp->next;
      delete del;
      del = tmp;
    }
    head = nullptr;
    count = 0;
  }
  T GetFirst() {
    if (count == 0) {
      cout << "Index out of range" << endl;
    }
    else {
      return head->value;
    }
  }
  T GetLast() {
    if (count == 0) {
      cout << "Index out of range" << endl;
    }
    else {
      return Get(count - 1);
    }
  }
  T Get(int index) const {
    if ((index < 0) || (index > count)) {
      cout << "Index out of range" << endl;
    }
    else {
      Node* current = head;
      for (int i = 0; i < index; i++) {
        current = current->next;
      }
      return current->value;
    }
  }
  int GetLength() const {
    return count;
  }
  LinkedList<T> GetSubSeq(int startindex, int endindex) {
    LinkedList list = LinkedList();
    if ((startindex < 0) || (startindex > endindex) || (endindex > count)) {
      cout << "Index out of range" << endl;
    }
    else {
      if ((startindex < count) && (endindex < count)) {
        for (int i = startindex; i <= endindex; i++) {
          list.Append(this->Get(i));
        }
      }
    }
    return list;
  }
  void Append(T data) {
    Node* add = new Node();
    add->value = data;
    if (count == 0) {
      head = add;
    }
    else {
      Node* current = head;
      while (current->next != nullptr) {
        current = current->next;
      }
      current->next = add;
    }
    count++;
  }
  void Prepend(T data) {
    Node* add = new Node();
    add->value = data;
    if (count == 0) {
      head = add;
    }
    else {
      add->next = head;
      head = add;
    }
    count++;
  }
  void InsertAt(T item, int index) {
    if ((index < 0) || (index > count)) {
      cout << "Index out of range" << endl;
// cout << index << " " << count << endl;
    }
    else {
      if (index == 0) {
        Prepend(item);
      }
      if (index == count) {
        Append(item);
      }
      else {
        Node* current = head;
        Node* add = new Node();
        add->value = item;
        for (int i = 0; i < index; i++) {
          current = current->next;
        }
        Node *tmp = current->next;
        current->next = add;
        add->next = tmp;
        count++;
      }
    }
  }
  LinkedList<T>* Concat(LinkedList<T> list) {
    auto* result = new LinkedList<T>;
    *result = *this;
    for (int i = 0; i < list.GetLength(); i++) {
      result->Append(list[i]);
    }
    return result;
  }
  T& operator[](int index) {
    if ((index < 0) || (index >= count)) {
      cout << "Index out of range" << endl;
    }
    else {
      Node* tmp = head;
      for (int i = 0; i < index; i++) {
        tmp = tmp->next;
      }
      return tmp->value;
    }
  }
};

#endif //LINKEDLIST_H