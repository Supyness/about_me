#ifndef LINKEDLISTSEQUENCE_H
#define LINKEDLISTSEQUENCE_H
#include "Sequence.h"
#include "LinkedList.h"
#include <iostream>
using namespace std;

template <typename T>
class ListSequence: public Sequence<T> {
 private:
  LinkedList<T> *list;
 public:
  ListSequence() {
    list = new LinkedList<T>(0);
  }
  ListSequence(int count) {
    list = new LinkedList<T>(count);
  }
  ListSequence(T* items, int count) {
    list = new LinkedList<T>(items, count);
  }
  ListSequence(const LinkedList<T> &list) {
    this->list = new LinkedList<T>(list);
  }
  ~ListSequence() {
    delete list;
  }
  int GetLength() override {
    return list->GetLength();
  }
  T GetFirst() override {
    return list->Get(0);
  }
  T GetLast() override {
    return list->Get(list->GetLength() - 1);
  }
  T Get(int index) override {
    return list->Get(index);
  }
  void Set(T item, int index) {
    if (index >= this->GetLength() || index < 0) {
      cout << "index out of range";
    }
    this->InsertAt(item, index);
    this->Remove(index + 1);
  }
  Sequence<T>* GetSubSeq(int startindex, int endindex) override {
    auto* res = new ListSequence<T>(list->GetSubSeq(startindex, endindex));
    return res;
  }
  void InsertAt(T item, int index) override {
    list->InsertAt(item, index);
  }
  void Append(T item) override {
    list->Append(item);
  }
  void Prepend(T item) override {
    list->Prepend(item);
  }
  Sequence<T>* Concat(Sequence<T>* list1) override {
    auto* res_list = new ListSequence<T>;
    *(res_list->list) = *(list);
    for (int i = 0; i < list->GetLength(); i++) {
      res_list->Append(list->Get(i));
    }
    return res_list;
  }
  T& operator[](int index) override {
    if ((index < 0) || (index > list->GetLength())) {
      cout << "Index out of range" << endl;
    }
    else {
      return (*list)[index];
    }
  }
};

template<typename T>
ostream & operator << (ostream & out, ListSequence<T>* a) {
  for (int i = 0; i < a->GetLength(); ++i) {
    out << a->Get(i) << " " << i << " ";
  }
  return out;
}


#endif //LINKEDLISTSEQUENCE_H