#ifndef SEQUENCE_H
#define SEQUENCE_H
#include <iostream>
#include "DynamicArraySequence.h"
#include "LinkedListSequence.h"

using namespace std;

template <typename T> class Sequence {
 public:
  virtual T GetFirst() = 0;
  virtual T GetLast() = 0;
  virtual T Get(int index) = 0;
  virtual Sequence<T> *GetSubSeq(int startindex, int endindex) = 0;
  virtual int GetLength() = 0;
  virtual T& operator[](int index) = 0;
  virtual void Append(T item) = 0;
  virtual void Prepend(T item) = 0;
  virtual void InsertAt(T item, int index) = 0;
  virtual void Delete(/*///*/) = 0;
  virtual void Set(T item, int index) = 0;
  virtual Sequence<T>* Concat(Sequence<T>*list) = 0;
  void map(T (*mapFunc)(T)) {
    for (int i = 0; i < GetLength(); i++) {
      InsertAt(mapFunc(Get(i)), i);
    }
  }
};
template <class T>
std::ostream& operator<<(std::ostream & out, Sequence<T>* seq)
{
  for (int i = 0; i < seq->GetLength(); ++i) {
    out << seq->Get(i) << " ";
  }
  return out;
}

#endif //SEQUENCE_H