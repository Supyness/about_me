#include <iostream>
#include <vector>

#define ll long long
 ll k = 0;
std::vector<int> Merge(std::vector<int> mas1, std::vector<int> mas2) {
  int i = 0, j = 0;
  std::vector<int> mas3;
  while (i < mas1.size() && j < mas2.size()) {
    if (mas1[i] < mas2[j]) {
      k += j;
      mas3.push_back(mas1[i]);
      i++;
    }
    else {
      mas3.push_back(mas2[j]);
      j++;
    }
  }
  while (i < mas1.size()) {
    k += j;
    mas3.push_back(mas1[i]);
    i++;
  }
  while (j < mas2.size()) {
    mas3.push_back(mas2[j]);
    j++;
  }
  return mas3;
}
std::vector<int> QS(std::vector<int> arr) {
  if (arr.size() > 1) {
    std::vector<int> arr1, arr2;
    for (int it = 0; it < arr.size() / 2; it++) arr1.push_back(arr[it]);
    for (int it = arr.size() / 2; it < arr.size(); it++) arr2.push_back(arr[it]);
    return Merge(QS(arr1), QS(arr2));
  }
  else return arr;
}

int main() {
  freopen("inverse.in", "r", stdin);
  freopen("inverse.out", "w", stdout);
  int n;
  std::cin >> n;
  std::vector<int> arr(n);
  for (int i = 0; i < n; i++) std::cin >> arr[i];
  QS(arr);
  std::cout << k;
}