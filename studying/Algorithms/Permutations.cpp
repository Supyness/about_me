#include <iostream>
#include <set>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <windows.h>
#include <algorithm>

using namespace std;

void nextPermutation(vector<int>& nums) {
  bool flag = false;
  int index = -1;
  for (int i = nums.size() - 2; i >= 0; --i) {
    int cur_index = -1, min = 101;
    for (int j = nums.size() - 1; j > i; --j) {
      if (nums[i] < nums[j] && nums[j] < min) {
        flag = true;
        index = i;
        min = nums[j];
        cur_index = j;
      }
    }
    if (flag) {
      swap(nums[i], nums[cur_index]);
      sort(nums.begin() + i + 1, nums.end());
      break;
    }
  }
  if (!flag) {
    sort(nums.begin(), nums.end());
  }
}

int main() {
  int n, k; cin >> n >> k;
  vector<int> dp(n, 0);
  for (int i = 0; i < n; ++i) {
    dp[i] = i + 1;
  }
  for (int i = 1; i < k; ++i) {
    nextPermutation(dp);
  }
  string ans;
  for (int i = 0; i < n; ++i) {
    ans += to_string(dp[i]);
  }
  cout << ans;
}