#include <iostream>
#include <stack>
#include <string>
#include <vector>

std::vector<std::string> arr1;
std::vector<std::string> arr2;

struct Node {
  int val;
  Node* prev;
  size_t size = 0;
};
Node* top = nullptr;

void push(Node &stack, int x) {
  stack.size++;
  Node* n = new Node();
  n -> val = x;
  n -> prev = top;
  top = n;
}

bool is_empty() {
  return top == nullptr;
}

int back() {
  return top -> val;
}

int pop(Node &stack) {
  if (is_empty()) {
    return -1;
  }
  else {
    stack.size--;
    Node* del = top;
    top = top -> prev;
    int t = del->val;
    delete del;
    return t;
  }
}

int main() {
  std::stack<int> p;
  std::string q; std::cin >> q;
  while (q != "exit") {
    if (q == "push") {
      int a; std::cin >> a;
      p.push(a);
      arr1.push_back("ok");
      //std::cout << "ok" << ' ';
    }
    if (q == "pop") {
      if (!p.empty()) {
        arr1.push_back(std::to_string(p.top()));
//        std::cout << p.top() << ' ';
        p.pop();
      }
      else {
        arr1.push_back("error");
//        std::cout << "error" << ' ';
      }
    }
    if (q == "clear") {
      p = {};
      arr1.push_back("ok");
//      std::cout << "ok" << ' ';
    }
    if (q == "size") std::cout << p.size() << ' ';
    if (q == "back") {
      if (!p.empty()) {
        arr1.push_back(std::to_string(p.top()));
//        std::cout << p.top() << ' ';
      }
      else {
        arr1.push_back("error");
//        std::cout << "error" << ' ';
      }
    }
    std::cin >> q;
  }
  arr1.push_back("bye");
//  std::cout << "bye";
  Node stack;
  std::string query;
  std::vector<std::string> arr;
  //std::cin >> query;
  while (query != "exit") {
    if (query == "push") {
      int num; std::cin >> num;
      push(stack, num);
      arr2.push_back("ok");
//      std::cout << "ok" << " ";
    }
    else if (query == "pop") {
      int last = pop(stack);
      if (last == -1) {
        arr2.push_back("error");
//        std::cout << "error" << " ";
      }
      else {
        arr2.push_back(std::to_string(last));
//        std::cout << last << " ";
      }
    }
    else if (query == "back") {
      int tm = back();
      if (tm == -1) {
        arr2.push_back("error");
//        std::cout << "error" << " ";
      }
      else {
        arr2.push_back(std::to_string(tm));
//        std::cout << tm << " ";
      }
    }
    else if (query == "clear") {
      while (!is_empty()) {
        pop(stack);
      }
      arr2.push_back("ok");
//      std::cout << "ok" << " ";
    }
    else if (query == "size") {
      arr2.push_back(std::to_string(stack.size));
//      std::cout << stack.size << " ";
    }
    std::cin >> query;
  }
  while (!is_empty()) pop(stack);
  arr2.push_back("bye");
//  std::cout << "bye";
  bool flag = true;
  for (int i = 0; i < arr1.size(); i++) {
    if (arr1[i] != arr2[i]) {
      flag = false;
      std::cout << "no";
      break;
    }
  }
  if (flag) std::cout << "yes";
}
