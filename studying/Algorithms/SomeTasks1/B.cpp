#include <iostream>
#include <vector>
#include <algorithm>

int main() {
  int n; std::cin >> n;
  std::vector<int> sp(n);
  int t;
  int counter = 0;
  for (int i = 0; i < n; ++i) std::cin >> sp[i];
  std::vector<int> dp(n, -INT_MAX);
  std::vector<int> active_position(n, -1);
  std::vector<int> previous_position(n, 0);
  int max_length = 0;
  for (int i = 0; i < n; ++i) {
    int l = 0, r = n;
    while (r - l > 1) {
      int m = (r + l) / 2;
      if (sp[i] > dp[m]) {
        r = m;
      }
      else l = m;
    }
    if (sp[i] < dp[l]) ++l;
    dp[l] = sp[i];
    active_position[l + 1] = i;
    previous_position[i] = active_position[l];
    max_length = std::max(max_length, l + 1);
  }
  std::vector<int> ans;
  int position = active_position[max_length];
  while (position != -1) {
    ans.push_back(position);
    position = previous_position[position];
  }
  std::sort(ans.begin(), ans.end());
  std::cout << max_length << std::endl;
  for (auto it : ans) std::cout << it + 1 << ' ';
}
