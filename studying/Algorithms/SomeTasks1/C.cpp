#include <iostream>
#include <vector>

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n, m; std::cin >> n >> m;
  std::vector<long long> sp(n);
  for (long long i = 0; i < n; ++i) std::cin >> sp[i];
  std::vector<std::vector<std::pair<std::vector<long long>, long long>>> dp(n + 1, std::vector<std::pair<std::vector<long long>, long long>>(m + 1, std::pair<std::vector<long long>, long long>()));
  for (long long i = 0; i < n + 1; ++i) {
    for (long long j = 0; j < m + 1; ++j) {
      dp[i][j].second = 0;
    }
  }
  dp[1][1].second = 0;
  for (long long i = 1; i < n + 1; ++i) {
    for (long long j = 1; j < i; ++j) {
      dp[i][1].second += sp[i - 1] - sp[j - 1];
    }
    dp[i][1].first.push_back(i);
  }
  for (long long i = 1; i < n + 1; ++i) {
    for (long long j = 2; j < std::min(i, m + 1); ++ j) {
      long long min_ans = LLONG_MAX, ind = 0;
      for (long long z = 1; z < i; ++z) {
        long long add = 0;
        for (long long k = z + 1; k < i; ++k) {
          add += std::min(sp[i - 1] - sp[k - 1], sp[k - 1] - sp[z - 1]);
        }
        if (min_ans > add + dp[z][j - 1].second) {
          ind = z;
          min_ans = add + dp[z][j - 1].second;
        }
      }
      dp[i][j].second = min_ans;
      for (auto it : dp[ind][j - 1].first) dp[i][j].first.push_back(it);
      dp[i][j].first.push_back(i);
    }
  }
  for (long long i = 1; i < n + 1; ++i) {
    for (long long j = i + 1; j < n + 1; ++j) {
      dp[i][m].second += sp[j - 1] - sp[i - 1];
    }
  }
  long long ind = 0;
  long long ans = LLONG_MAX;
  for (long long i = 1; i < n + 1; ++i) {
    if (ans > dp[i][m].second) {
      ind = i;
      ans = dp[i][m].second;
    }
  }
  std::cout << dp[ind][m].second << std::endl;
  for (auto it : dp[ind][m].first) std::cout << sp[it - 1] << ' ';
}