#include <iostream>
#include <vector>

int main() {
  int n, k; std::cin >> n >> k;
  if (k == 0 && n == 1) {
    std::cout << 0;
    return 0;
  }
  if (k == 0 && n != 1) {
    std::cout << -1;
    return 0;
  }
  if (k == 1 && n == 1) {
    std::cout << 0;
    return 0;
  }
  if (k == 1 && n != 1) {
    std::cout << n - 1;
    return 0;
  }
  int counter = 0, tmp = 1;
  while (tmp < n) {
    tmp *= 2;
    ++counter;
  }
  if (counter <= k)  {
    std::cout << counter;
    return 0;
  }
  std::vector<std::vector<int>> dp(450, std::vector<int>(k + 1, 1));
  for (int i = 0; i < 450; ++i) {
    dp[i][1] = i + 1;
  }
  for (int i = 1; i < 450; ++i) {
    bool flag = false;
    for (int j = 2; j < k + 1; ++j) {
      dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
      if (dp[i][j] >= n) {
        std::cout << i;
        flag = true;
        break;
      }
    }
    if (flag) return 0;
  }
}