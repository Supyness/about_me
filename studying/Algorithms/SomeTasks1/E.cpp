#include <iostream>
#include <vector>

int main() {
  long long n; std::cin >> n;
  std::vector<long long> dp(11, 0);
  dp[0] = 1;
  for (long long i = 1; i < 11; ++i) {
    dp[i] = dp[i - 1] * 2;
  }
  for (long long i = 1; i < 11; ++i) {
    dp[i] += dp[i - 1];
  }
  std::vector<std::vector<std::pair<std::vector<long long>, std::vector<long long>>>> dp_ans(n + 1, std::vector<std::pair<std::vector<long long>, std::vector<long long>>>(2, std::pair<std::vector<long long>, std::vector<long long>>(std::vector<long long>(n + 1, 0), std::vector<long long>(n + 1, 0))));
  long long counter = 1;
  for (long long i = 1; i < n + 1; ++i) {
    dp_ans[i][1].first[i] = 1;
    for (long long j = 1; j < n + 1; ++j) {
      dp_ans[i][1].second[j] = dp_ans[i][1].second[j - 1] + dp_ans[i][1].first[j];
    }

  }
  long long ans = 0;
  for (long long i = 2; i < 11; ++i) {
    for (long long j = 0; j < n + 1; ++j) {
      dp_ans[j][i % 2].first.assign(n + 1, 0);
      dp_ans[j][i % 2].second.assign(n + 1, 0);
    }
    for (long long j = dp[i - 1]; j < n + 1; ++j) {
      for (long long k = 1; k < n + 1; ++k) {
        std::vector<long long> pros(i, k);
        for (long long t = 1; t < i; ++t) {
          pros[t] = pros[t - 1] * 2;
        }
        for (long long t = 1; t < i; ++t) pros[t] += pros[t - 1];
        if (j >= pros[i - 1]) {
          dp_ans[j][i % 2].first[k] += dp_ans[j - k][(i - 1) % 2].second[n] - dp_ans[j - k][(i - 1) % 2].second[2 * k - 1];
        }
        else {
          break;
        }
      }
      for (long long k = 1; k < n + 1; ++k) {
        dp_ans[j][i % 2].second[k] = dp_ans[j][i % 2].second[k - 1] + dp_ans[j][i % 2].first[k];
      }
    }
    ans += dp_ans[n][i % 2].second[n];
  }
  std::cout << ans + 1;
}