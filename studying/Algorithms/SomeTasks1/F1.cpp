#pragma optimize( "", off )
#include <iostream>
#include <vector>

int main() {
  /*std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);*/
  int n; std::cin >> n;
  std::vector<std::vector<long long>> dp(n + 1, std::vector<long long>(n + 1, 0));
  long long answer = 0;
  for (int i = 1; i <= n / 2; ++i) {
    for (int j = 1; j <= n - i; ++j) {
      for (int z = 2 * i - 1; z >= i; --z) {
        if (z < j) {
          dp[j][z] = dp[j - z][z + 1] + dp[j][z + 1];
        }
        else if (z == j){
          dp[j][z] = 1;
        }
      }
    }
    answer += (dp[n - i][i + 1]);
  }
  std::cout << answer + 1;
}