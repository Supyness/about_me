#include <iostream>
#include <vector>

int main() {
  int n, m; std::cin >> n;
  std::vector<int> sp1(n + 1);
  for (int i = 0; i < n; ++i) {
    std::cin >> sp1[i + 1];
  }
  std::cin >> m;
  std::vector<int> sp2(m + 1);
  for (int i = 0; i < m; ++i) {
    std::cin >> sp2[i + 1];
  }
  std::vector<std::vector<int>> dp(n + 1, std::vector(m + 1, 0));
  for (int i = 1; i < n + 1; ++i) {
    for (int j = 1; j < m + 1; ++j) {
      if (sp1[i] == sp2[j]) {
        dp[i][j] = std::max(std::max(dp[i - 1][j - 1] + 1, dp[i - 1][j]), dp[i][j - 1]);
      }
      else {
        dp[i][j] = std::max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }
  std::cout << dp[n][m] << std::endl;
}