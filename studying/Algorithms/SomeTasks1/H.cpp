#include <iostream>
#include <vector>
#include <string>

int main() {
  std::string sp1;
  std::getline(std::cin, sp1);
  std::string sp2;
  std::getline(std::cin, sp2);
  std::vector<int> dp1(sp2.length() + 1, 0);
  const int c = 200;
  std::vector<std::vector<int>> tmp;
  for (int i = 1; i < sp1.length() + 1; ++i) {
    std::vector<int> dp2(sp2.length() + 1, 0);
    for (int j = 1; j < sp2.length() + 1; ++j) {
      if (sp1[i - 1] == sp2[j - 1]) {
        dp2[j] = dp1[j - 1] + 1;
      }
      else {
        if (dp1[j] >= dp2[j - 1]) {
          dp2[j] = dp1[j];
        }
        else {
          dp2[j] = dp2[j - 1];
        }
      }
    }
    if (i % c == 1) {
      tmp.push_back(dp2);
    }
    std::swap(dp1, dp2);
  }
  std::vector<std::vector<int>> new_arr(c + 1, std::vector<int>(sp2.length() + 1, 0));
  std::string ans;
  int t = sp2.length();
  int counter = (sp1.length() % c != 0 ? sp1.length() / c * c + 1 : sp1.length() / c * c - c + 1);
  for (int cc = 0; cc < sp1.length() / c + (sp1.length() % c != 0 ? 1 : 0); ++cc) {
    new_arr[0] = tmp[counter / c];
    int d = counter;
    for (int i = 1; i < (cc == 0 ? sp1.length() - sp1.length() / c * c + (sp1.length() % c == 0 ? c : 0): c); ++i) {
      for (int j = 1; j < sp2.length() + 1; ++j) {
        if (sp1[counter - 1] == sp2[j - 1]) {
          new_arr[i][j] = new_arr[i - 1][j - 1] + 1;
        } else {
          if (new_arr[i - 1][j] >= new_arr[i][j - 1]) {
            new_arr[i][j] = new_arr[i - 1][j];
          } else {
            new_arr[i][j] = new_arr[i][j - 1];
          }
        }
      }
      ++counter;
    }
    int k = (cc == 0 ? sp1.length() - sp1.length() / c * c + (sp1.length() % c == 0 ? c : 0) - 1 : c);
    while (k > 0) {
      if (sp1[counter - 1] == sp2[t - 1]) {
        ans += sp1[counter - 1];
        --counter;
        --k;
        --t;
      }
      else {
        if (new_arr[k - 1][t] >= new_arr[k][t - 1]) {
          --counter;
          --k;
        } else {
          --t;
        }
      }
    }
    new_arr[c] = new_arr[0];
    counter = d - c;
  }
  std::reverse(ans.begin(), ans.end());
  std::cout << ans;
}