#include <iostream>
#include <string>
#include <vector>

std::vector<int> LCS(std::string& sp1, std::string& sp2, int l, int r, int l1, int r1, bool rev) {
  if (!rev) {
    std::vector<int> dp1(r1 - l1 + 2, 0);
    for (int i = l + 1; i <= r + 1; ++i) {
      std::vector<int> dp2(r1 - l1 + 2, 0);
      for (int j = l1 + 1; j <= r1 + 1; ++j) {
        dp2[j - l1] = std::max(dp1[j - l1], dp2[j - l1 - 1]);
        if (sp1[i - 1] == sp2[j - 1]) {
          dp2[j - l1] = dp1[j - 1 - l1] + 1;
        }
      }
      std::swap(dp1, dp2);
    }
    return dp1;
  }
  else {
    std::vector<int> dp1(r1 - l1 + 2, 0);
    for (int i = l + 1; i <= r + 1; ++i) {
      std::vector<int> dp2(r1 - l1 + 2, 0);
      for (int j = l1 + 1; j <= r1 + 1; ++j) {
        dp2[j - l1] = std::max(dp1[j - l1], dp2[j - l1 - 1]);
        if (sp1[r - i + l + 1] == sp2[r1 - j + l1 + 1]) {
          dp2[j - l1] = dp1[j - l1 - 1] + 1;
        }
      }
      std::swap(dp1, dp2);
    }
    return dp1;
  }
}

void solute(std::string& s1, std::string& s2, int l1, int r1, int l2, int r2, std::string& ans) {
  if (r1 == l1) {
    for (int i = l2; i <= r2; ++i) {
      if (s2[i] == s1[l1]) {
        ans += s1[l1];
        break;
      }
    }
    return;
  }
  int m = (r1 + l1) / 2;
  std::vector<int> f = LCS(s1, s2, l1, m, l2, r2, 0);
  std::vector<int> s = LCS(s1, s2, m + 1, r1, l2, r2, 1);
  int max_ind = s[0], it_m = 0;
  for (int i = 0; i <= r2 - l2 + 1; ++i) {
    if (f[i] + s[r2 - l2 - i + 1] > max_ind) {
      max_ind = f[i] + s[r2 - l2 - i + 1];
      it_m = i;
    }
  }
  if (it_m != 0) {
    solute(s1, s2, l1, m, l2, l2 + it_m - 1, ans);
  }
  solute(s1, s2, m + 1, r1, l2 + it_m, r2, ans);
}

int main() {
  std::string s1, s2;
  std::cin >> s1 >> s2;
  std::string ans;
  solute(s1, s2, 0, s1.length() - 1, 0, s2.length() - 1, ans);
  std::cout << ans;
}