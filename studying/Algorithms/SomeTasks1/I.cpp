#include <iostream>
#include <vector>

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  std::vector<int> sp1(n), sp2(m);
  for (int i = 0; i < n; ++i) {
    std::cin >> sp1[i];
  }
  for (int i = 0; i < m; ++i) {
    std::cin >> sp2[i];
  }
  std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1, 0));
  for (int i = 1; i < n + 1; ++i) {
    int best_value = 0;
    for (int j = 1; j < m + 1; ++j) {
      dp[i][j] = dp[i - 1][j];
      if (sp1[i - 1] == sp2[j - 1] && dp[i - 1][j] < best_value + 1) {
        dp[i][j] = best_value + 1;
      }
      if (sp1[i - 1] > sp2[j - 1] && dp[i - 1][j] > best_value) {
        best_value = dp[i - 1][j];
      }
    }
  }
  int ans = 0;
  for (int j = 0; j < m + 1; ++j) {
    ans = std::max(ans, dp[n][j]);
  }
  std::cout << ans << std::endl;
}