#include <iostream>
#include <vector>

int main() {
  int per = 1000000000 + 7;
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n; std::cin >> n;
  std::vector<long long> sp(n);
  long long max_num = 0;
  for (long long i = 0; i < n; ++i) {
    long long elem; std::cin >> elem;
    sp[i] = elem;
    max_num = std::max(max_num, elem);

  }
  std::vector<long long> dp(n + 1, 0);
  dp[0] = 1;
  std::vector<long long> indexes(max_num + 1, -1);
  for (long long i = 1; i < n + 1; ++i) {
    if (indexes[sp[i - 1]] == -1) {
      dp[i] = (dp[i - 1] * 2) % per;
    }
    else {
      dp[i] = (dp[i - 1] * 2 - dp[indexes[sp[i - 1]] - 1] + per) % per;
    }
    indexes[sp[i - 1]] = i;
  }
  std::cout << dp[n] - 1;
}