#include <iostream>
#include <vector>

int main() {
  int s, n; std::cin >> s >> n;
  std::vector<int> sp(n + 1);
  for (int i = 0; i < n; ++i) {
    std::cin >> sp[i + 1];
  }
  std::vector<int> dp(s + 1, 0);
  dp[0] = 1;
  for (int i = 1; i < n + 1; ++i) {
    for (int j = s; j >= sp[i]; --j) {
      if (dp[j - sp[i]] == 1) {
        dp[j] = 1;
      }
    }
  }
  for (int i = s; i > -1; --i) {
    if (dp[i] != 0) {
      std::cout << i;
      break;
    }
  }
}