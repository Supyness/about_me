#include <iostream>
#include <vector>
#include <algorithm>
#pragma optimize( "", off )

int main() {
  long long n; std::cin >> n;
  long long counter = 0;
  std::vector<long long> sp(n);
  for (long long i = 0; i < n; ++i) {
    std::cin >> sp[i];
  }
  long long s; std::cin >> s;
  std::vector<long long> arr;
  int x = (1 << (n / 2 + n % 2));
  std::vector<bool> masks(n / 2 + n % 2, 0);
  for (int i = 0; i < x; ++i) {
    int t = i, count = 0;
    while (t != 0) {
      masks[count] = t % 2;
      t >>= 1;
      ++count;
    }
    long long weight = 0;
    for (int j = n / 2; j < n; ++j) {
      if (masks[j - n / 2] == 1) {
        weight += sp[j];
      }
    }
    if (weight <= s && weight != 0) arr.push_back(weight);
  }
  std::sort(arr.begin(), arr.end());
  counter += arr.size();
  x = (1 << (n / 2));
  masks.assign(n / 2, 0);
  for (int i = 0; i < x; ++i) {
    int t = i, count = 0;
    while (t > 0) {
      masks[count] = t % 2;
      t >>= 1;
      ++count;
    }
    long long weight = 0;
    for (int j = 0; j < n / 2; ++j) {
      if (masks[j] == 1) {
        weight += sp[j];
      }
    }
    if (weight <= s && weight != 0) {
      int l = 0, r = arr.size();
      while (r - l > 1) {
        int m = (l + r) / 2;
        if (weight + arr[m] > s) {
          r = m;
        }
        else {
          l = m;
        }
      }
      if (arr.size() <= l) counter += 1;
      else {
        counter += l + (weight + arr[l] > s ? 1 : 2);
      }
    }
  }
  std::cout << counter + 1;
}