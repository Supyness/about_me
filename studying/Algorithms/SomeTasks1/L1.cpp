#include <iostream>
#include <vector>
#include <algorithm>

std::vector<std::pair<long long, size_t>> weights_cnt;

size_t f(long long sum, size_t crt_weight) {
  if (crt_weight >= weights_cnt.size()) {
    if (sum >= 0)
      return 1;
    else
      return 0;
  }
  if (sum < 0)
    return 0;

  size_t ans = 0;
  size_t c = 1;
  for (long long i = 0; i <= weights_cnt[crt_weight].second; ++i) {
    ans += c * f(sum - i * weights_cnt[crt_weight].first, crt_weight + 1);
    if (i != weights_cnt[crt_weight].second)
      c *= (weights_cnt[crt_weight].second - i);
    c /= (i + 1);
  }
  return ans;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  size_t n;
  std::cin >> n;
  std::vector<size_t> weights(n);
  for (size_t i = 0; i < n; ++i)
    std::cin >> weights[i];
  std::sort(weights.begin(), weights.end());
  size_t weight = weights[0];
  size_t cnt = 1;
  for (size_t i = 1; i < n; ++i) {
    if (weights[i] != weight) {
      weights_cnt.emplace_back(weight, cnt);
      weight = weights[i];
      cnt = 1;
    } else {
      ++cnt;
    }
  }
  weights_cnt.emplace_back(weight, cnt);

  long long c;
  std::cin >> c;
  std::cout << f(c, 0);
}