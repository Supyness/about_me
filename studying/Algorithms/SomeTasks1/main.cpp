#include <iostream>
#include <vector>

int main() {
  int n, k; std::cin >> n >> k;
  if (k == 0 && n == 1) {
    std::cout << 0;
    return 0;
  }
  if (k == 0 && n != 1) {
    std::cout << -1;
    return 0;
  }
  if (k == 1 && n == 1) {
    std::cout << 0;
    return 0;
  }
  if (k == 1 && n != 1) {
    std::cout << n - 1;
    return 0;
  }
  int counter = 0, tmp = 1;
  while (tmp < n) {
    tmp *= 2;
    ++counter;
  }
  if (counter <= k)  {
    std::cout << counter;
    return 0;
  }
  std::vector<std::vector<int>> dp(450, std::vector<int>(n + 1, 0));
  for (int i = 2; i < n + 1; ++i) {
    dp[1][i] = n - 1;
    dp[0][i] = -1;
  }
  for (int i = 2; i < 450; ++i) {
    for (int j = 1; j < n + 1; ++j) {
      dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
    }
  }
  std::cout << dp[449][n + 1];
}