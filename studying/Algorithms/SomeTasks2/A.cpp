#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

std::vector<int> dividors(int num) {
  std::vector<int> divs;
  for (int i = 1; i < int(sqrt(num)) + 1; ++i) {
    if (num % i == 0) {
      divs.push_back(i);
      divs.push_back(num / i);
    }
  }
  return divs;
}

long long square(int num1, int num2, int num3) {
  return (num1 * num2 + num1 * num3 + num2 * num3) * 2;
}



int main() {
  int n; std::cin >> n;
  std::vector<int> divs = dividors(n);
  long long minimum = 1e9;
  std::vector<int> ans = {0, 0, 0};
  for (int i = 0; i < divs.size(); ++i) {
    for (int j = i; j < divs.size(); ++j) {
      int third = n / divs[i] / divs[j];
      if ((third != 0) && (divs[i] * divs[j] * third == n)) {
        long long sq = square(divs[i], divs[j], third);
        if (sq < minimum) {
          ans = {divs[i], divs[j], third};
          minimum = std::min(minimum, sq);
        }
      }
    }
  }
  std::sort(ans.begin(), ans.end());
  for (auto it : ans) std::cout << it << ' ';
  return 0;
}