#include <iostream>
#include <vector>
#include <algorithm>


int main() {
  int n; std::cin >> n;
  std::vector<int> sp(n, 0);
  for (int i = 0; i < n; ++i) {
    std::cin >> sp[i];
  }
  int k; std::cin >> k;
  std::vector<std::pair<int, int>> requests;
  std::sort(sp.begin(), sp.end());
  for (int i = 0; i < k; ++i) {
    int L, R; std::cin >> L >> R;
    auto low = std::lower_bound(sp.begin(), sp.end(), L);
    auto upper = std::upper_bound(sp.begin(), sp.end(), R);
    std::cout << upper - low << ' ';
  }
}