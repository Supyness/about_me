#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <map>
#include <string>


bool if_in(int a, int b, std::vector<int>& sp) {
  int l = 0, r = sp.size();
  while (r - l > 1) {
    int m = (r + l) / 2;
    if (sp[m] > b) r = m;
    else if (sp[m] < a) l = m;
    else return true;
  }
  if (((sp[l] <= b) && (sp[l] >= a))) return true;
  if ((r < sp.size() && ((sp[r] <= b) && (sp[r] >= a)))) return true;
  return false;
}

int main() {
  std::string ans;
  int n; std::cin >> n;
  std::map<int, std::vector<int>> M;
  for (int i = 0; i < n; ++i) {
    int a; std::cin >> a;
    if (M.find(a) == M.end()) {
      M[a] = {};
    }
    M[a].push_back(i + 1);
  }
  int q; std::cin >> q;
  for (int i = 0; i < q; ++i) {
    int a, b, num; std::cin >> a >> b >> num;
    if (M.find(num) == M.end()) ans += '0';
    else if (if_in(a, b, M[num])) {
      ans += '1';
    }
    else {
      ans += '0';
    }
  }
  std::cout << ans;
}