#include <iostream>
#include <vector>
#include <set>

int main() {
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<std::pair<int, int>>> graph(n);
  for (int i = 0; i < m; ++i) {
    int x, y, w; std::cin >> x >> y >> w;
    graph[x - 1].emplace_back(y - 1, w);
    graph[y - 1].emplace_back(x - 1, w);
  }
  long long ans = 0;
  std::vector<bool> used(n, false);
  std::set<std::pair<int, int>> S;
  S.insert({0, 0});
  while (!S.empty()) {
    std::pair<int, int> tmp = *S.begin();
    S.erase(S.begin());
    if (used[tmp.second]) continue;
    used[tmp.second] = true;
    ans += tmp.first;
    for (auto& it : graph[tmp.second]) {
      if (!used[it.first]) {
        S.insert({it.second, it.first});
      }
    }
  }
  std::cout << ans;
}