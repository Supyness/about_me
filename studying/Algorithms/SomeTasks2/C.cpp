#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <map>
#include <string>



int main() {
  int n; std::cin >> n;
  if (n == 0) {
    std::cout << 1;
    return 0;
  }
  std::vector<std::vector<int>> dp(3, std::vector<int>(n, 0));
  for (int i = 0; i < 3; ++i) {
    dp[i][0] = 1;
  }
  for (int i = 1; i < n; ++i) {
    dp[0][i] = dp[0][i - 1] + dp[1][i - 1] + dp[2][i - 1];
    dp[1][i] = dp[0][i - 1] + dp[1][i - 1] + dp[2][i - 1];
    dp[2][i] = dp[0][i - 1] + dp[2][i - 1];
  }
  std::cout << dp[0][n - 1] + dp[1][n - 1] + dp[2][n - 1];
}