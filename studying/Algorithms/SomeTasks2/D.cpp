#include <iostream>
#include <vector>

int main() {
  int64_t n;
  std::cin >> n;
  std::vector<int64_t> a(n);
  for (int64_t i = 0; i < n; i++) {
    std::cin >> a[i];
  }
  std::vector<int64_t> prefix(n, -1);
  std::vector<int64_t> suffix(n, -1);
  prefix[0] = a[0];
  suffix[n - 1] = a[n - 1];
  if (n > 2) {
    prefix[2] = std::max(prefix[2], prefix[2 - 2] + a[2]);
    suffix[n - 3] = std::max(suffix[n - 3], suffix[n - 1] + a[n - 3]);

  }
  for (int64_t i = 3; i < n; i++) {
    prefix[i] = std::max(prefix[i], prefix[i - 2] + a[i]);
    prefix[i] = std::max(prefix[i], prefix[i - 3] + a[i]);
  }
  for (int64_t i = n - 4; i >= 0; i--) {
    suffix[i] = std::max(suffix[i], suffix[i + 2] + a[i]);
    suffix[i] = std::max(suffix[i], suffix[i + 3] + a[i]);
  }

  for (int64_t i = 0; i < n; i++) {
    int64_t answer = -1;
    if (i - 1 >= 0 && i + 1 < n && prefix[i - 1] != -1 && suffix[i + 1] != -1) {
      answer = std::max(answer, prefix[i - 1] + suffix[i + 1]);
    }
    if (i - 2 >= 0 && i + 1 < n && prefix[i - 2] != -1 && suffix[i + 1] != -1) {
      answer = std::max(answer, prefix[i - 2] + suffix[i + 1]);
    }
    if (i + 2 < n && i - 1 >= 0 && prefix[i - 1] != -1 && suffix[i + 2] != -1) {
      answer = std::max(answer, prefix[i - 1] + suffix[i + 2]);
    }
    std::cout << answer << ' ';
  }
  return 0;
}