#include <iostream>
#include <vector>
#include <algorithm>

void reorder_polygon(std::vector<std::pair<double, double>>& P){
  size_t pos = 0;
  for(size_t i = 1; i < P.size(); i++){
    if(P[i].first < P[pos].second || (P[i].second == P[pos].second && P[i].first < P[pos].second))
      pos = i;
  }
  rotate(P.begin(), P.begin() + pos, P.end());
}

std::vector<std::pair<double, double>> minkowski(std::vector<std::pair<double, double>> P, std::vector<std::pair<double, double>> Q){
  reorder_polygon(P);
  reorder_polygon(Q);
  P.push_back(P[0]);
  P.push_back(P[1]);
  Q.push_back(Q[0]);
  Q.push_back(Q[1]);
  std::vector<std::pair<double, double>> result;
  size_t i = 0, j = 0;
  while(i < P.size() - 2 || j < Q.size() - 2){
    result.push_back({P[i].first - Q[j].first, P[i].second - Q[j].second});
    auto cross = (P[i + 1].first - P[i].first) * (Q[j + 1].second - Q[j].second) - (Q[j + 1].first - Q[j].first) * (P[i + 1].second - P[i].second);
    if(cross >= 0)
      ++i;
    if(cross <= 0)
      ++j;
  }
  return result;
}

int main() {
  double n, m; std::cin >> n >> m;
  std::vector<std::pair<double, double>> poly1;
  double maxim_x = -1e9, maxim_y = -1e9;
  for (double i = 0; i < n; ++i) {
    double a, b; std::cin >> a >> b;
    poly1.push_back({a, b});
  }
  std::vector<std::pair<double, double>> poly2;
  for (double i = 0; i < m; ++i) {
    double a, b; std::cin >> a >> b;
    maxim_x = std::max(maxim_x, a);
    maxim_y = std::max(maxim_y, b);
    poly2.push_back({a, b});
  }
  std::vector<std::pair<double, double>> summ = minkowski(poly1, poly2);
  double maxim1 = -1e9, maxim2 = -1e9;
  for (auto it : summ) std::cout << it.first << ' ' << it.second << std::endl;
  for (auto it : summ) {
    maxim1 = std::max(maxim1, it.first - maxim_x);
    maxim2 = std::max(maxim2, it.second - maxim_y);
  }
  std::cout << std::max(maxim1, maxim2);
}