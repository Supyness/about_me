//
// Created by evskl on 14.03.2022.
//

#include <vector>
#include <iostream>

int n = 3;

int main() {
  std::vector<int> arr(n, 0);
  for (int i = 1; i < n + 1; ++i) {
    arr[i - 1] = i;
  }
  std::vector<std::vector<int>> t;
  std::vector<std::vector<std::vector<int>>> k;
  for (int x1 = 0; x1 < n; ++x1) {
    for (int x2 = 0; x2 < n; ++x2) {
      for (int x3 = 0; x3 < n; ++x3) {
        for (int x4 = 0; x4 < n; ++x4) {
          for (int x5 = 0; x5 < n; ++x5) {
            for (int x6 = 0; x6 < n; ++x6) {
              t.push_back({arr[x1], arr[x2], arr[x3], arr[x4], arr[x5], arr[x6]});
            }
          }
        }
      }
    }
  }
  for (std::vector<int> x1 : t) {
    for (std::vector<int> x2 : t) {
      for (std::vector<int> x3 : t) {
        if (x1 != x2 && x2 != x3) {
          bool flag = true;
          /*for (int i = 0; i < k.size(); ++i) {
            if (!(k[i][0] != x1 && k[i][1] != x2 && k[i][2] != x3)) {
              flag = false;
            }
          }*/
          if (flag) {
            std::vector<int> counters(3, 0);
            for (int i = 0; i < 6; ++i) {
              for (int j = 0; j < 6; ++j) {
                if (x1[i] > x2[j]) {
                  counters[0]++;
                }
              }
            }
            for (int i = 0; i < 6; ++i) {
              for (int j = 0; j < 6; ++j) {
                if (x2[i] > x3[j]) {
                  counters[1]++;
                }
              }
            }
            for (int i = 0; i < 6; ++i) {
              for (int j = 0; j < 6; ++j) {
                if (x3[i] > x1[j]) {
                  counters[2]++;
                }
              }
            }
            if (counters[0] > 18 && counters[1] > 18 && counters[2] > 18) {
              for (int i : x1) std::cout << i << ' ';
              std::cout << std::endl;
              for (int i : x2) std::cout << i << ' ';
              std::cout << std::endl;
              for (int i : x3) std::cout << i << ' ';
              std::cout << std::endl;
            }
          }
        }
      }
    }
  }
}