#include <iostream>
#include <vector>
#pragma optimize( "", off )

long long t = 1e9 + 7;

std::vector<std::vector<long long>> n_degree(std::vector<std::vector<long long>> matrix1, long long n, long long size) {
  if (n == 1) {
    return matrix1;
  }
  if (n % 2 == 0) {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n / 2, size);
    std::vector<std::vector<long long>> matrix3(size, std::vector<long long>(size, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix2[z][j]) % t;
          matrix3[i][j] %= t;
        }
      }
    }
    return matrix3;
  }
  else {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n - 1, size);
    std::vector<std::vector<long long>> matrix3(size, std::vector<long long>(size, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix1[z][j]) % t;
          matrix3[i][j] %= t;
        }
      }
    }
    return matrix3;
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n, l, m; std::cin >> n >> l >> m;
  std::vector<std::vector<long long>> sp(3, std::vector<long long>(n, 0));
  for (long long i = 0; i < 3; ++i) {
    for (long long j = 0; j < n; ++j) {
      std::cin >> sp[i][j];
    }
  }
  std::vector<long long> ost(m, 0);
  for (long long i = 0; i < n; ++i) {
    ost[sp[0][i] % m] += 1;
  }
  std::vector<std::vector<long long>> matrix(m, std::vector<long long>(m, 0));
  for (long long i = 0; i < m; ++i) {
    for (long long j = 0; j < n; ++j) {
      matrix[i][(i - sp[1][j] + m) % m] += 1;
    }
  }
  if (l != 2) {
    matrix = n_degree(matrix, l - 2, m);
    std::vector<long long> ost1(m, 0);
    for (long long i = 0; i < m; ++i) {
      for (long long j = 0; j < m; ++j) {
        ost1[i] = (ost1[i] + (ost[j] * matrix[i][j]) % t) % t;
      }
    }
    long long ans = 0;
    for (int i = 0; i < n; ++i) {
      ans += ost1[(2 * m - sp[1][i] - sp[2][i]) % m];
      ans %= t;
    }
    std::cout << ans;
  }
  else {
    long long ans = 0;
    for (int i = 0; i < n; ++i) {
      ans += ost[(2 * m - sp[1][i] - sp[2][i]) % m];
      ans %= t;
    }
    std::cout << ans;
  }
}