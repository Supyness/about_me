#include <iostream>
#include <vector>
#pragma optimize( "", off )

long long t = 1e9 + 7;

long long sum(std::vector<std::vector<long long>> matrix) {
  long long sum = 0;
  for (long long i = 0; i < matrix.size(); ++i) {
    for (long long j = 0; j < matrix.size(); ++j) {
      sum += matrix[i][j];
      sum %= t;
    }
  }
  return sum % t;
}

std::vector<std::vector<long long>> n_degree(std::vector<std::vector<long long>> matrix1, long long n, long long size) {
  if (n == 1) {
    return matrix1;
  }
  if (n % 2 == 0) {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n / 2, size);
    std::vector<std::vector<long long>> matrix3(size, std::vector<long long>(size, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix2[z][j]) % t;
          matrix3[i][j] %= t;
        }
      }
    }
    return matrix3;
  }
  else {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n - 1, size);
    std::vector<std::vector<long long>> matrix3(size, std::vector<long long>(size, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix1[z][j]) % t;
          matrix3[i][j] %= t;
        }
      }
    }
    return matrix3;
  }
}

bool bin(long long num) {
  long long counter = 0;
  while (num != 0) {
    counter += num % 2;
    num /= 2;
  }
  return counter % 3 == 0;
}


int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n, k;
  std::cin >> n >> k;
  std::vector<long long> sp(n, 0);
  for (int i = 0; i < n; ++i) {
    std::cin >> sp[i];
  }
  std::vector<std::vector<long long>> matrix(n, std::vector<long long>(n, 0));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      matrix[i][j] = bin(sp[i] ^ sp[j]);
    }
  }
  if (k != 1) {
    std::cout << sum(n_degree(matrix, k - 1, n));
  }
  else {
    std::cout << n % t;
  }
}