#include <iostream>

int main() {
  long long n; std::cin >> n;
  long long r, l; r = n; l = 0;
  while (r - l > 1) {
    long long m = (r + l) / 2;
    if (m * (m + 1) / 2 > n) {
      r = m;
    }
    else {
      l = m;
    }
  }
  if (n == 1) {
    std::cout << 1;
  }
  else  std::cout << l;
}