#include <iostream>
#include <vector>
#include <string>

bool bit(int num, int mask) {
  std::string two = "";
  while (mask != 0) {
    two += std::to_string(mask % 2);
    mask /= 2;
  }
  if (num >= two.size()) return false;
  return two[num] == '1';
}
int if_count(int mask) {
  int counter = 0;
  while (mask != 0) {
    counter += mask % 2;
    mask /= 2;
  }
  return counter;
}

int main() {
  int n;
  std::cin >> n;
  std::vector<std::vector<int>> sp(n, std::vector<int>(n, 0));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cin >> sp[i][j];
    }
  }
  std::vector<std::vector<int>> dp(n, std::vector<int>((1 << n), 1e9));
  for (int mask = 0; mask < (1 << n); ++mask) {
    for (int v = 0; v < n; ++v) {
      if (bit(v, mask) && if_count(mask) == 1) {
        dp[v][mask] = 0;
      }
      if (bit(v, mask) && if_count(mask) > 1) {
        for (int u = 0; u < n; ++u) {
          if (bit(u, mask)) {
            int new_mask = (mask ^ (1 << v));
            dp[v][mask] = std::min(dp[v][mask], dp[u][new_mask] + sp[v][u]);
          }
        }
      }
    }
  }
  int max_path = 1e9, ind = 0;
  for (int i = 0; i < n; ++i) {
    if (max_path > dp[i][(1 << n) - 1]) {
      max_path = dp[i][(1 << n) - 1];
      ind = i;
    }
  }
  std::cout << max_path << std::endl;
  int counter = 0, mask = (1 << n) - 1;
  std::vector<bool> arr(n, false);
  while (counter < n) {
    std::cout << ind + 1 << ' ';
    arr[ind] = true;
    for (int i = 0; i < n; ++i) {
      if (dp[ind][mask] == dp[i][mask ^ (1 << ind)] + sp[ind][i] && !arr[i]) {
        mask = mask ^ (1 << ind);
        ind = i;
        break;
      }
    }
    ++counter;
  }
}