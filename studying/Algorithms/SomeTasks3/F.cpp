#include <iostream>
#include <vector>
#pragma optimize( "", off )

int t = 1000000000;

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<int>> sp(n, std::vector<int>(m + 1, 0));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m + 1; ++j) {
      std::cin >> sp[i][j];
    }
  }
  int ans = 1e9;
  std::vector<std::vector<int>> dp1((1 << m), std::vector<int>(2, t));
  std::vector<std::vector<int>> dp2((1 << m), std::vector<int>(2, t));
  dp1[0][0] = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < (1 << m); ++j) {
      dp2[j] = {t, t};
    }
    for (int j = 0; j < (1 << m); ++j) {
      for (int z = 0; z < 2; ++z) {
        dp2[j][0] = std::min(dp2[j][0], dp1[j][z]);
        for (int k = 1; k < m + 1; ++k) {
          if ((j & (1 << (k - 1))) == 0) {
            int addible = sp[i][k] + (z == 0 ? sp[i][0] : 0);
            dp1[j | (1 << (k - 1))][1] = std::min(dp1[j | (1 << (k - 1))][1], dp1[j][z] + addible);
          }
        }
      }
    }
    ans = std::min(ans, std::min(dp1[(1 << m) - 1][0], dp1[(1 << m) - 1][1]));
    std::swap(dp1, dp2);
  }
  ans = std::min(ans, std::min(dp1[(1 << m) - 1][0], dp1[(1 << m) - 1][1]));
  std::cout << ans;
}