#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker, "/stack:16777216")
#include <string>
#include <vector>
#include <map>
#include <list>
#include <iterator>
#include <set>
#include <queue>
#include <iostream>
#include <sstream>
#include <stack>
#include <deque>
#include <cmath>
#include <memory.h>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <algorithm>
#include <utility>
#include <time.h>
using namespace std;

#define FOR(i, a, b) for(int i = (a); i < (b); ++i)
#define RFOR(i, b, a) for(int i = (b) - 1; i >= (a); --i)
#define REP(i, N) FOR(i, 0, N)
#define RREP(i, N) RFOR(i, N, 0)
#define FILL(A,value) memset(A,value,sizeof(A))

#define ALL(V) V.begin(), V.end()
#define SZ(V) (int)V.size()
#define PB push_back
#define MP make_pair
#define Pi 3.14159265358979

typedef long long Int;
typedef unsigned long long UINT;
typedef vector <int> VI;
typedef pair <int, int> PII;

const int INF = 1000000000;
const int MAX = 128;
const int MAX2 = 7000;
const int BASE = 1000000000;

int n, m;
int D[MAX];
int A[MAX][MAX];
int R[MAX][1 << 16][2];

int main()
{
#ifndef ONLINE_JUDGE
  //freopen("in.txt", "r", stdin);
#endif

  scanf("%d %d", &n, &m);
  FOR (i,0,n)
  {
    scanf("%d", &D[i]);
    FOR (j,0,m)
      scanf("%d", &A[i][j]);
  }

  FOR (i,0,n+1)
    FOR (j,0,1 << m)
      FOR (k,0,2)
        R[i][j][k] = INF;
  R[0][0][0] = 0;
  FOR (i,0,n)
    FOR (mask,0,(1 << m))
      FOR (b,0,2)
      {
        if (R[i][mask][b] >= INF)
          continue;
        R[i+1][mask][0] = min(R[i+1][mask][0], R[i][mask][b]);
        FOR (j,0,m)
          if ((mask & (1 << j)) == 0)
          {
            int add = A[i][j];
            if (b == 0)
              add += D[i];
            //cout << add << endl;
            R[i][mask | (1 << j)][1] = min(R[i][mask | (1 << j)][1], R[i][mask][b] + add);
          }
      }
  int res = INF;
  FOR (i,0,n+1)
    FOR (k,0,2)
      res = min(res, R[i][(1 << m)-1][k]);
  cout << res << endl;


  return 0;
}