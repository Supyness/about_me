//
// Created by evskl on 12.03.2022.
//
#include <iostream>
#include <vector>
#include <string>

int t = 1000000000 + 7;

int solute_func(std::vector<std::vector<char>>& sp, int n, int m) {
  int current = 1;
  for (int i = 0; i < n; ++i) {
    std::vector<bool> flags = {false, false};
    for (int j = 0; j < m; ++j) {
      if (sp[i][j] == '+') {
        flags[(j % 2) ^ 1] = true;
      }
      if (sp[i][j] == '-') {
        flags[j % 2] = true;
      }
    }
    if (flags[0] && flags[1]) {
      current = 0;
    }
    if (!flags[0] && !flags[1]) {
      current = (current * 2) % t;
    }
  }
  return current;
}

int main() {
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<char>> sp1(n, std::vector<char>(m, '.'));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      std::cin >> sp1[i][j];
    }
  }
  std::vector<std::vector<char>> sp2(m, std::vector<char>(n, '.'));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      sp2[j][i] = sp1[i][j];
    }
  }
  int ans = (solute_func(sp1, n, m) + solute_func(sp2, m, n)) % t;
  std::vector<int> addible(2, t - 1);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      if (sp1[i][j] == '+') {
        addible[(i + j) % 2] = 0;
      }
      if (sp1[i][j] == '-') {
        addible[((i + j) % 2) ^ 1] = 0;
      }
    }
  }
  std::cout << (ans + (addible[0] + addible[1]) % t) % t;
}