#include <iostream>
#include <vector>

int main() {
  int n; std::cin >> n;
  std::vector<std::vector<char>> table(n, std::vector<char>(n, '1'));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      std::cin >> table[i][j];
    }
  }
  std::vector<int> dp((1 << 18), 0);
  std::vector<int> two_degres(18, 1);
  for (int i = 1; i < n; ++i) {
    two_degres[i] = two_degres[i - 1] * 2;
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < (1 << 18); ++j) {
      if (dp[j] != 0) {
        int counter = j + 1;
        for (int z = n - 1; z > i; --z) {
          if (counter >= two_degres[z]) counter -= two_degres[z];
        }
        if (counter < two_degres[i]) {
          counter = j + 1;
          for (int z = n - 1; z > -1; --z) {
            if (counter >= two_degres[z]) counter -= two_degres[z];
            else if (table[i][z] == 'Y') dp[j + two_degres[z] + two_degres[i]] = dp[j] + 1;
          }
        }
      }
    }
    for (int j = 0; j < n; ++j) {
      if (table[i][j] == 'Y') dp[two_degres[j] + two_degres[i] - 1] = 1;
    }
  }
  int max_ans = 0;
  for (int i = 0; i < (1 << 18); ++i) {
    max_ans = std::max(max_ans, dp[i]);
  }
  std::cout << max_ans * 2;
}