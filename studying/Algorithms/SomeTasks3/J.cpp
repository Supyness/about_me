#include <iostream>
#include <vector>
#include <string>

bool return_mask_poses(int num1, int num2, int n) {
  std::string mask1 = "";
  std::string mask2 = "";
  while (mask1.size() < n) {
    mask1 += std::to_string(num1 % 2);
    num1 /= 2;
  }
  while (mask2.size() < n) {
    mask2 += std::to_string(num2 % 2);
    num2 /= 2;
  }
  for (int i = 1; i < n; ++ i) {
    int k = mask1[i - 1] - '0' + mask1[i] - '0' + mask2[i - 1] - '0' + mask2[i] - '0';
    if (k == 0 || k == 4) {
      return false;
    }
  }
  return true;
}

int main() {
  int n, m; std::cin >> n >> m;
  if (n > m) std::swap(n, m);
  int pos1 = (1 << n);
  std::vector<std::vector<bool>> dp1(pos1, std::vector<bool>(pos1, 0));
  for (int i = 0; i < pos1; ++i) {
    for (int j = 0; j < pos1; ++j) {
      if (return_mask_poses(i, j, n)) {
        dp1[i][j] = true;
      }
      else dp1[i][j] = false;
    }
  }
  std::vector<int> dp1_1(pos1, 1);
  for (int i = 1; i < m; ++i) {
    std::vector<int> dp1_2(pos1, 0);
    for (int j = 0; j < pos1; ++j) {
      for (int z = 0; z < pos1; ++z) {
        dp1_2[j] = dp1_2[j] + dp1_1[z] * dp1[z][j];
      }
    }
    std::swap(dp1_1, dp1_2);
  }
  int ans = 0;
  for (int i = 0; i < pos1; ++i) {
    ans += dp1_1[i];
  }
  std::cout << ans;
}