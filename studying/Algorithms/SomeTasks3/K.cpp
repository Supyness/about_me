#include <iostream>
#include <cstring>
#include <cstdio>

const long long max_mask = (1 << 6);
std::string n;
long long m, mod;
long long tmp_matrix[max_mask][max_mask];
long long main_mask;

class Bigint {
 public:
  long long capacity;
  long long arr[105];
  Bigint() {
    memset(arr, 0, sizeof(arr));
    capacity = 1;
  }
  Bigint(std::string num) {
    memset(arr, 0, sizeof(arr));
    capacity = num.size();
    long long counter = 0;
    for (long long i = capacity - 1; i > -1; --i) {
      arr[counter++] = num[i] - '0';
    }
  }
  bool is_odd() {
    return arr[0] & 1;
  }
  bool is_null() {
    return (capacity == 1 && arr[0] == 0);
  }
};

Bigint operator-(const Bigint& first, const long long &t) {
  Bigint result = first;
  long long counter = 0;
  result.arr[0] -= t;
  while (result.arr[counter] < 0) {
    result.arr[counter + 1]--;
    result.arr[counter++] += 10;
  }
  if (!result.arr[result.capacity - 1]) --result.capacity;
  return result;
}

Bigint operator/(const Bigint &first, const long long &t) {
  Bigint result;
  long long addible = 0;
  for (long long i = first.capacity - 1; i > -1; --i) {
    long long current = addible * 10 + first.arr[i];
    result.arr[i] = current / t;
    addible = current % t;
  }
  long long counter = first.capacity - 1;
  while (counter && !result.arr[counter]) {
    counter--;
  }
  result.capacity = counter + 1;
  return result;
}

struct matrix {
  long long arr[max_mask][max_mask];
  matrix() {
    memset(arr, 0, sizeof(arr));
  }
  matrix(long long tmp[max_mask][max_mask]) {
    memcpy(arr, tmp, sizeof(arr));
  }
  void multy(Bigint t) {
    matrix result, tmp;
    for (long long i = 0; i < max_mask; ++i) {
      result.arr[i][i] = 1;
    }
    tmp = *this;
    while (!t.is_null()) {
      if (t.is_odd()) {
        result *= tmp;
      }
      tmp *= tmp;
      t = t / 2;
    }
    *this = result;
  }
  void operator*=(const matrix &second) {
    matrix result;
    for (long long i = 0; i < main_mask + 1; ++i) {
      for (long long j = 0; j < main_mask + 1; ++j) {
        for (long long z = 0; z < main_mask + 1; ++z) {
          result.arr[i][j] = (result.arr[i][j] + (this->arr[i][z] * second.arr[z][j]) % mod) % mod;
        }
      }
    }
    *this = result;
  }
};

long long g_bit(long long number, long long pos) {
  return (number >> pos) & 1 ? 1 : 0;
}

int main() {
  /*std::string a;
  std::cin >> a;
  Bigint t = Bigint(a);
  t = t / 2;*/
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  std::cin >> n;
  std::cin >> m >> mod;
  //scanf("%s %d %d", &n, &m, &mod);
  main_mask = (1 << m) - 1;
  if (Bigint(n).arr[0] == 1) {
    std::cout << (1 << m) % mod << std::endl;
  }
  else {
    for (long long i = 0; i < main_mask + 1; ++i) {
      for (long long j = 0; j < main_mask + 1; ++j) {
        bool per = true;
        for (long long bit = 0; bit < m - 1; ++bit) {
          long long current = g_bit(i, bit) + g_bit(i, bit + 1) + g_bit(j, bit) + g_bit(j, bit + 1);
          per &= !(current == 0 || current == 4);
        }
        tmp_matrix[i][j] = per ? 1 : 0;
      }
    }
    matrix new_matrix = matrix(tmp_matrix);
    new_matrix.multy(Bigint(n) - 1);
    for (long long i = 0; i < main_mask + 1; ++i) {
      for (long long j = 0; j < main_mask + 1; ++j) {
        std::cout << new_matrix.arr[i][j] << ' ';
      }
      std::cout << std::endl;
    }
    unsigned long long result = 0;
    for (long long i = 0; i < main_mask + 1; ++i) {
      for (long long j = 0; j < main_mask + 1; ++j) {
        result = (result + new_matrix.arr[i][j]) % mod;
      }
    }
    std::cout << result << std::endl;
  }
}