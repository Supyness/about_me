#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
#include <string.h>
#include <cmath>
#include <queue>
#include <vector>
#include <map>
#include <stdlib.h> // for exit(0)
#include <stack>
#include <list>
#include <ctime>
#include <set>

using namespace std;

const long long MASK_BORDER = 1 << 6;
const long long MAX_DIGITS = 105;
const long long OSN = 10;

char n[MAX_DIGITS];
long long m,p;
long long adj_mask[MASK_BORDER][MASK_BORDER];
long long MASK;

struct Bigint {
  long long amount;
  long long digits[MAX_DIGITS];
  Bigint() {
    memset(digits,0,sizeof(digits));
    amount = 1;
  }
  Bigint(char *buf) {
    memset(digits,0,sizeof(digits));
    amount = strlen(buf);
    long long pos = 0;
    for (long long i = amount - 1; i>=0; i--)
      digits[pos++] = buf[i] - '0';
  }
  bool isZero() {
    return amount == 1 && digits[0] == 0;
  }
  bool isOdd() {
    return digits[0] & 1;
  }
};
Bigint operator - (const Bigint &a, const long long &n) {
  Bigint res = a;
  long long pos = 0;
  res.digits[pos] -= n;
  while (res.digits[pos] < 0) {
    res.digits[pos+1]--;
    res.digits[pos++] += OSN;
  }
  if (!res.digits[res.amount-1])
    res.amount--;
  return res;
}
Bigint operator / (const Bigint &a, const long long &n) {
  Bigint res;
  long long ost = 0;
  for (long long i=a.amount - 1; i>=0; --i) {
    long long cur = ost * OSN + a.digits[i];
    res.digits[i] = cur / n;
    ost = cur % n;
  }
  long long pos = a.amount - 1;
  while (pos && !res.digits[pos])
    pos--;
  res.amount = pos + 1;
  return res;
}

struct matrix {
  long long mas[MASK_BORDER][MASK_BORDER];
  matrix() {
    memset(mas,0,sizeof(mas));
  }
  matrix(long long src[MASK_BORDER][MASK_BORDER]) {
    memcpy(mas,src,sizeof(mas));
  }
  void setE() {
    for (long long i=0;i<MASK_BORDER;++i)
      mas[i][i] = 1;
  }
  void fast_mul(Bigint n) {
    matrix res, b;
    res.setE();
    b = *this;
    while (!n.isZero()) {
      if (n.isOdd()) { // &1
        res *= b;
      }
      b *= b;
      n = n / 2;
    }
    *this = res;
  }
  void operator *= (const matrix &b) {
    matrix res;
    for (long long i = 0; i <= MASK; ++i) {
      for (long long j = 0; j <= MASK; ++j) {
        for (long long k = 0; k <= MASK; ++k) {
          res.mas[i][j] = (res.mas[i][j] + (this->mas[i][k] * b.mas[k][j]) % p ) % p;
        }
      }
    }
    *this = res;
  }
} a;
void input(){
  scanf("%s %d %d", &n, &m, &p);
  MASK = (1 << m) - 1;
}
long long get_bit(long long num, long long bit) {
  return (num >> bit) & 1 ? 1 : 0;
}
void check_compatible() {
  for (long long prv = 0; prv <= MASK; ++prv) {
    for (long long cur = 0; cur <= MASK; ++cur) {
      bool isOK = true;
      for (long long bit = 0; bit < m - 1; ++bit) {
        long long sum = get_bit(prv,bit) + get_bit(prv,bit+1) +
            get_bit(cur,bit) + get_bit(cur,bit+1);
        isOK &= !(sum == 0 || sum == 4);
      }
      adj_mask[prv][cur] = isOK ? 1 : 0;
    }
  }
}
void solve(){
  check_compatible();
  a = matrix(adj_mask);
  /*for (long long i = 0; i < MASK + 1; ++i) {
    for (long long j = 0; j < MASK + 1; ++j) {
      std::cout << a.mas[i][j] << ' ';
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;*/
  a.fast_mul(Bigint(n) - 1);
  /*for (long long i = 0; i < MASK; ++i) {
    for (long long j = 0; j < MASK; ++j) {
      std::cout << a.mas[i][j] << ' ';
    }
    std::cout << std::endl;
  }*/
}
void output() {
  long long res = 0;
  for (long long i=0; i <= MASK; ++i) {
    for (long long j=0; j <= MASK; ++j)
      res = (res + a.mas[i][j]) % p;
  }
  printf("%d", res);
}
int main()
{

  input();
  if (Bigint(n).digits[0] == 1) {
    std::cout << (1 << m) % p << std::endl;
    return 0;
  }
  solve();
  output();

  return 0;
}