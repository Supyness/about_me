#include <iostream>
#include <vector>
#pragma optimize( "", off )

const int c_i = 16;
const long long mod = 1e9 + 7;

std::vector<std::vector<long long>> multyply(std::vector<std::vector<long long>>& matrix1, std::vector<std::vector<long long>>& matrix2) {
  std::vector<std::vector<long long>> matrix_tmp(c_i + 1, std::vector<long long>(c_i + 1, 0));
  for (int i = 0; i < c_i + 1; ++i) {
    for (int j = 0; j < c_i + 1; ++j) {
      for (int z = 0; z < c_i + 1; ++z) {
        matrix_tmp[i][j] = (matrix_tmp[i][j] + (matrix1[i][z] * matrix2[z][j]) % mod) % mod;
      }
    }
  }
  return matrix_tmp;
}

std::vector<std::vector<long long>> n_degree(std::vector<std::vector<long long>> matrix1, long long n) {
  if (n == 1) {
    return matrix1;
  }
  if (n % 2 == 0) {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n / 2);
    std::vector<std::vector<long long>> matrix3(c_i + 1, std::vector<long long>(c_i + 1, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] = (matrix3[i][j] + (matrix2[i][z] * matrix2[z][j]) % mod) % mod;
        }
      }
    }
    return matrix3;
  }
  else {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n - 1);
    std::vector<std::vector<long long>> matrix3(c_i + 1, std::vector<long long>(c_i + 1, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] = (matrix3[i][j] + (matrix2[i][z] * matrix1[z][j]) % mod) % mod;
        }
      }
    }
    return matrix3;
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  long long n, k; std::cin >> n >> k;
  std::vector<std::vector<long long>> sp(n, std::vector<long long>(3, 0));
  for (int i = 0; i < n; ++i) {
    long long a, b, c; std::cin >> a >> b >> c;
    sp[i] = {a, b, c};
  }
  std::vector<std::vector<long long>> matrix_main(c_i + 1, std::vector<long long>(c_i + 1, 0));
  matrix_main[0][0] = 1;
  for (int i = 0; i < n; ++i) {
    std::vector<std::vector<long long>> matrix(c_i + 1, std::vector<long long>(c_i + 1, 0));
    for (int j = 0; j <= sp[i][2]; ++j) {
      if (j == 0) {
        matrix[0][0] = 1;
        matrix[0][1] = (sp[i][2] > 0 ? 1 : 0);
      }
      else if (j != sp[i][2]) {
        matrix[j][j] = 1;
        matrix[j][j - 1] = 1;
        matrix[j][j + 1] = 1;
      }
      else {
        matrix[j][j] = 1;
        matrix[j][j - 1] = 1;
      }
    }
    matrix = n_degree(matrix, std::min(sp[i][1], k) - sp[i][0]);
    matrix_main = multyply(matrix_main, matrix);
  }
  std::cout << matrix_main[0][0] % mod;
}