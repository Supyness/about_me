#include <iostream>
#include <vector>
#include <string>

long long to_dec(std::string& s) {
  long long tmp = 0;
  for (int i = 0; i < s.size(); ++i) {
    if (s[i] == '1') {
      tmp += (1 << i);
    }
  }
  return tmp;
}

int main() {
  int n;
  std::cin >> n;
  std::vector<long long> two_degrees(n + 1, 1);
  std::vector<std::string> sp_first_part(n, "");
  std::vector<std::string> sp_second_part(n, "");
  std::vector<long long> arr_first(n, 0);
  std::vector<long long> arr_second(n, 0);
  for (int i = 0; i < n; ++i) {
    std::string s;
    for (int j = 0; j < n / 2 + n % 2; ++j) {
      char t;
      std::cin >> t;
      s += t;
    }
    arr_first[i] = to_dec(s);
    s = "";
    for (int j = 0; j < n / 2; ++j) {
      char t;
      std::cin >> t;
      s += t;
    }
    arr_second[i] = to_dec(s);
    if (i >= 1) {
      two_degrees[i] = two_degrees[i - 1] * 2;
    }
  }
  two_degrees[n] = two_degrees[n - 1] * 2;
  /*if (n == 1) std::cout << 2;
  else {
    std::vector<long long> dp((1 << (n / 2 + n % 2)), 0);
    for (long long i = 1; i < (1 << (n / 2 + n % 2)); ++i) {
      long long k = i;
      for (int j = 0; j < n / 2 + n % 2; ++j) {
        if (i & (1 << j)) {
          k &= (arr1[j] + (1 << j));
        }
      }
      if (k == i) dp[i] += 1;
    }
    for (int i = 0; i < n / 2 + n % 2; ++i) {
      for (long long mask = 0; mask < (1 << (n / 2 + n % 2)); ++mask) {
        if (!(mask & (1 << i))) {
          dp[mask + (1 << i)] += dp[mask];
        }
      }
    }
    int ans = 0;
    int oldest = n / 2 - (n % 2 == 0 ? 1 : 0);
    for (long long i = 1; i < (1 << n / 2); ++i) {
      if (!(i & (i - 1))) ++oldest;
      long long k = (i << (n / 2 + n % 2));
      std::cout << static_cast<long long>(arr1[oldest] + two_degrees[oldest]) << std::endl;
      if ((k & (arr1[oldest] + static_cast<long long>(1 << oldest))) == k) {
        int t = (1 << (n / 2 + n % 2)) - 1;
        ans += dp[t & arr1[oldest]];
        ans += 1;
      }
    }
    std::cout << ans + dp[dp.size() - 1] + 1;
  }*/
}