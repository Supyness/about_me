#include <iostream>
#include <vector>
#pragma optimize( "", off )

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n;
  std::cin >> n;
  std::vector<int> arr_first(n / 2 + n % 2, 0);
  std::vector<int> arr_second(n / 2, 0);
  std::vector<int> arr_third(n / 2 + n % 2, 0);
  for (int i = 0; i < n / 2 + n % 2; ++i) {
    char t;
    int s = 0;
    for (int j = 0; j < n / 2 + n % 2; ++j) {
      std::cin >> t;
      if (t == '1') {
        s += (1 << j);
      }
    }
    for (int j = 0; j < n / 2; ++j) {
      std::cin >> t;
    }
    arr_first[i] = s;
  }
  for (int i = 0; i < n / 2; ++i) {
    char t;
    int s = 0;
    for (int j = 0; j < n / 2 + n % 2; ++j) {
      std::cin >> t;
      if (t == '1') {
        s += (1 << j);
      }
    }
    arr_third[i] = s;
    s = 0;
    for (int j = 0; j < n / 2; ++j) {
      std::cin >> t;
      if (t == '1') {
        s += (1 << j);
      }
    }
    arr_second[i] = s;
  }
  int oldest = -1;
    std::vector<int> dp((1 << (n / 2 + n % 2)), 0);
    for (int i = 1; i < (1 << (n / 2 + n % 2)); ++i) {
      if (!(i & (i - 1))) ++oldest;
      int k = i;
      k -= (1 << oldest);
      if (k == 0) dp[i] += 1;
      else if ((k & arr_first[oldest]) == k && dp[k]) dp[i] += 1;
    }
    for (int i = 0; i < n / 2 + n % 2; ++i) {
      for (int mask = 0; mask < (1 << (n / 2 + n % 2)); ++mask) {
        if (!(mask & (1 << i))) {
          dp[mask + (1 << i)] += dp[mask];
        }
      }
    }
    long long ans = 0;
    oldest = -1;
    std::vector<int> dp1((1 << (n / 2)), 0);
    std::vector<int> per((1 << (n / 2)), 0);
    for (int i = 1; i < (1 << n / 2); ++i) {
      if (!(i & (i - 1))) ++oldest;
      int k = i;
      k -= (1 << oldest);
      if (k == 0) {
        per[i] = arr_third[oldest];
      }
      else {
        per[i] = arr_third[oldest] & per[k];
      }
      if ((k == 0) || ((k & (arr_second[oldest])) == k && dp1[k])) {
        ans += dp[per[i]];
        ans += 1;
        dp1[i] += 1;
      }
    }
    std::cout << ans + dp[dp.size() - 1] + 1;
}