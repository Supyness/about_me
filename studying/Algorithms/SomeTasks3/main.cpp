#include <iostream>
#include <vector>

const long long s = 999999937;

long long sum(std::vector<std::vector<long long>> matrix) {
  long long sum = 0;
  for (long long i = 0; i < matrix.size(); ++i) {
    sum += matrix[0][i];
  }
  return sum % 999999937;
}

std::vector<std::vector<long long>> n_degree(std::vector<std::vector<long long>> matrix1, long long n) {
  if (n == 1) {
    return matrix1;
  }
  if (n % 2 == 0) {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n / 2);
    std::vector<std::vector<long long>> matrix3(5, std::vector<long long>(5, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix2[z][j]) % 999999937;
        }
        matrix3[i][j] %= 999999937;
      }
    }
    return matrix3;
  }
  else {
    std::vector<std::vector<long long>> matrix2 = n_degree(matrix1, n - 1);
    std::vector<std::vector<long long>> matrix3(5, std::vector<long long>(5, 0));
    for (long long i = 0; i < matrix2.size(); ++i) {
      for (long long j = 0; j < matrix2.size(); ++j) {
        for (long long z = 0; z < matrix2.size(); ++z) {
          matrix3[i][j] += (matrix2[i][z] * matrix1[z][j]) % 999999937;
        }
        matrix3[i][j] %= 999999937;
      }
    }
    return matrix3;
  }
}

int main() {
  long long n;
  std::cin >> n;
  std::vector<std::vector<long long>> matrix = {{1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 0, 0}, {1, 1, 1, 1, 1}, {1, 1, 1, 0, 0}};
  while (n != 0) {
    std::cout << sum(n_degree(matrix, n)) << std::endl;
    std::cin >> n;
  }
}
