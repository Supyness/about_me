#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <unordered_map>

long long hash(std::string s) {
  return std::stoll(s);
}

int main() {
  int n; std::cin >> n;
  std::unordered_map<long long, int> U_M;
  std::unordered_map<long long, int> U_M1;
  std::string s;
  std::string s1;
  std::deque<std::string> headQ;
  std::deque<std::string> tailQ;
  for (int i = 0; i < n; ++i) {
    int t; std::cin >> t;
    --t;
    s += std::to_string(t);
  }
  for (int i = 0; i < n; ++i) {
    int t; std::cin >> t;
    --t;
    s1 += std::to_string(t);
  }
  headQ.push_back(s);
  tailQ.push_back(s1);
  U_M[hash(s)] = 1;
  U_M1[hash(s1)] = 1;
  size_t counter1 = 1, counter2 = 1;
  bool flag = false;
  while (!headQ.empty() && !tailQ.empty()) {
    size_t p = headQ.size();
    for (int i = 0; i < p; ++i) {
      std::string ss = headQ.front();
      headQ.pop_front();
      if (U_M1[hash(ss)]) {
        std::cout << counter1 + U_M1[hash(ss)] - 2 << std::endl;
        flag = true;
        break;
      }
      for (int j = 2; j <= n; ++j) {
        for (int t = 0; t <= n - j; ++t) {
          std::string tmp = ss;
          std::reverse(tmp.begin() + t, tmp.begin() + t + j);
          if (!U_M[hash(tmp)]) {
            headQ.push_back(tmp);
            U_M[hash(tmp)] = counter1 + 1;
            //std::cout << tmp << ' ';
          }
        }
      }
    }
    //std::cout << std::endl;
    ++counter1;
    if (flag) break;
    p = tailQ.size();
    for (int i = 0; i < p; ++i) {
      std::string ss = tailQ.front();
      tailQ.pop_front();
      if (U_M[hash(ss)]) {
        std::cout << counter2 + U_M[hash(ss)] - 2 << std::endl;
        flag = true;
        break;
      }
      for (int j = 2; j <= n; ++j) {
        for (int t = 0; t <= n - j; ++t) {
          std::string tmp = ss;
          std::reverse(tmp.begin() + t, tmp.begin() + t + j);
          if (!U_M1[hash(tmp)]) {
            tailQ.push_back(tmp);
            U_M1[hash(tmp)] = counter2 + 1;
            //std::cout << tmp << ' ';
          }
        }
      }
    }
    //std::cout << std::endl;
    ++counter2;
    if (flag) break;
  }
}
