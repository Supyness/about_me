#include <iostream>
#include <vector>
#include <queue>
#pragma optimize( "", off )

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<std::vector<int>>> graph(n);
  std::vector<std::vector<int>> t;
  for (int i = 0; i < m; ++i) {
    int x, y, w;
    std::cin >> x >> y >> w;
    graph[x - 1].push_back({y - 1, w, i});
    graph[y - 1].push_back({x - 1, w, i});
    t.push_back({x - 1, y - 1, w});
  }
  int ans = 1e9;
  int con = 10 * (n - 1);
  std::vector<std::queue<int>> Q(con);
  std::vector<bool> used(n, false);
  std::vector<int> dist(n, 1e9);
  for (int i = 0; i < m; ++i) {
    used.assign(n, false);
    dist.assign(n, 1e9);
    Q[0].push(t[i][0]);
    dist[t[i][0]] = 0;
    int max_added = 1;
    bool flag = false;
    for (int j = 0; j < con; ++j) {
      if (max_added <= 0) break;
      while (!Q[j].empty()) {
        int v = Q[j].front();
        Q[j].pop();
        --max_added;
        if (dist[v] + 2 >= ans) {
          flag = true;
          break;
        }
        if (used[v]) continue;
        used[v] = true;
        for (std::vector<int>& it : graph[v]) {
          //++counter;
          if (it[2] == i) {
            continue;
          }
          if (!used[it[0]]) {
            Q[j + it[1]].push(it[0]);
            ++max_added;
            dist[it[0]] = std::min(dist[it[0]], dist[v] + it[1]);
          }
        }
      }
      if (flag) break;
    }
    ans = std::min(ans, dist[t[i][1]] + t[i][2]);
  }
  std::cout << ans << std::endl;
  return 0;
}