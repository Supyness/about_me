#include <iostream>
#include <vector>
#include <queue>

int main() {
  long long h; std::cin >> h;
  long long a, b, c; std::cin >> a >> b >> c;
  if (a == 1 || b == 1 || c == 1) std::cout << h;
  else {
    std::vector<long long> s = {a, b, c};
    std::sort(s.begin(), s.end());
    c = s[2];
    b = s[1];
    a = s[0];
    std::vector<long long> spaces(a, 1e18);
    std::queue<long long> Q;
    Q.push(1);
    spaces[1] = 1;
    while (!Q.empty()) {
      long long v = Q.front();
      Q.pop();
      if (v > h) continue;
      if (spaces[(v + a) % a] > v + a) {
        spaces[(v + a) % a] = std::min(spaces[(v + a) % a], v + a);
        Q.push(v + a);
      }
      if (spaces[(v + b) % a] > v + b) {
        spaces[(v + b) % a] = std::min(spaces[(v + b) % a], v + b);
        Q.push(v + b);
      }
      if (spaces[(v + c) % a] > v + c) {
        Q.push(v + c);
        spaces[(v + c) % a] = std::min(spaces[(v + c) % a], v + c);
      }
      if (spaces[(v + a + b) % a] > v + a + b) {
        Q.push(v + a + b);
        spaces[(v + a + b) % a] = std::min(spaces[(v + a + b) % a], v + a + b);
      }
      if (spaces[(v + a + c) % a] > v + a + c) {
        Q.push(v + a + c);
        spaces[(v + a + c) % a] = std::min(spaces[(v + a + c) % a], v + a + c);
      }
      if (spaces[(v + b + c) % a] > v + b + c) {
        Q.push(v + b + c);
        spaces[(v + b + c) % a] = std::min(spaces[(v + b + c) % a], v + b + c);
      }
      if (spaces[(v + a + b + c) % a] > v + a + b + c) {
        Q.push(v + a + b + c);
        spaces[(v + a + b + c) % a] = std::min(spaces[(v + a + b + c) % a], v + a + b + c);
      }
    }
    long long ans = 0;
    for (auto it: spaces) {
      if (it != 1e18) ans += (h - it) / a + 1;
    }
    std::cout << ans;
  }
}