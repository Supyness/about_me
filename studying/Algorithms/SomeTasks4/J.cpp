#include <iostream>
#include <set>
#include <map>
#include <string>
#include <vector>
#pragma optimize( "", off )

void NextMove(std::vector<int>& vec, std::vector<std::vector<int>>& next) {
  int space = 0;
  for (int i = 0; i < 16; ++i) {
    if (!vec[i]) {
      space = i;
      break;
    }
  }
  int counter = 0;
  if (space < 15 && space % 4 != 3) {
    next.push_back(vec);
    std::swap(next[counter][space], next[counter][space + 1]);
    ++counter;
  }
  if (space > 0 && space % 4 != 0) {
    next.push_back(vec);
    std::swap(next[counter][space], next[counter][space - 1]);
    ++counter;
  }
  if (space >= 4) {
    next.push_back(vec);
    std::swap(next[counter][space], next[counter][space - 4]);
    ++counter;
  }
  if (space <= 11) {
    next.push_back(vec);
    std::swap(next[counter][space], next[counter][space + 4]);
    ++counter;
  }
}

char Move(std::vector<int>& from, std::vector<int>& to) {
  int space1 = 0;
  int space2 = 0;
  for (int i = 0; i < 16; ++i) {
    if (!from[i]) {
      space1 = i;
      break;
    }
  }
  for (int i = 0; i < 16; ++i) {
    if (!to[i]) {
      space2 = i;
      break;
    }
  }
  int tmp = space2 - space1;
  if (tmp == 1) return 'R';
  else if (tmp == -1) return 'L';
  else if (tmp == 4) return 'D';
  else return 'U';
}

int Heuristic(std::vector<int>& vec) {
  int ans = 0;
  for (int i = 0; i < vec.size(); ++i) {
    int element = vec[i];
    if (!element) {
      ans += abs(3 - i / 4) + abs(3 - i % 4);
    }
    else {
      ans += abs((element - 1) / 4 - i / 4) + abs((element - 1) % 4 - i % 4);
    }
  }
  return ans;
}

void Astar(std::vector<int>& graph_s, std::vector<int>& graph_f, std::map<std::vector<int>, int>& first,
           std::map<std::vector<int>, int>& second, std::map<std::vector<int>, std::vector<int>>& par) {
  std::set<std::pair<int, std::vector<int>>> pr;
  std::set<std::vector<int>> used;
  second.insert({graph_s, 0});
  first.insert({graph_s, second[graph_s] + Heuristic(graph_s)});
  pr.insert({first[graph_s], graph_s});
  while (!pr.empty()) {
    std::vector<int> tmp = pr.begin()->second;
    pr.erase(pr.begin());

    if (tmp == graph_f) {
      int ans = 0;
      std::string way;
      std::vector<int> tmp_new = graph_f;
      while (tmp_new != graph_s) {
        ++ans;
        std::vector<int> previous = par[tmp_new];
        way += Move(tmp_new, previous);
        tmp_new = previous;
      }
      std::cout << ans << std::endl;
      for (int i = way.size() - 1; i > -1; --i) {
        std::cout << way[i];
      }
      break;
    }
    used.insert(tmp);
    std::vector<std::vector<int>> next;
    NextMove(tmp, next);
    for (auto it : next) {
      int sc = second[tmp] + 1;
      if (used.find(it) != used.end() && sc >= second[it]) {
        continue;
      }
      if (used.find(it) == used.end() || sc < second[it]) {
        par.insert({it, tmp});
        second.insert({it, sc});
        first.insert({it, second[it] + Heuristic(it)});
        std::pair<int, std::vector<int>> pr1 = {first[it] - second[it], it};
        if (pr.find(pr1) == pr.end()) pr.insert(pr1);
      }
    }
  }
}

int main() {
  std::vector<int> graph_s(16);
  std::vector<int> graph_f = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
  for (int i = 0; i < 16; ++i) {
    std::cin >> graph_s[i];
  }
  std::map<std::vector<int>, int> first;
  std::map<std::vector<int>, int> second;
  std::map<std::vector<int>, std::vector<int>> par;
  int counter = 0;
  for (int i = 0; i < 16; ++i) {
    if (graph_s[i] != 0) {
      for (int j = 0; j < i; ++j) {
        if (graph_s[j] > graph_s[i]) ++counter;
      }
    }
  }
  for (int i = 0; i < 16; ++i) {
    if (!graph_s[i]) {
      counter += 1 + i / 4;
    }
  }
  if ((counter & 1) == 1) std::cout << -1;
  else {
    Astar(graph_s, graph_f, first, second, par);
  }
  return 0;
}