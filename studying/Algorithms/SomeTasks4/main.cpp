#include <iostream>
#include <vector>
#include <queue>

int main() {
  long long h; std::cin >> h;
  long long a, b, c; std::cin >> a >> b >> c;
  if (a == 1 || b == 1 || c == 1) std::cout << h;
  else {
    std::vector<long long> s = {a, b, c};
    std::sort(s.begin(), s.end());
    c = s[2];
    b = s[1];
    a = s[0];
    std::vector<long long> spaces(a, 2 * 1e18);
    std::queue<long long> Q;
    Q.push(1);
    spaces[1] = 1;
    while (!Q.empty()) {
      long long v = Q.front();
      v = spaces[v % a];
      Q.pop();
      if (spaces[(v + b) % a] > b + v) {
        spaces[(v + b) % a] = std::min(spaces[(v + b) % a], b + v);
        Q.push((v + b) % a);
      }
      if (spaces[(v + c) % a] > c + v) {
        Q.push((v + c) % a);
        spaces[(v + c) % a] = std::min(spaces[(v + c) % a], c + v);
      }
      if (spaces[(v + a + b) % a] > a + b + v) {
        Q.push((v + a + b) % a);
        spaces[(v + a + b) % a] = std::min(spaces[(v + a + b) % a], a + b + v);
      }
      if (spaces[(v + a + c) % a] > a + c + v) {
        Q.push((v + a + c) % a);
        spaces[(v + a + c) % a] = std::min(spaces[(v + a + c) % a], a + c + v);
      }
      if (spaces[(v + b + c) % a] > b + c + v) {
        Q.push((v + b + c) % a);
        spaces[(v + b + c) % a] = std::min(spaces[(v + b + c) % a], b + c + v);
      }
      if (spaces[(v + a + b + c) % a] > a + b + c + v) {
        Q.push((v + a + b + c) % a);
        spaces[(v + a + b + c) % a] = std::min(spaces[(v + a + b + c) % a], a + b + c + v);
      }
    }
    long long ans = 0;
    for (auto it: spaces) {
      if (it <= h) ans += (h - it) / a + 1;
    }
    std::cout << ans;
  }
}