#include <iostream>
#include <vector>
#include <cmath>

int t = 985000;

template<typename Key, typename Value, typename Hash = std::hash<Key>, typename Equal = std::equal_to<Key>, typename Alloc = std::allocator<std::pair<Key, Value>>>
class UnorderedMap {
 public:
  typedef std::pair<const Key, Value> NodeType;
 private:
  struct Node {

    NodeType *value;
    size_t hash__;
    Node* prev = nullptr;
    Node* next = nullptr;
    Node(NodeType* node, Node *first, Node *second, Hash hash_) : value(std::move(node)),
                                                                  hash__(hash_(node->first)), prev(first), next(second) {}


    ~Node() = default;
  };

 public:
  typedef Alloc allocator_type;
  using AllocTraits = std::allocator_traits<allocator_type>;
  typedef typename AllocTraits::template rebind_alloc<Node> node_alloc;
  typedef typename AllocTraits::template rebind_alloc<NodeType> node_alloc_new;
  template<bool is_const>
  class CommonIterator;

  template<typename CommonIterator>
  class Common_reverse_iterator;

  using iterator = CommonIterator<false>;
  using const_iterator = CommonIterator<true>;
  /*using reverse_iterator = Common_reverse_iterator<CommonIterator<false>>;
  using const_reverse_iterator = Common_reverse_iterator<CommonIterator<true>>;*/
  using reverse_iterator = Common_reverse_iterator<iterator>;
  using const_reverse_iterator = Common_reverse_iterator<const_iterator>;

  template<bool is_const>
  class CommonIterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = NodeType;
    using difference_type = long long;
    using pointer = typename std::conditional<is_const, const NodeType *, NodeType *>::type;
    using reference = typename std::conditional<is_const, const NodeType &, NodeType &>::type;

    CommonIterator(Node *pos) : pos(pos) {};

    CommonIterator() : pos(nullptr) {};

    CommonIterator(const CommonIterator &first) : pos(first.pos) {};

    ~CommonIterator() = default;

    reference operator*() const { return *pos->value; }

    CommonIterator &operator++() {
      pos = (pos != nullptr ? pos->next : nullptr);
      return *this;
    }

    Node *get_node() {
      return pos;
    }

    CommonIterator operator++(int) {
      CommonIterator tmp = *this;
      ++(*this);
      return tmp;
    }

    pointer operator->() const {
      return pos->value;
    };

    CommonIterator &operator--() {
      pos = (pos != nullptr ? pos->prev : nullptr);
      return *this;
    }

    CommonIterator operator--(int) const {
      CommonIterator tmp = *this;
      --(*this);
      return tmp;
    }

    operator const_iterator() const {
      return const_iterator(pos);
    }

    friend bool operator==(const CommonIterator &first, const CommonIterator &second) {
      return first.pos == second.pos;
    }

    friend bool operator!=(const CommonIterator &first, const CommonIterator &second) {
      return !(first == second);
    }

   private:
    Node *pos;
  };

  template<typename CommonIterator>
  class Common_reverse_iterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;

    Common_reverse_iterator(CommonIterator pos) : pos(pos) {};

    typename CommonIterator::reference operator*() const { return *pos; }

    Common_reverse_iterator &operator++() {
      --pos;
      return *this;
    }

    Common_reverse_iterator operator++(int) const {
      Common_reverse_iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    Node *get_node() {
      return pos.get_node();
    }

    Common_reverse_iterator base() {
      return Common_reverse_iterator(CommonIterator(pos.get_node()->next));
    }

    Common_reverse_iterator &operator--() {
      ++pos;
      return *this;
    }

    Common_reverse_iterator operator--(int) const {
      Common_reverse_iterator tmp = *this;
      --(*this);
      return tmp;
    }

    operator const_reverse_iterator() {
      return const_reverse_iterator(pos);
    }

    operator const_iterator() {
      return const_iterator(pos);
    }

    typename CommonIterator::pointer operator->() const {
      return pos.operator->();
    };

    friend bool operator==(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return first.pos == second.pos;
    }

    friend bool operator!=(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return !(first.pos == second.pos);
    }

   private:
    CommonIterator pos;
  };

  std::vector<Node *> hashes;
  size_t backets_capacity;
  float maximum_load_factor = 10;
  typename std::allocator_traits<Alloc>::template rebind_alloc<NodeType> alloc_node_type;
  typename std::allocator_traits<Alloc>::template rebind_alloc<Node> alloc_node;
  Hash hash_ = Hash();
  Node* begin_node = nullptr;
  Node* end_node = nullptr;
  size_t size_ = 0;

 public:
  iterator begin() {
    return iterator(begin_node);
  }

  iterator end() {
    return iterator((end_node == nullptr ? nullptr : end_node->next));
  }

  [[nodiscard]] const_iterator end() const {
    return cend();
  }

  [[nodiscard]] const_iterator begin() const {
    return cbegin();
  }

  [[nodiscard]] const_iterator cbegin() const {
    return const_iterator(begin_node);
  }

  [[nodiscard]] const_iterator cend() const {
    return const_iterator(end_node->next);
  }

  reverse_iterator rbegin() {
    return reverse_iterator(end_node);
  }

  const_reverse_iterator crbegin() const {
    return const_reverse_iterator(--cend());
  }

  reverse_iterator rend() {
    return reverse_iterator(begin_node->prev);
  }

  [[nodiscard]] const_reverse_iterator rend() const {
    return crend();
  }

  const_reverse_iterator crend() const {
    return const_reverse_iterator(--cbegin());
  }

  [[nodiscard]] const_reverse_iterator rbegin() const {
    return crbegin();
  }

  UnorderedMap() : hashes(t, nullptr), backets_capacity(0) {};

  UnorderedMap(UnorderedMap &&U_M) noexcept: hashes(std::move(U_M.hashes)),
                                             backets_capacity(U_M.backets_capacity),
                                             alloc_node_type(std::move(U_M.alloc_node_type)), alloc_node(std::move(U_M.alloc_node)),
                                             hash_(U_M.hash_),
                                             begin_node(std::move(U_M.begin_node)), end_node(std::move(U_M.end_node)), size_(U_M.size_) {}

  UnorderedMap(const UnorderedMap &U_M) {
    hashes.resize(U_M.hashes.size());
    backets_capacity = U_M.backets_capacity;
    if (U_M.begin_node == nullptr) {
      alloc_node = U_M.alloc_node;
      alloc_node_type = U_M.alloc_node_type;
      hash_ = U_M.hash_;
    }
    else {
      alloc_node = U_M.alloc_node;
      alloc_node_type = U_M.alloc_node_type;
      hash_ = U_M.hash_;
      for (auto it = U_M.begin(); it != U_M.end(); ++it) {
        insert(*it.get_node()->value);
      }
    }
  }

  UnorderedMap &operator=(UnorderedMap &&U_M) {
    hashes = std::move(U_M.hashes);
    backets_capacity = U_M.backets_capacity;
    alloc_node = std::move(U_M.alloc_node);
    alloc_node_type = std::move(U_M.alloc_node_type);
    hash_ = U_M.hash_;
    begin_node = U_M.begin_node;
    end_node = U_M.end_node;
    size_ = U_M.size_;
    return *this;
  }

  UnorderedMap &operator=(const UnorderedMap &U_M) {
    hashes.resize(U_M.hashes.size());
    backets_capacity = U_M.backets_capacity;
    if (U_M.begin_node == nullptr) {
      alloc_node = U_M.alloc_node;
      alloc_node_type = U_M.alloc_node_type;
      hash_ = U_M.hash_;
    }
    else {
      alloc_node = U_M.alloc_node;
      alloc_node_type = U_M.alloc_node_type;
      hash_ = U_M.hash_;
      for (auto it = U_M.begin(); it != U_M.end(); ++it) {
        insert(*it.get_node()->value);
      }
    }
    return *this;
  }

  ~UnorderedMap() = default;

  [[nodiscard]] size_t size() const {
    return size_;
  }

  template<typename A, typename B, typename U = Equal>
  bool f(A &a, B &b, U u = U()) const {
    return u(a, b);
  }

  float load_factor() const {
    if (backets_capacity == 0) {
      return size();
    }
    return size() / backets_capacity;
  }

  std::pair<iterator, bool> insert(const NodeType& node) {
    return emplace(node);
  }
  template<class P>
  std::pair<iterator, bool> insert(P && value) {
    return emplace(std::forward<P>(value));
  }
  std::pair<iterator, bool> insert(NodeType &&node) {
    return emplace(std::move(node));
  }

  template<typename ...Args>
  std::pair<iterator, bool> emplace(Args &&... args) {
    NodeType *node = std::allocator_traits<node_alloc_new>::allocate(alloc_node_type, 1);
    std::allocator_traits<node_alloc_new>::construct(alloc_node_type, node, std::forward<Args>(args)...);

    size_t h = hash_(node->first);
    if (hashes[h % hashes.size()] != nullptr) {
      iterator iter = iterator(hashes[hash_(node->first) % hashes.size()]);
      while (iter != end() &&
          iter.get_node()->hash__ % hashes.size() == hash_(node->first) % hashes.size()) {
        if (f(iter.get_node()->value->first, node->first)) {
          return std::make_pair(iter, false);
        } else {
          ++iter;
        }
      }
      Node* prev;
      Node* next;
      if (iter.get_node() != nullptr) {
        prev = iter.get_node()->prev;
        next = iter.get_node();
      }
      else {
        prev = nullptr;
        next = nullptr;
      }
      Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc_node, 1);
      std::allocator_traits<node_alloc>::construct(alloc_node, new_node, node,
                                                   prev, next, hash_);
      if (prev != nullptr) prev->next = new_node;
      if (next != nullptr) next->prev = new_node;
      ++size_;
      return std::make_pair(iter, true);
    }
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc_node, 1);
    std::allocator_traits<node_alloc>::construct(alloc_node, new_node, node,
                                                 nullptr, begin_node, hash_);
    ++backets_capacity;
    if (size_ == 0) {
      begin_node = new_node;
      end_node = new_node;
    }
    else if (size_ == 1) {
      begin_node->prev = new_node;
      new_node->next = begin_node;
      begin_node = new_node;
      end_node = begin_node->next;
    }
    else if (size_ >= 2) {
      begin_node->prev = new_node;
      new_node->next = begin_node;
      begin_node = new_node;
    }
    ++size_;
    iterator iter = begin();
    hashes[hash_(node->first) % hashes.size()] = iter.get_node();
    return std::make_pair(iter, true);
  }

  iterator erase(iterator it) {
    iterator new_it = (it == nullptr ? iterator(nullptr) : iterator(it.get_node()->next));
    size_t hash_new = it.get_node()->hash__;
    if (it != begin() && new_it != end() && it.get_node() != nullptr && new_it.get_node() != nullptr) {
      if ((--it).get_node()->hash__ != hash_new &&
          (new_it).get_node()->hash__ != hash_new) {
        ++it;
        hashes[hash_new % hashes.size()] = nullptr;
        --backets_capacity;
      }
    } else if (it != begin() && new_it == end() && it.get_node() != nullptr && new_it.get_node() != nullptr) {
      if ((--it).get_node()->hash__ != hash_new) {
        ++it;
        hashes[hash_new % hashes.size()] = nullptr;
        --backets_capacity;
      }
    } else if (it == begin() && it.get_node() != nullptr && new_it.get_node() != nullptr) {
      if ((new_it).get_node()->hash__ != hash_new) {
        begin_node = new_it.get_node();
        begin_node->prev = nullptr;
        hashes[hash_new % hashes.size()] = nullptr;
        --backets_capacity;
      }
    }
    Node *prev = it.get_node()->prev;
    Node *next = it.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }
    --size_;
    return new_it;
  }

  template<class InputIt>
  void insert(InputIt it1, InputIt it2) {
    if (std::is_trivially_move_constructible<Key>::value && std::is_move_constructible<Key>::value) {
      for (auto it = it1; it != it2; ++it) {
        insert(*it);
      }
    }
  }

  iterator erase(iterator it1, iterator it2) {
    auto new_it = (it2.get_node() == nullptr ? iterator(nullptr) : iterator(it2.get_node()->next));
    for (auto it = it1; it != new_it; ++it) {
      erase(it);
    }
    return new_it;
  }

  iterator find(const Key &key) {
    size_t new_hash = hash_(key) % hashes.size();
    if (hashes[new_hash] == nullptr) {
      return end();
    } else {
      Node *new_node = hashes[new_hash];
      while (new_node != nullptr && new_node->hash__ % hashes.size() == new_hash) {
        if (f(static_cast<Node *>(new_node)->value->first, key)) {
          return iterator(new_node);
        } else {
          new_node = new_node->next;
        }
      }
      return end();
    }
  }

  float max_load_factor() const {
    return maximum_load_factor;
  }

  const_iterator find(const Key &key) const {
    size_t new_hash = Hash(key) % hashes.size();
    if (hashes[new_hash] == nullptr) {
      return end();
    } else {
      Node *new_node = hashes[new_hash];
      while (new_node != nullptr && static_cast<Node *>(new_node)->hash_ % hashes.size() == new_hash) {
        if (f(static_cast<Node *>(new_node)->value.first, key)) {
          return const_iterator(new_node);
        } else {
          new_node = new_node->next;
        }
      }
      return end();
    }
  }

  void reserve(size_t count) {
    size_t new_size = std::ceil(count / maximum_load_factor);
    UnorderedMap new_U_M = UnorderedMap();
    new_U_M.hashes.assign(new_size, nullptr);
    for (Node* first = begin_node; first != nullptr; first = first->next) {
      NodeType* node = first->value;
      size_t h = new_U_M.hash_(node->first);
      if (new_U_M.hashes[h % new_U_M.hashes.size()] != nullptr) {
        iterator iter = iterator(new_U_M.hashes[h % new_U_M.hashes.size()]);
        while (iter != end() &&
            iter.get_node()->hash__ == h % new_U_M.hashes.size()) {
          if (f(iter.get_node()->value->first, node->first)) {
          } else {
            ++iter;
          }
        }
        Node *prev = iter.get_node()->prev;
        Node *next = iter.get_node();
        Node *new_node = std::allocator_traits<node_alloc>::allocate(new_U_M.alloc_node, 1);
        std::allocator_traits<node_alloc>::construct(new_U_M.alloc_node, new_node, node,
                                                     prev, next, new_U_M.hash_);
        if (prev != nullptr) prev->next = new_node;
        next->prev = new_node;
        ++new_U_M.size_;
      }
      Node *new_node = std::allocator_traits<node_alloc>::allocate(new_U_M.alloc_node, 1);
      std::allocator_traits<node_alloc>::construct(new_U_M.alloc_node, new_node, node,
                                                   nullptr, new_U_M.begin_node, new_U_M.hash_);
      ++new_U_M.backets_capacity;
      if (new_U_M.size_ == 0) {
        new_U_M.begin_node = new_node;
        new_U_M.end_node = new_node;
      }
      else if (new_U_M.size_ == 1) {
        new_U_M.begin_node->prev = new_node;
        new_node->next = new_U_M.begin_node;
        new_U_M.begin_node = new_node;
        new_U_M.end_node = new_U_M.begin_node->next;
      }
      else if (new_U_M.size_ >= 2) {
        new_U_M.begin_node->prev = new_node;
        new_node->next = new_U_M.begin_node;
        new_U_M.begin_node = new_node;
      }
      ++new_U_M.size_;
      iterator iter = new_U_M.begin();
      new_U_M.hashes[new_U_M.hash_(node->first) % new_U_M.hashes.size()] = iter.get_node();
    }
    *this = std::move(new_U_M);
  }

  Value &operator[](const Key &key) {
    auto it = find(key);
    if (it == end()) {
      insert(std::make_pair(key, Value()));
      return begin().get_node()->value->second;
    } else {
      return it.get_node()->value->second;
    }
  }

  Value &at(const Key &key) {
    auto it = find(key);
    if (it == end()) {
      throw std::out_of_range("out_of_range");
    } else {
      return it.get_node()->value->second;
    }
  }

  void pri() {
    Node* tmp = begin_node;
    while (tmp != nullptr) {
      std::cout << tmp->value->first << ' ' << tmp->value->second << std::endl;
      tmp = tmp->next;
    }
  }
};