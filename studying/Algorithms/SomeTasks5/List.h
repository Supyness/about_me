#include <chrono>
#include <stdexcept>
#include <list>
#include <memory>
#include <iostream>
#include <fstream>

template<size_t N>
class StackStorage {
 private:
  char _memory[N];
  size_t _capacity = 0;
 public:
  char *allocate(size_t cap, size_t weight) {
    if (_memory + N >= _memory + _capacity + cap) {
      _capacity += (_capacity % weight != 0 ? weight - (_capacity % weight) : 0);
      auto tmp(_memory + _capacity);
      _capacity += cap;
      return tmp;
    }
    return nullptr;
  }
  void deallocate(const char *ptr, size_t cap) noexcept {
    if (ptr <= _memory + N) {
      if (ptr + cap == _memory + _capacity) {
        _capacity -= cap;
      }
    }
  }
};

template<typename T, size_t N>
class StackAllocator {
 private:
  StackStorage<N> &_storage;
 public:
  typedef T value_type;
  typedef T *pointer;
  typedef size_t size_type;
  StackAllocator() = default;
  StackAllocator(StackStorage<N> &_second) : _storage(std::make_shared<StackStorage<N>>(_second)) {};
  StackAllocator(const StackAllocator<T, N> &_second) : _storage(_second._storage) {};
  template<class U>
  struct rebind {
    typedef StackAllocator<U, N> other;
  };
  template<typename U, size_t M>
  friend
  class StackAllocator;
  template<class U>
  StackAllocator(const StackAllocator<U, N> &_second)
      : _storage(_second._storage) {};
  StackAllocator<T, N> &operator=(const StackAllocator<T, N> &);
  ~StackAllocator();
  pointer allocate(size_t cap) {
    return reinterpret_cast<T *>((_storage->allocate(cap * sizeof(T), sizeof(T))));
  };
  void deallocate(pointer p, size_type cap) {
    _storage->deallocate(reinterpret_cast<char *>(p), cap * sizeof(T));
  };
};

template<typename T, size_t N>
StackAllocator<T, N>::~StackAllocator() {
  _storage.reset();
}

template<typename T, size_t N>
StackAllocator<T, N> &StackAllocator<T, N>::operator=(const StackAllocator<T, N> &_second) {
  _storage = _second._storage;
  return *this;
}

template<typename T, typename Alloc = std::allocator<T>>
class List {
 public:
  typedef T value_type;
  typedef Alloc allocator_type;
  using AllocTraits = std::allocator_traits<allocator_type>;
  typedef size_t size_type;
  template<bool is_const>
  class CommonIterator;
  template<typename CommonIterator>
  class Common_reverse_iterator;
  using iterator = CommonIterator<false>;
  using const_iterator = CommonIterator<true>;
  using reverse_iterator = Common_reverse_iterator<iterator>;
  using const_reverse_iterator = Common_reverse_iterator<const_iterator>;
  Alloc get_allocator() const {
    return alloc;
  }
  List() : _size(0) {
    fakenode = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
  };
  List(const Alloc &other) : alloc(other), _size(0) {
    fakenode = alloc.allocate(1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
  }
  List(size_t sz, const Alloc &other = Alloc()) : alloc(other), _size(0) {
    fakenode = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
    for (size_t i = 0; i < sz; ++i) {
      try {
        emplace_back(T());
      }
      catch (...) {
        for (size_t j = 0; j < i; ++j) {
          pop_back();
        }
        throw std::string("lalala coconut");
      }
    }
  }
  List(size_t sz, T &num, const Alloc &other) : alloc(other), _size(0) {
    fakenode = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
    size_t k = 0;
    try {
      for (size_t i = 0; i < sz; ++i) {
        push_back(num);
        ++k;
      }
    }
    catch (...) {
      for (size_t i = 0; i < k; ++i) {
        pop_back();
      }
    }
  }
  List(size_t sz, const T &num) {
    fakenode = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
    for (size_t i = 0; i < sz; ++i) {
      push_back(num);
    }
  }
  size_t size() const {
    return _size;
  }
  List(const List &lst) {
    alloc = std::allocator_traits<node_alloc>::select_on_container_copy_construction(lst.get_allocator());
    while (_size != 0) {
      pop_back();
      --_size;
    }
    _size = 0;
    fakenode = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    fakenode->prev = nullptr;
    fakenode->next = nullptr;
    auto it = lst.begin();
    size_t sz = 0;
    while (it != lst.end()) {
      try {
        emplace_back(*it);
        ++it;
        ++sz;
      }
      catch (...) {
        for (size_t i = 0; i < sz; ++i) {
          pop_back();
        }
        throw std::string("lalala coconut");
      }
    }
  }
  List &operator=(const List &lst) {
    if (AllocTraits::propagate_on_container_copy_assignment::value) {
      alloc = lst.get_allocator();
    };
    size_t sz1 = _size;
    auto it = lst.begin();
    size_t sz = 0;
    while (it != lst.end()) {
      try {
        emplace_back(*it);
        ++it;
        ++sz;
      }
      catch (...) {
        for (size_t i = 0; i < sz; ++i) {
          pop_back();
        }
        throw std::string("lalala coconut");
      }
    }
    while (sz1 != 0) {
      pop_front();
      --sz1;
    }
    return *this;
  }
  ~
  List() {
    size_t sz = _size;
    while (sz != 0) {
      pop_back();
      --sz;
    }
    fakenode = nullptr;
    _size = 0;
  }
  void pri() {
    Basenode *node = fakenode->next;
    while (node != fakenode) {
      std::cout << node->value << std::endl;
      node = node->next;
    }
  }
  template<typename... Args>
  void emplace_back1(const Args &... args) {
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    //alloc.construct(new_node, args...);
    new_node->next = fakenode;
    new_node->prev = (fakenode->prev == nullptr ? fakenode : fakenode->prev);
    if (fakenode->prev != nullptr) fakenode->prev->next = new_node;
    else {
      fakenode->next = new_node;
    }
    fakenode->prev = new_node;
    ++_size;
  }
  template<typename... Args>
  void emplace_back(const Args &... args) {
    try {
      Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
      std::allocator_traits<node_alloc>::construct(alloc, new_node, args...);
      new_node->next = fakenode;
      new_node->prev = (fakenode->prev == nullptr ? fakenode : fakenode->prev);
      if (fakenode->prev != nullptr) fakenode->prev->next = new_node;
      else {
        fakenode->next = new_node;
      }
      fakenode->prev = new_node;
      ++_size;
    }
    catch (...) {
      throw std::string("lalala coconut");
    }
  }
  void push_back(const T &value) {
    try {
      Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
      std::allocator_traits<node_alloc>::construct(alloc, new_node, value,
                                                   (fakenode->prev == nullptr ? fakenode : fakenode->prev),
                                                   fakenode);
      if (fakenode->prev != nullptr) fakenode->prev->next = new_node;
      else {
        fakenode->next = new_node;
      }
      fakenode->prev = new_node;
      ++_size;
    }
    catch (...) {
      throw std::string("lalala coconut");
    }
  }
  void push_front(const T &value) {
    try {
      Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
      std::allocator_traits<node_alloc>::construct(alloc, new_node, value, fakenode,
                                                   (fakenode->next == nullptr ? fakenode : fakenode->next));
      if (fakenode->next != nullptr) fakenode->next->prev = new_node;
      else {
        fakenode->prev = new_node;
      }
      fakenode->next = new_node;
      ++_size;
    }
    catch (...) {
      throw std::string("lalala coconut");
    }
  }
  void pop_back() {
    Node *new_node = static_cast<Node *>(fakenode->prev);
    if (new_node != nullptr) {
      fakenode->prev = new_node->prev;
      if (new_node->prev != nullptr) new_node->prev->next = fakenode;
      std::allocator_traits<node_alloc>::destroy(alloc, new_node);
      if (new_node->prev != nullptr && new_node->next != nullptr)
        std::allocator_traits<node_alloc>::deallocate(alloc,
                                                      new_node,
                                                      1);
      --_size;
    }
  }
  void pop_front() {
    Node *new_node = static_cast<Node *>(fakenode->next);
    fakenode->next = new_node->next;
    if (new_node->next != nullptr) {
      new_node->next->prev = fakenode;
    }
    --_size;
    std::allocator_traits<node_alloc>::destroy(alloc, new_node);
    std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
  }
 private:
  struct Basenode {
    Basenode *prev;
    Basenode *next;
    Basenode() : prev(nullptr), next(nullptr) {};
    Basenode(Basenode *first, Basenode *second) : prev(first), next(second) {};
    ~Basenode() = default;
  };
  struct Node : Basenode {
    T value;
    Node(const T &val, Basenode *first, Basenode *second) : Basenode(first, second), value(val) {
    };
    Node(const T &value) : Basenode(), value(value) {};
    Node() : Basenode(), value(T()) {};
    ~Node() = default;
  };
  typename AllocTraits::template rebind_alloc<Node> alloc;
  Basenode *fakenode;
  size_t _size = 0;
 public:
  typedef typename AllocTraits::template rebind_alloc<Node> node_alloc;
  template<bool is_const>
  class CommonIterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = T;
    using difference_type = long long;
    using pointer = typename std::conditional<is_const, const T *, T *>::type;
    using reference = typename std::conditional<is_const, const T &, T &>::type;
    CommonIterator(Basenode *pos) : pos(pos) {};
    CommonIterator() : pos(nullptr) {};
    CommonIterator(const CommonIterator &first) : pos(first.pos) {};
    ~CommonIterator() = default;
    reference operator*() const { return static_cast<Node *>(pos)->value; }
    CommonIterator &operator++() {
      pos = pos->next;
      return *this;
    }
    Basenode *get_node() {
      return pos;
    }
    CommonIterator operator++(int) {
      CommonIterator tmp = *this;
      ++(*this);
      return tmp;
    }
    pointer operator->() const {
      return &static_cast<Node *>(pos)->value;
    };
    CommonIterator &operator--() {
      pos = pos->prev;
      return *this;
    }
    CommonIterator operator--(int) const {
      CommonIterator tmp = *this;
      --(*this);
      return tmp;
    }
    operator const_iterator() const {
      return const_iterator(pos);
    }
    friend bool operator==(const CommonIterator &first, const CommonIterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const CommonIterator &first, const CommonIterator &second) {
      return !(first == second);
    }
   private:
    Basenode *pos;
  };
  template<typename CommonIterator>
  class Common_reverse_iterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    Common_reverse_iterator(CommonIterator pos) : pos(pos) {};
    typename CommonIterator::reference operator*() const { return *pos; }
    Common_reverse_iterator &operator++() {
      --pos;
      return *this;
    }
    Common_reverse_iterator operator++(int) const {
      Common_reverse_iterator tmp = *this;
      ++(*this);
      return tmp;
    }
    Basenode *get_node() {
      return pos.get_node();
    }
    Common_reverse_iterator base() {
      return Common_reverse_iterator(CommonIterator(pos.get_node()->next));
    }
    Common_reverse_iterator &operator--() {
      ++pos;
      return *this;
    }
    Common_reverse_iterator operator--(int) const {
      Common_reverse_iterator tmp = *this;
      --(*this);
      return tmp;
    }
    operator const_reverse_iterator() {
      return const_reverse_iterator(pos);
    }
    operator const_iterator() {
      return const_iterator(pos);
    }
    typename CommonIterator::pointer operator->() const {
      return pos.operator->();
    };
    friend bool operator==(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return !(first.pos == second.pos);
    }
   private:
    CommonIterator pos;
  };
  iterator begin() {
    return iterator(fakenode->next);
  }
  iterator end() {
    return iterator(fakenode);
  }
  [[nodiscard]] const_iterator end() const {
    return cend();
  }
  [[nodiscard]] const_iterator begin() const {
    return cbegin();
  }
  [[nodiscard]] const_iterator cbegin() const {
    return const_iterator(fakenode->next);
  }
  [[nodiscard]] const_iterator cend() const {
    return const_iterator(fakenode);
  }
  reverse_iterator rbegin() {
    return reverse_iterator(fakenode->prev);
  }
  const_reverse_iterator crbegin() const {
    return const_reverse_iterator(--cend());
  }
  reverse_iterator rend() {
    return reverse_iterator(fakenode);
  }
  [[nodiscard]] const_reverse_iterator rend() const {
    return crend();
  }
  const_reverse_iterator crend() const {
    return const_reverse_iterator(--cbegin());
  }
  [[nodiscard]] const_reverse_iterator rbegin() const {
    return crbegin();
  }
  void insert(const_reverse_iterator pos, const T &value) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node();
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    std::allocator_traits<node_alloc>::construct(alloc, new_node, value,
                                                 prev, next);
    if (prev != nullptr) prev->next = new_node;
    next->prev = new_node;
    ++_size;
  }
  void insert(const_iterator pos, const T &value) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node();
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    std::allocator_traits<node_alloc>::construct(alloc, new_node, value,
                                                 prev, next);
    if (prev != nullptr) prev->next = new_node;
    next->prev = new_node;
    ++_size;
  }
  void erase(iterator pos) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }

    --_size;
  }
  void erase(const_iterator pos) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }
    Node *new_node = static_cast<Node *>(pos.get_node());
    std::allocator_traits<node_alloc>::destroy(alloc, new_node);
    std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
    --_size;
  }
  void erase(reverse_iterator pos) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }
    --_size;
  }
  void erase(const_reverse_iterator pos) {
    Basenode *prev = pos.get_node()->prev;
    Basenode *next = pos.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }
    --_size;
  }
};