#include <iostream>
#include <vector>

template<typename T = int>
class Deque {
 public:
  template<bool is_const>
  class CommonIterator;

  template<typename CommonIterator>
  class CommonReverseIterator;

  Deque() : carry(1), size_(0), start(0) {
    deq = new T *[1];
    try {
      deq[0] = reinterpret_cast<T *>(new uint8_t[capacity * sizeof(T)]);
    }
    catch (...) {
      delete deq;
    }
  };

  Deque(const Deque &D) : size_(D.size_), start(D.start), deq(D.deq) {
    *this = Deque<T>();
    int k = 0;
    try {
      for (const_iterator it = D.begin(); it != D.end(); ++it) {
        push_back(*it);
        ++k;
      }
    }
    catch (...) {
      for (int i = 0; i < k; ++i) {
        pop_back();
      }
    }
  };

  Deque(const int k) : Deque(k, T()) {
  };

  Deque(const int k, const T &elem) {
    start = 0;
    carry = k / capacity + 1;
    deq = new T *[carry];
    for (size_t i = 0; i < carry; ++i) {
      deq[i] = reinterpret_cast<T *>(new uint8_t[capacity * sizeof(T)]);
    }
    for (int i = 0; i < k; ++i) {
      push_back(elem);
    }
  }

  ~Deque() {
    while (size_ != 0) pop_back();
  }

  void push_front(const T &elem) {
    if (start == 0) resize();
    iterator back = begin() - 1;
    new(&(*back)) T(elem);
    --start;
    ++size_;
  }

  void push_back(const T &elem) {
    if (start + size_ == carry * capacity) resize();
    iterator front = end();
    new(&(*front)) T(elem);
    ++size_;
  }

  void pop_front() noexcept {
    iterator back = begin();
    back->~T();
    ++start;
    --size_;
  }

  void pop_back() noexcept {
    iterator front = end() - 1;
    front->~T();
    --size_;
  }

  size_t size() const {
    return size_;
  }

  T operator[](const size_t ind) const {
    return *(begin() + ind);
  }

  T &operator[](const size_t ind) {
    return *(begin() + ind);
  }

  T &at(size_t pos) {
    if (begin() + pos >= end()) throw std::out_of_range("out_of_range");
    else {
      return (*this)[pos];
    }
  }

  using iterator = CommonIterator<false>;
  using const_iterator = CommonIterator<true>;
  using reverse_iterator = CommonReverseIterator<iterator>;
  using const_reverse_iterator = CommonReverseIterator<const_iterator>;

  template<bool is_const>
  class CommonIterator {
   private:
    T *pos;
    T **curent_pos;
   public:
    using value_type = T;
    using difference_type = long long;
    using pointer = typename std::conditional<is_const, const T *, T *>::type;
    using reference = typename std::conditional<is_const, const T &, T &>::type;

    CommonIterator(T *pos, T **curent_pos) : pos(pos), curent_pos(curent_pos) {};

    CommonIterator() : pos(nullptr), curent_pos(nullptr) {};

    CommonIterator(const CommonIterator &first) : pos(first.pos), curent_pos(first.curent_pos) {};

    ~CommonIterator() = default;

    reference operator*() const { return *pos; }

    CommonIterator &operator++() {
      *this += 1;
      return *this;
    }

    CommonIterator operator++(int) const {
      CommonIterator tmp = *this;
      ++(*this);
      return tmp;
    }

    pointer operator->() const {
      return pos;
    };

    CommonIterator &operator--() {
      if (pos == *curent_pos) {
        --curent_pos;
        pos = *curent_pos + capacity;
      }
      --pos;
      return *this;
    }

    CommonIterator operator--(int) const {
      CommonIterator tmp = *this;
      --(*this);
      return tmp;
    }

    CommonIterator &operator+=(long long n) {
      if (n != 0) {
        n += pos - *curent_pos;
        if (n > 0) {
          curent_pos += n / capacity;
          pos = *curent_pos + n % capacity;
        } else {
          n = capacity - 1 - n;
          curent_pos -= n / capacity;
          pos = *curent_pos + (capacity - 1 - n % capacity);
        }
      }
      return *this;
    }

    CommonIterator &operator-=(long long n) {
      return *this += -n;
    }

    CommonIterator operator+(long long n) const {
      CommonIterator tmp = *this;
      tmp += n;
      return tmp;
    }

    CommonIterator operator-(long long n) const {
      CommonIterator tmp = *this;
      tmp -= n;
      return tmp;
    }

    operator const_iterator() const {
      return const_iterator(pos, curent_pos);
    }

    friend difference_type operator-(const CommonIterator &first, const CommonIterator &second) {
      return (first.curent_pos - second.curent_pos) * capacity
          + (first.pos - *first.curent_pos - (second.pos - *second.curent_pos));
    }

    friend bool operator==(const CommonIterator &first, const CommonIterator &second) {
      return first.pos == second.pos;
    }

    friend bool operator<(const CommonIterator &first, const CommonIterator &second) {
      return (first.curent_pos < second.curent_pos
          || (first.curent_pos == second.curent_pos && first.pos < second.pos));
    }

    friend bool operator!=(const CommonIterator &first, const CommonIterator &second) {
      return !(first == second);
    }

    friend bool operator>(const CommonIterator &first, const CommonIterator &second) {
      return second < first;
    }

    friend bool operator<=(const CommonIterator &first, const CommonIterator &second) {
      return !(second < first);
    }

    friend bool operator>=(const CommonIterator &first, const CommonIterator &second) {
      return !(first < second);
    }
  };

  template<typename CommonIterator>
  class CommonReverseIterator {
   private:
    CommonIterator pos;
   public:
    CommonReverseIterator(CommonIterator pos) : pos(pos) {};

    typename CommonIterator::reference operator*() const { return *pos; }

    CommonReverseIterator &operator++() {
      --pos;
      return *this;
    }

    CommonReverseIterator operator++(int) const {
      CommonReverseIterator tmp = *this;
      ++(*this);
      return tmp;
    }

    CommonReverseIterator &operator--() {
      ++pos;
      return *this;
    }

    CommonReverseIterator &operator--(int) const {
      CommonReverseIterator tmp = *this;
      --(*this);
      return tmp;
    }

    typename CommonIterator::pointer operator->() const {
      return pos.operator->();
    };

    CommonReverseIterator &operator+=(long long n) {
      pos -= n;
    }

    CommonReverseIterator &operator-=(long long n) {
      pos += n;
    }

    CommonReverseIterator operator+(long long n) const {
      CommonReverseIterator tmp = *this;
      tmp -= n;
      return tmp;
    }

    CommonReverseIterator operator-(long long n) const {
      CommonReverseIterator tmp = *this;
      tmp += n;
      return tmp;
    }

    operator const_reverse_iterator() const {
      return const_reverse_iterator(pos);
    }

    friend typename CommonIterator::difference_type
    operator-(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return (first.pos - second.pos);
    }

    friend bool operator==(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return first.pos == second.pos;
    }

    friend bool operator<(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return (first.pos > second.pos);
    }

    friend bool operator!=(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return !(first.pos == second.pos);
    }

    friend bool operator>(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return second.pos > first.pos;
    }

    friend bool operator<=(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return !(second.pos > first.pos);
    }

    friend bool operator>=(const CommonReverseIterator &first, const CommonReverseIterator &second) {
      return !(first.pos > second.pos);
    }
  };

  void insert(iterator pos, const T &elem) {
    Deque<T> new_deq = Deque<T>();
    for (iterator it = begin(); it != pos; ++it) {
      new_deq.push_back(*it);
    }
    new_deq.push_back(elem);
    for (iterator it = pos; it != end(); ++it) {
      new_deq.push_back(*it);
    }
    std::swap(*this, new_deq);
  }

  void erase(iterator pos) {
    Deque<T> new_deq = Deque<T>();
    for (iterator it = begin(); it != pos; ++it) {
      new_deq.push_back(*it);
    }
    for (iterator it = pos + 1; it != end(); ++it) {
      new_deq.push_back(*it);
    }
    std::swap(*this, new_deq);
  }

  iterator begin() noexcept {
    T **sp = deq + start / capacity;
    T *sp1 = *sp + start % capacity;
    return iterator(sp1, sp);
  }

  iterator end() noexcept {
    T **sp = deq + (start + size_) / capacity;
    T *sp1 = *sp + (start + size_) % capacity;
    return iterator(sp1, sp);
  }

  [[nodiscard]] const_iterator end() const noexcept {
    return cend();
  }

  [[nodiscard]] const_iterator begin() const noexcept {
    return cbegin();
  }

  [[nodiscard]] const_iterator cbegin() const noexcept {
    T **sp = deq + start / capacity;
    T *sp1 = *sp + start % capacity;
    return const_iterator(sp1, sp);
  }

  [[nodiscard]] const_iterator cend() const noexcept {
    T **sp = deq + (start + size_) / capacity;
    T *sp1 = *sp + (start + size_) % capacity;
    return const_iterator(sp1, sp);
  }

  reverse_iterator rbegin() noexcept {
    return reverse_iterator(end() - 1);
  }

  [[nodiscard]] const_reverse_iterator rbegin() const noexcept {
    return crbegin();
  }

  const_reverse_iterator crbegin() noexcept {
    return const_reverse_iterator(cend() - 1);
  }

  reverse_iterator rend() noexcept {
    return reverse_iterator(begin() - 1);
  }

  [[nodiscard]] const_reverse_iterator rend() const noexcept {
    return crend();
  }

  const_reverse_iterator crend() noexcept {
    return const_reverse_iterator(cbegin() - 1);
  }

 private:
  size_t carry;
  size_t size_ = 0;
  size_t start;
  T **deq;
  static const int capacity = 256;
  void resize() {
    T **deq1 = new T *[carry * 3];
    size_t i = 0;
    try {
      for (i = 0; i < carry * 3; ++i) {
        if (i < carry || i >= 2 * carry) {
          deq1[i] = reinterpret_cast<T *>(new uint8_t[capacity * sizeof(T)]);
        } else {
          deq1[i] = deq[i - carry];
        }
      }
      start = carry * capacity;
      carry *= 3;
    }
    catch (...) {
      for (size_t j = 0; j < i; ++j) {
        delete deq1[j];
      }
      delete[] deq1;
    }
    std::swap(deq, deq1);
  }
};