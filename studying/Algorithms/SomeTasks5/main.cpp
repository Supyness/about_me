#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>


int main()
{
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<int>> matrix(n, std::vector<int>(m, 0));
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      std::cin >> matrix[i][j];
    }
  }
  int target; std::cin >> target;
  int l = 0, r = matrix.size();
  while (l < r) {
    int m = (l + r) / 2;
    if (matrix[m][0] > target) r = m;
    else l = m;
  }
  int ind = l;
  l = 0, r = matrix[0].size();
  while (l < r) {
    int m = (l + r) / 2;
    if (matrix[ind][m] > target) r = m;
    else l = m;
  }
  if (matrix[ind][l] == target || matrix[ind][r] == target) return true;
  return false;
}