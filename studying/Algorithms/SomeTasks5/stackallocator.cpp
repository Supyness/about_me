#include <chrono>
#include <stdexcept>
#include <list>
#include <memory>
#include <iostream>
#include <fstream>
#include <vector>

template<size_t N>
class StackStorage {
 private:
  char _memory[N];
  size_t _capacity = 0;
  size_t space_left = N;
  void* used = _memory;
 public:
  char *allocate(size_t cap, size_t weight) {
    if (std::align(weight, cap, used, space_left)) {
      char* tmp = reinterpret_cast<char*>(used);
      used = (char*)used + cap;
      space_left -= cap;
      return tmp;
    }
    return nullptr;
  }
  void deallocate(const char *ptr, size_t cap) noexcept {
    if (ptr <= _memory + N) {
      if (ptr + cap == _memory + _capacity) {
        _capacity -= cap;
      }
    }
  }
};

template<typename T, size_t N>
class StackAllocator {
 private:
  StackStorage<N>* _storage;
 public:
  typedef T value_type;
  typedef T *pointer;
  typedef size_t size_type;
  StackAllocator() = default;
  StackAllocator(StackStorage<N> &_second) : _storage(&_second) {};
  StackAllocator(const StackAllocator<T, N> &_second) : _storage(_second._storage) {};
  template<class U>
  struct rebind {
    typedef StackAllocator<U, N> other;
  };
  template<typename U, size_t M>
  friend class StackAllocator;
  template<class U>
  StackAllocator(const StackAllocator<U, N> &_second)
      : _storage(_second._storage) {};
  StackAllocator<T, N> &operator=(const StackAllocator<T, N> &);
  ~StackAllocator();
  pointer allocate(size_t cap) {
    return reinterpret_cast<T *>((_storage->allocate(cap * sizeof(T), sizeof(T))));
  };
  void deallocate(pointer p, size_type cap) {
    _storage->deallocate(reinterpret_cast<char *>(p), cap * sizeof(T));
  };
};

template<typename T, size_t N>
StackAllocator<T, N>::~StackAllocator() = default;

template<typename T, size_t N>
StackAllocator<T, N> &StackAllocator<T, N>::operator=(const StackAllocator<T, N> &_second) {
  _storage = _second._storage;
  return *this;
}

template<typename T, typename Alloc = std::allocator<T>>
class List {
 public:
  typedef T value_type;
  typedef Alloc allocator_type;
  using AllocTraits = std::allocator_traits<allocator_type>;
  typedef size_t size_type;
  template<bool is_const>
  class CommonIterator;
  template<typename CommonIterator>
  class Common_reverse_iterator;
  using iterator = CommonIterator<false>;
  using const_iterator = CommonIterator<true>;
  using reverse_iterator = Common_reverse_iterator<iterator>;
  using const_reverse_iterator = Common_reverse_iterator<const_iterator>;
  Alloc get_allocator() const {
    return alloc;
  }
  List(const Alloc& other = Alloc()) : alloc(other), _size(0) {
    fakenode.prev = nullptr;
    fakenode.next = nullptr;
  }
  List(size_t sz, const Alloc& other = Alloc()) : alloc(other), _size(0) {
    fakenode.prev = nullptr;
    fakenode.next = nullptr;
    for (size_t i = 0; i < sz; ++i) {
      try {
        emplace_back();
      }
      catch(...) {
        for (size_t j = 0; j < i; ++j) {
          pop_back();
        }
        throw std::string("lalala coconut");
      }
    }
  }
  List(size_t sz, T& num, const Alloc& other = Alloc()) : alloc(other), _size(0) {
    fakenode.prev = nullptr;
    fakenode.next = nullptr;
    size_t nw = 0;
    try {
      for (size_t i = 0; i < sz; ++i) {
        push_back(num);
        ++nw;
      }
    }
    catch(...) {
      for (size_t i = 0; i < nw; ++i) {
        pop_back();
      }
      throw;
    }
  }
  size_t size() const {
    return _size;
  }
  List(const List& lst) {
    alloc = std::allocator_traits<node_alloc>::select_on_container_copy_construction(lst.get_allocator());
    while (_size != 0) {
      pop_back();
      --_size;
    }
    _size = 0;
    fakenode.prev = nullptr;
    fakenode.next = nullptr;
    auto it = lst.begin();
    size_t sz = 0;
    while (it != lst.end()) {
      try {
        emplace_back(*it);
        ++it;
        ++sz;
      }
      catch(...) {
        for (size_t i = 0; i < sz; ++i) {
          pop_back();
        }
        throw;
      }
    }
  }
  List& operator=(const List& lst) {
    if (AllocTraits::propagate_on_container_copy_assignment::value) {
      alloc = lst.get_allocator();
    };
    size_t sz1 = _size;
    auto it = lst.begin();
    size_t sz = 0;
    while (it != lst.end()) {
      try {
        emplace_back(*it);
        ++it;
        ++sz;
      }
      catch(...) {
        for (size_t i = 0; i < sz; ++i) {
          pop_back();
        }
        throw;
      }
    }
    while (sz1 != 0) {
      pop_front();
      --sz1;
    }
    return *this;
  }
  ~List() {
    size_t sz = _size;
    while (sz != 0) {
      pop_back();
      --sz;
    }
    _size = 0;
  }
  template<typename... Args>
  void emplace_back(const Args&... args) {
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    try {
      std::allocator_traits<node_alloc>::construct(alloc, new_node, args...);
      new_node->next = &fakenode;
      new_node->prev = (fakenode.prev == nullptr ? &fakenode : fakenode.prev);
      if (fakenode.prev != nullptr) fakenode.prev->next = new_node;
      else {
        fakenode.next = new_node;
      }
      fakenode.prev = new_node;
      ++_size;
    }
    catch(...) {
      throw;
    }
  }
  void push_back(const T& value) {
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    try {
      std::allocator_traits<node_alloc>::construct(alloc, new_node, value,
                                                   (fakenode.prev == nullptr ? &fakenode : fakenode.prev),
                                                   &fakenode);
      if (fakenode.prev != nullptr) fakenode.prev->next = new_node;
      else {
        fakenode.next = new_node;
      }
      fakenode.prev = new_node;
      ++_size;
    }
    catch(...) {
      std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
      throw;
    }
  }
  void push_front(const T& value) {
    Node *new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    try {
      std::allocator_traits<node_alloc>::construct(alloc, new_node, value, &fakenode,
                                                   (fakenode.next == nullptr ? &fakenode : fakenode.next));
      if (fakenode.next != nullptr) fakenode.next->prev = new_node;
      else {
        fakenode.prev = new_node;
      }
      fakenode.next = new_node;
      ++_size;
    }
    catch(...) {
      std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
      throw;
    }
  }
  void pop_back() noexcept {
    Node* new_node = static_cast<Node*>(fakenode.prev);
    if (new_node != nullptr) {
      fakenode.prev = new_node->prev;
      if (new_node->prev != nullptr) new_node->prev->next = &fakenode;
      std::allocator_traits<node_alloc>::destroy(alloc, new_node);
      if (new_node->prev != nullptr && new_node->next != nullptr) std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
      --_size;
    }
  }
  void pop_front() noexcept {
    Node* new_node = static_cast<Node*>(fakenode.next);
    fakenode.next = new_node->next;
    if (new_node->next != nullptr) {
      new_node->next->prev = &fakenode;
    }
    --_size;
    std::allocator_traits<node_alloc>::destroy(alloc, new_node);
    std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
  }
 private:
  struct Basenode {
    Basenode *prev;
    Basenode *next;
    Basenode() : prev(nullptr), next(nullptr) {};
    Basenode(Basenode* first, Basenode* second) : prev(first), next(second) {};
    ~Basenode() = default;
  };
  struct Node : Basenode{
    T value;
    Node(const T& val, Basenode* first, Basenode* second) : Basenode(first, second), value(val) {
    };
    Node(const T &value) : Basenode(), value(value) {};
    Node() : Basenode(), value(T()) {};
    ~Node() = default;
  };
  typename AllocTraits::template rebind_alloc<Node> alloc;
  mutable Basenode fakenode;
  size_t _size = 0;
 public:
  typedef typename AllocTraits::template rebind_alloc<Node> node_alloc;
  template<bool is_const>
  class CommonIterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = T;
    using difference_type = long long;
    using pointer = typename std::conditional<is_const, const T *, T *>::type;
    using reference = typename std::conditional<is_const, const T &, T &>::type;
    CommonIterator(Basenode* pos) : pos(pos) {};
    CommonIterator() : pos(nullptr) {};
    CommonIterator(const CommonIterator &first) : pos(first.pos) {};
    ~CommonIterator() = default;
    reference operator*() const { return static_cast<Node*>(pos)->value; }
    CommonIterator &operator++() {
      pos = pos->next;
      return *this;
    }
    Basenode* get_node() {
      return pos;
    }
    CommonIterator operator++(int) {
      CommonIterator tmp = *this;
      ++(*this);
      return tmp;
    }
    pointer operator->() const {
      return &static_cast<Node*>(pos)->value;
    };
    CommonIterator &operator--() {
      pos = pos->prev;
      return *this;
    }
    CommonIterator operator--(int) const {
      CommonIterator tmp = *this;
      --(*this);
      return tmp;
    }
    operator const_iterator() const {
      return const_iterator(pos);
    }
    friend bool operator==(const CommonIterator &first, const CommonIterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const CommonIterator &first, const CommonIterator &second) {
      return !(first == second);
    }
   private:
    Basenode *pos;
  };
  template<typename CommonIterator>
  class Common_reverse_iterator {
   public:
    using iterator_category = std::bidirectional_iterator_tag;
    Common_reverse_iterator(CommonIterator pos) : pos(pos) {};
    typename CommonIterator::reference operator*() const { return *pos; }
    Common_reverse_iterator &operator++() {
      --pos;
      return *this;
    }
    Common_reverse_iterator operator++(int) const {
      Common_reverse_iterator tmp = *this;
      ++(*this);
      return tmp;
    }
    Basenode* get_node() {
      return pos.get_node();
    }
    Common_reverse_iterator base() {
      return Common_reverse_iterator(CommonIterator(pos.get_node()->next));
    }
    Common_reverse_iterator &operator--() {
      ++pos;
      return *this;
    }
    Common_reverse_iterator operator--(int) const {
      Common_reverse_iterator tmp = *this;
      --(*this);
      return tmp;
    }
    operator const_reverse_iterator() {
      return const_reverse_iterator(pos);
    }
    operator const_iterator() {
      return const_iterator(pos);
    }
    typename CommonIterator::pointer operator->() const {
      return pos.operator->();
    };
    friend bool operator==(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return first.pos == second.pos;
    }
    friend bool operator!=(const Common_reverse_iterator &first, const Common_reverse_iterator &second) {
      return !(first.pos == second.pos);
    }
   private:
    CommonIterator pos;
  };
  iterator begin() noexcept {
    return iterator(fakenode.next);
  }
  iterator end() noexcept {
    return iterator(&fakenode);
  }
  const_iterator end() const noexcept {
    return cend();
  }
  const_iterator begin() const noexcept {
    return cbegin();
  }
  const_iterator cbegin() const noexcept {
    return const_iterator(fakenode.next);
  }
  const_iterator cend() const noexcept {
    return const_iterator(&fakenode);
  }
  reverse_iterator rbegin() noexcept {
    return reverse_iterator(fakenode.prev);
  }
  const_reverse_iterator crbegin() const noexcept {
    return const_reverse_iterator(--cend());
  }
  reverse_iterator rend() noexcept {
    return reverse_iterator(&fakenode);
  }
  const_reverse_iterator rend() const noexcept {
    return crend();
  }
  const_reverse_iterator crend() const noexcept {
    return const_reverse_iterator(--cbegin());
  }
  const_reverse_iterator rbegin() const noexcept {
    return crbegin();
  }
  void insert(const_iterator pos, const T& value) {
    Basenode* prev = pos.get_node()->prev;
    Basenode* next = pos.get_node();
    Node* new_node = std::allocator_traits<node_alloc>::allocate(alloc, 1);
    try {
      std::allocator_traits<node_alloc>::construct(alloc, new_node, value,
                                                   prev, next);
      if (prev != nullptr) prev->next = new_node;
      next->prev = new_node;
      ++_size;
    }
    catch(...) {
      std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
      throw;
    }
  }
  void erase(iterator pos) noexcept {
    erase(static_cast<const_iterator>(pos));
  }
  void erase(const_iterator pos) noexcept {
    Basenode* prev = pos.get_node()->prev;
    Basenode* next = pos.get_node()->next;
    if (prev != nullptr) {
      prev->next = next;
    }
    if (next != nullptr) {
      next->prev = prev;
    }
    Node* new_node = static_cast<Node*>(pos.get_node());
    std::allocator_traits<node_alloc>::destroy(alloc, new_node);
    std::allocator_traits<node_alloc>::deallocate(alloc, new_node, 1);
    --_size;
  }
};