#include <iostream>
#include <vector>

int main() {
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<int>> sp(n, std::vector<int>(m, 0));
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    --x;
    --y;
    sp[x][i] = 1;
    sp[y][i] = 1;
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      std::cout << sp[i][j] << ' ';
    }
    std::cout << std::endl;
  }
}