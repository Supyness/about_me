#include <iostream>
#include <vector>

int n;
std::vector<int> used;
std::vector<int> t_up;
std::vector<int> t_in;
size_t timer = 0;

void dfs(int v, std::vector<std::vector<int>>& graph) {
  used[v] = 1;
  t_in[v] = timer++;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (!used[to]) dfs(to, graph);
  }
  t_up[v] = timer++;
}

int main() {
  std::cin >> n;
  used.assign(n, 0);
  t_up.assign(n, 0);
  t_in.assign(n, 0);
  int root;
  std::vector<std::vector<int>> graph(n);
  for (int i = 0; i < n; ++i) {
    int num; std::cin >> num;
    if (num == 0) {
      root = i;
    }
    else {
      --num;
      graph[num].push_back(i);
    }
  }
  dfs(root, graph);
  int m; std::cin >> m;
  for (int i = 0; i < m; ++i) {
    int a, b; std::cin >> a >> b;
    --a;
    --b;
    std::cout << ((t_in[a] < t_in[b]) && (t_up[b] < t_up[a])) << std::endl;
  }
}