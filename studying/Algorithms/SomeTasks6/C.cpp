#include <iostream>
#include <vector>

std::vector<int> used;
std::vector<int> p;
bool is_circle = false;


void dfs(int v, std::vector<std::vector<int>>& graph) {
  if (is_circle) return;
  used[v] = 1;
  p.push_back(v);
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (used[to] == 1) {
      p.push_back(to);
      is_circle = true;
      return;
    }
    else dfs(to, graph);
    if (is_circle) return;
  }
  used[v] = 2;
  p.pop_back();
}

int main() {
  int n, m; std::cin >> n >> m;
  used.assign(n + 1, 0);
  std::vector<std::vector<int>> graph(n + 1);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    graph[x].push_back(y);
  }
  for (int i = 1; i < n + 1; ++i) {
    if (used[i] == 0) {
      dfs(i, graph);
      if (is_circle) break;
    }
  }
  if (is_circle) {
    std::cout << "YES" << std::endl;
    int to = p.back();
    int counter = p.size() - 2;
    while (p[counter] != to) counter--;
    while (counter != p.size() - 1) {
      std::cout << p[counter] << ' ';
      ++counter;
    }
  }
  else {
    std::cout << "NO" << std::endl;
  }
}