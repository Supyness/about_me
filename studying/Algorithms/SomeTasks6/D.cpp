#include <iostream>
#include <vector>

int n;
std::vector<int> used;
std::vector<int> top_sort;
bool is_circle = false;

void dfs(int v, std::vector<std::vector<int>>& graph) {
  used[v] = 1;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (used[to] == 1) {
      is_circle = true;
      return;
    }
    else if (used[to] == 0) dfs(to, graph);
    if (is_circle) return;
  }
  used[v] = 2;
  top_sort.push_back(v);
}

void topological_sort(std::vector<std::vector<int>>& graph, int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) dfs(i, graph);
  }
  std::reverse(top_sort.begin(), top_sort.end());
}

int main() {
  int n, m; std::cin >> n >> m;
  used.assign(n, 0);
  std::vector<std::vector<int>> graph(n);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    --x;
    --y;
    graph[x].push_back(y);
  }
  topological_sort(graph, n);
  if (is_circle) std::cout << -1;
  else {
    for (auto it: top_sort) std::cout << ++it << ' ';
  }
}