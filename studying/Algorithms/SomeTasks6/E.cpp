#include <iostream>
#include <vector>
#include <set>

std::vector<int> used;
std::vector<int> row;
bool is_circle = false;
std::vector<int> colours;
int counter = 0;

void dfs_order(int v, std::vector<std::vector<int>>& graph) {
  used[v] = 1;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (!used[to]) dfs_order(to, graph);
  }
  row.push_back(v);
}

void dfs_in_component(int v, std::vector<std::vector<int>>& graph) {
  colours[v] = counter;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (colours[to] == -1) dfs_in_component(to, graph);
  }
}

int main() {
  int n, m; std::cin >> n >> m;
  colours.assign(n, -1);
  std::vector<std::vector<int>> graph(n);
  std::vector<std::vector<int>> graph_T(n);
  std::set<std::pair<int, int>> S;
  int capacity = 0;
  used.assign(n, 0);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    --x;
    --y;
    graph[x].push_back(y);
    graph_T[y].push_back(x);
  }
  for (int i = 0; i < n; ++i) {
    if (!used[i]) dfs_order(i, graph);
  }
  for (int i = 0; i < n; ++i) {
    int ver = row[n - 1 - i];
    if (colours[ver] == -1) {
      dfs_in_component(ver, graph_T);
      ++counter;
    }
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < graph[i].size(); ++j) {
      int to = graph[i][j];
      if (colours[i] != colours[to]) S.insert({colours[i], colours[to]});
    }
  }
  std::cout << S.size();
}