#include <iostream>
#include <vector>

std::vector<int> used;
std::vector<int> components;
bool is_circle = false;
std::vector<int> all_components;
int counter = 0;

void dfs(int v, std::vector<std::vector<int>>& graph) {
  used[v] = 1;
  components.push_back(v);
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (!used[to]) dfs(to, graph);
  }
}

void count_components(std::vector<std::vector<int>>& graph, int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) {
      components.clear();
      dfs(i, graph);
      ++counter;
      for (int j = 0; j < components.size(); ++j) all_components[components[j]] = counter;
    }
  }
}

int main() {
  int n, m; std::cin >> n >> m;
  used.assign(n, 0);
  all_components.assign(n, 0);
  std::vector<std::vector<int>> graph(n);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    --x;
    --y;
    graph[x].push_back(y);
    graph[y].push_back(x);
  }
  count_components(graph, n);
  std::cout << counter;
  std::cout << std::endl;
  for (auto it : all_components) std::cout << it << ' ';
}