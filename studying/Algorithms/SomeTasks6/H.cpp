#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

std::vector<int> used;
std::vector<int> t_up;
std::vector<int> t_in;
std::vector<int> bridge;
size_t timer = 0;

void dfs(int v, std::vector<std::vector<std::pair<int, int>>>& graph, int p = -1) {
  used[v] = 1;
  t_in[v] = t_up[v] = timer++;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i].first;
    if (to == p) continue;
    if (used[to]) {
      t_up[v] = std::min(t_up[v], t_in[to]);
    }
    else {
      dfs(to, graph, v);
      t_up[v] = std::min(t_up[v], t_up[to]);
      if (t_up[to] > t_in[v]) bridge.push_back(graph[v][i].second);
    }
  }
}

void find_all_bridges(std::vector<std::vector<std::pair<int, int>>>& graph, int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) dfs(i, graph);
  }
}

int main() {
  int n, m; std::cin >> n >> m;
  std::vector<std::vector<std::pair<int, int>>> graph(n);
  t_up.assign(n, 0);
  t_in.assign(n, 0);
  used.assign(n, 0);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    --x;
    --y;
    graph[x].push_back({y, i + 1});
    graph[y].push_back({x, i + 1});
  }
  find_all_bridges(graph, n);
  std::cout << bridge.size() << std::endl;
  std::sort(bridge.begin(), bridge.end());
  for (auto it : bridge) std::cout << it << ' ';
}