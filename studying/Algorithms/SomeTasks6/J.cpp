#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

std::vector<int> ans;

void euler(int v, std::vector<std::set<int>>& graph) {
  while (!graph[v].empty()) {
    int u = *graph[v].begin();
    graph[v].erase(u);
    //graph[u].erase(v);
    euler(u, graph);
  }
  ans.push_back(v + 1);
}

int main() {
  int n, a; std::cin >> n >> a;
  --a;
  std::vector<std::set<int>> graph(n);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      int num; std::cin >> num;
      if (num == 0 && i != j) graph[i].insert(j);
    }
  }
  euler(a, graph);
  for (int i = ans.size() - 1; i > 0; --i) {
    std::cout << ans[i] << ' ' << ans[i - 1] << std::endl;
  }
}