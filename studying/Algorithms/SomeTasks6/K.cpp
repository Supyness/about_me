#include <iostream>
#include <vector>
#include <queue>
#pragma optimize( "", off )

std::vector<int> used;
std::vector<int> t_up;
std::vector<int> t_in;
size_t timer = 0;
std::vector<int> edges_used;
int count = 0;

struct Edge {
  bool is_tree = false;
  int where = 0;
  int number = 0;
  Edge(int second, int number) : where(second), number(number) {};
};

std::vector<std::vector<Edge*>> graph;
std::vector<int> roots;
std::vector<std::vector<int>> answer;

void dfs(int v, int p = -1) {
  used[v] = 1;
  t_in[v] = t_up[v] = timer++;
  for (auto i : graph[v]) {
    int to = i->where;
    if (to == p) continue;
    if (used[to]) {
      t_up[v] = std::min(t_up[v], t_in[to]);
    }
    else {
      i->is_tree = true;
      dfs(to, v);
      t_up[v] = std::min(t_up[v], t_up[to]);
    }
  }
}
void dfs1(int v, std::queue<std::pair<int, int>>& q, int comp) {
  used[v] = true;
  for (auto it : graph[v]) {
    int to = it->where;
    if (t_up[to] >= t_in[v] && !used[to] && it->is_tree) {
      q.push({to, ++count});
    }
    else if (t_up[to] < t_in[v] && it->is_tree && !used[to]) {
      q.push({to, comp});
    }
    else if (t_up[to] < t_in[v] && !edges_used[it->number]) {
      answer[comp].push_back(it->number);
      edges_used[it->number] = true;
    }
  }
}


void find_all_curpoints(int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) {
      dfs(i);
      roots.push_back(i);
    }
  }
}

void solute(int n) {
  for (int i = 0; i < roots.size(); ++i) {
    std::queue<std::pair<int, int>> Q;
    Q.push({roots[i], count});
    while (!Q.empty()) {
      dfs1(Q.front().first, Q, Q.front().second);
      Q.pop();
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  t_up.assign(n, 0);
  t_in.assign(n, 0);
  used.assign(n, 0);
  graph.resize(n);
  answer.resize(n, {});
  edges_used.assign(m, 0);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    graph[x].push_back(new Edge(y, i));
    graph[y].push_back(new Edge(x, i));
  }
  find_all_curpoints(n);
  used.assign(n, 0);
  solute(n);
  size_t counter = 0;
  for (auto it : answer) {
    if (it.size() != 0) {
      ++counter;
    }
  }
  std::cout << counter << std::endl;
  for (auto it : answer) {
    if (it.size() != 0) {
      std::cout << it.size() << ' ';
      for (auto itt: it) std::cout << itt << ' ';
      std::cout << std::endl;
    }
  }
}