#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
//#pragma optimize( "", off )

std::vector<std::vector<int>> graph;
std::vector<int> used, enter, ret, colors, painted;
int timee, n, m, maxColor;

struct pair_hash {
  template <class T1, class T2>
  std::size_t operator () (const std::pair<T1,T2> &p) const {
    auto h1 = std::hash<T1>{}(p.first);
    auto h2 = std::hash<T2>{}(p.second);
    return h1 ^ h2;
  }
};

std::unordered_map<std::pair<int, int>, std::vector<int>, pair_hash> numbersOfEdges;

std::pair<int, int> getEdge(int a, int b) {
  if (a > b) {
    int temp = a;
    a = b;
    b = temp;
  }
  return std::make_pair(a, b);
};

void setColor(int v, int to, int color) {
  std::vector<int> numbers = numbersOfEdges[getEdge(v, to)];
  for (int i = 0; i < numbers.size(); ++i) {
    colors[numbers[i]] = color;
  }
};

void dfs(int v, int p = -1) {
  used[v] = 1;
  enter[v] = ret[v] = timee++;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (to == p) continue;
    if (used[to]) {
      ret[v] = std::min(ret[v], enter[to]);
    } else {
      dfs(to, v);
      ret[v] = std::min(ret[v], ret[to]);
    }
  }
}

void paint(int v, int color, int p) {
  painted[v] = 1;
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (to == p) continue;
    if (!painted[to]) {
      if (ret[to] >= enter[v]) {
        int newColor = ++maxColor;
        setColor(v, to, newColor);
        paint(to, newColor, v);
      } else {
        setColor(v, to, color);
        paint(to, color, v);
      }
    } else if (enter[to] < enter[v]) {
      setColor(v, to, color);
    }
  }
}

void solve() {
  timee = 1;
  for (int i = 1; i <= n; ++i) {
    if (!used[i]) dfs(i);
  }
  maxColor = 0;
  for (int i = 1; i <= n; ++i) {
    if (!painted[i]) {
      paint(i, maxColor, -1);
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  std::cin >> n >> m;

  graph.resize(n + 1);
  used.resize(n + 1);
  painted.resize(n + 1);
  enter.resize(n + 1);
  ret.resize(n + 1);
  colors.resize(m + 1);

  for (int i = 1; i <= m; i++) {
    int beg, end;
    std::cin >> beg >> end;

    graph[beg].push_back(end);
    graph[end].push_back(beg);

    numbersOfEdges[getEdge(beg, end)].push_back(i);
  }

  solve();

  std::cout << maxColor << std::endl;
  std::vector<std::pair<int, int>> ans;
  for (int i = 1; i < colors.size(); ++i) {
    ans.push_back({colors[i], i - 1});
  }
  int i = 1;
  std::sort(ans.begin(), ans.end());
  std::vector<int> ans1 = {ans[0].second};
  while (i < ans.size()) {
    while (i < ans.size() && ans[i].first == ans[i - 1].first) {
      ans1.push_back(ans[i].second);
      ++i;
    }
    std::cout << ans1.size() << ' ';
    for (auto it : ans1) std::cout << it << ' ';
    std::cout << std::endl;
    if (i < ans.size()) {
      ans1 = {ans[i].second};
      ++i;
    }
    else {
      ans1 = {};
    }
  }
  if (!ans1.empty()) {
    std::cout << ans1.size() << ' ';
    for (auto it : ans1) std::cout << it << ' ';
  }
}