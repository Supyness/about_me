#include <iostream>
#include <vector>

std::vector<int> used;
std::vector<int> components;
std::vector<int> all_components;
int counter = 0;

void dfs(int v, std::vector<std::vector<int>>& graph) {
  used[v] = 1;
  components.push_back(v);
  for (int i = 0; i < graph[v].size(); ++i) {
    int to = graph[v][i];
    if (!used[to]) dfs(to, graph);
  }
}

void count_components(std::vector<std::vector<int>>& graph, int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) {
      components.clear();
      dfs(i, graph);
      ++counter;
      for (int j = 0; j < components.size(); ++j) all_components[components[j]] = counter;
    }
  }
}

int main() {
  int n, m; std::cin >> n >> m;
  std::vector<int> start_state(n);
  std::vector<std::vector<int>> pairs(n);
  for (int i = 0; i < n; ++i) {
    std::cin >> start_state[i];
  }
  used.assign(2 * m, 0);
  all_components.assign(2 * m, 0);
  std::vector<std::vector<int>> graph(2 * m);
  for (int i = 0; i < m; ++i) {
    int ver_num;
    std::cin >> ver_num;
    for (int j = 0; j < ver_num; ++j) {
      int a;
      std::cin >> a;
      --a;
      pairs[a].push_back(i);
    }
  }
  for (int i = 0; i < n; ++i) {
    if (start_state[i] == 1) {
      graph[pairs[i][0] * 2].push_back(pairs[i][1] * 2);
      graph[pairs[i][1] * 2].push_back(pairs[i][0] * 2);
      graph[(pairs[i][0] * 2) ^ 1].push_back((pairs[i][1] * 2) ^ 1);
      graph[(pairs[i][1] * 2) ^ 1].push_back((pairs[i][0] * 2) ^ 1);
    }
    else {
      graph[pairs[i][0] * 2].push_back((pairs[i][1] * 2) ^ 1);
      graph[pairs[i][1] * 2].push_back((pairs[i][0] * 2) ^ 1);
      graph[(pairs[i][0] * 2) ^ 1].push_back(pairs[i][1] * 2);
      graph[(pairs[i][1] * 2) ^ 1].push_back(pairs[i][0] * 2);
    }
  }
  bool flag = true;
  count_components(graph, 2 * m);
  for (int i = 0; i < 2 * m; i += 2) {
    if (all_components[i] == all_components[i + 1]) {
      std::cout << "NO";
      flag = false;
      break;
    }
  }
  if (flag) std::cout << "YES";
}