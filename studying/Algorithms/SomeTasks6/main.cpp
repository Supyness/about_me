#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <stack>
#pragma optimize( "", off )

std::vector<int> used;
std::vector<int> t_up;
std::vector<int> t_in;
size_t timer = 0;
int all_colors = 0;
std::vector<std::pair<int, int>> colors;
std::vector<std::set<std::pair<int, int>>> graph;
std::stack<int> S;

void dfs(int v, int p = -1) {
  used[v] = 1;
  t_in[v] = timer++;
  for (auto i : graph[v]) {
    int to = i.first;
    if (to == p) continue;
    if (used[to]) {
      t_up[v] = std::min(t_up[v], t_in[to]);
    }
    else {
      dfs(to, v);
      t_up[v] = std::min(t_up[v], t_up[to]);
    }
  }
}

/*void color(int v, int colour, int parent = -1) {
  used[v] = 1;
  for (auto i : graph[v]) {
    int to = i.first;
    if (to == parent) continue;
    if (!used[to]) {
      if (t_up[to] >= t_in[v]) {
        int new_color = ++all_colors;
        colors[i.second].first = new_color;
        color(to, new_color, v);
      }
      else {
        colors[i.second].first = colour;
        color(to, colour, v);
      }
    }
    else if (t_in[to] < t_in[v]) colors[i.second].first = colour;
  }
}*/

void color(int v, int parent = -1) {
  used[v] = 1;
  t_in[v] = t_up[v] = timer++;
  for (auto it : graph[v]) {
    int to = it.first;
    if (to == parent) {
      continue;
    }
    if (!used[to]) {
      S.push(it.second);
      color(to, v);
      if (t_up[to] >= t_in[v]) {
        int color = all_colors++;
        while (S.top() != it.second) {
          colors[S.top()].first = color;
          S.pop();
        }
        colors[it.second].first = color;
        S.pop();
      }
      if (t_up[to] < t_up[v]) t_up[v] = t_up[to];
    }
    else if (t_in[to] < t_in[v]) {
      S.push(it.second);
      if (t_in[to] < t_up[v]) t_up[v] = t_in[to];
    }
    else if (t_up[v] > t_in[to]) t_up[v] = t_up[to];
  }
}


void find_all_curpointss(int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) dfs(i);
  }
}

void solute(int n) {
  for (int i = 0; i < n; ++i) {
    if (!used[i]) {
      timer = 0;
      all_colors++;
      color(i);
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
  int n, m; std::cin >> n >> m;
  colors.resize(m);
  for (int i = 0; i < m; ++i) {
    colors[i] = {-1, i};
  }
  //std::vector<int> all_edges(m, 0);
  //std::set<std::pair<int, int>> edge_nums;
  t_up.assign(n, 0);
  t_in.assign(n, 0);
  used.assign(n, 0);
  graph.resize(n);
  for (int i = 0; i < m; ++i) {
    int x, y; std::cin >> x >> y;
    graph[x].insert({y, i});
    graph[y].insert({x, i});
  }
  find_all_curpointss( n);
  used.assign(n, 0);

}