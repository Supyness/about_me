#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::min;

vector<size_t> CalcLevel(size_t n) {
  vector<size_t> level; //level[1] = 0, level[2] = 1, level[3] = 1; level[4] = 2;
  for (size_t i = 2; i <= n; i++) {
    level[i] = level[i - 1] + ((i & (i - 1)) == 0 ? 1 : 0);
  }
  return level;
}

vector<vector<int>> CalcSparseTableArray(const vector<int>& array, ) {
  for (size_t i = 0; i < n; i++) {
    st[i][0] = a[i];
  }
  for (size_t k = 1; k <= max_k; ++k) {
    for (size_t i = 0; i < n; ++i) {
      size_t second_idx = i + (1 << (k - 1));
      if (second_idx < n) {
        st[i][k] = min(st[i][k - 1], st[second_idx][k - 1]);
      }
      else {
        st[i][k] = st[i][k - 1];
      }
    }
  }
}

int main() {
  size_t n;
  cin >> n;
  vector<int> a(n);
  for (int& x: a) cin >> x;

  vector<size_t> level = CalcLevel(n);
  size_t max_k = level[n] + 1;
  vector<vector<int>> st(n, vector<int>(level[n] + 1));

  size_t q; cin >> q;
  while (q --> 0) {
    size_t l, r;
    cin >> l >> r;
    --l;
    --r;
    size_t k = level[r - l + 1];
    int ans = min(st[l][k], st[r + 1 - (1 << k)][k]);
    std::cout << ans << '\n';
  }
}
