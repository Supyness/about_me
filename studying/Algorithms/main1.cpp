#include <iostream>
#include <vector>

#define ll long long

int main() {
  ll n, p, q;
  std::cin >> n >> p >> q;
  std::vector<ll> arr(n);
  ll max1 = 0;
  for (ll i = 0; i < n; i++) {
    ll el; std::cin >> el;
    max1 = std::max(el, max1);
    arr[i] = el;
  }
  if (p == q) {
    std::cout << max1 / p + (max1 % p == 0 ? 0 : 1);
  }
  else {
    ll l = 0;
    ll r = max1 / q + (max1 % q == 0 ? 0 : 1);
    while (r - l > 1) {
      bool flag = true;
      ll m = (r + l) / 2;
      ll k = 0;
      //std::cout << m << ' ' << r << ' ' << l << std::endl;
      for (ll i = 0; i < n; i++) {
        if (arr[i] - m * q > 0) {
          k += (arr[i] - m * q) / (p - q) + ((arr[i] - m * q) % (p - q) == 0 ? 0 : 1);
        }
        if (k > m) {
          flag = false;
          break;
        }
      }
      if (!flag) l = m;
      else r = m;
    }
    ll t = 0;
    for (ll i = 0; i < n; i++) {
      if (arr[i] - l * q > 0) {
        t += (arr[i] - l * q) / (p - q) + ((arr[i] - l * q) % (p - q) == 0 ? 0 : 1);
      }
    }
    if (t > l) std::cout << r;
    else std::cout << l;
  }
}