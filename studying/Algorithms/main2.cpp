#include <iostream>
#include <vector>

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);
  int n, m, l; std::cin >> n >> m >> l;
  std::vector<std::vector<int>> arr1(n), arr2(m);
  for (int i = 0; i < n; i++) {
    std::vector<int> arr_p(l);
    for (int j = 0; j < l; j++) std::cin >> arr_p[j];
    arr1[i] = arr_p;
  }
  for (int i = 0; i < m; i++) {
    std::vector<int> arr_p(l);
    for (int j = 0; j < l; j++) std::cin >> arr_p[j];
    arr2[i] = arr_p;
  }
  int q; std::cin >> q;
  for (int i = 0; i < q; i++) {
    int v1, v2; std::cin >> v1 >> v2;
    //std::cout << arr1[v1 - 1][l - 1] << ' ' << arr2[v2 - 1][l - 1] << ' ' << 111 << std::endl;
    if (l == 1) std::cout << 1 << std::endl;
    else if (arr1[v1 - 1][l - 1] <= arr2[v2 - 1][l - 1]) {
      std::cout << l << "\n";
    }
    else if (arr1[v1 - 1][0] - arr2[v2 - 1][0] >= 0) {
//      std::cout << 1 << ' ' << arr1[v1 - 1][0] << ' ' << arr2[v2 - 1][0] << std::endl;
        std::cout << 1 << "\n";
    }
    else {
      int lp = 0, r = l - 1;
      while (r - lp > 1) {
        int m = (r + lp) / 2;
        if (arr1[v1 - 1][m] - arr2[v2 - 1][m] > 0) {
          r = m;
        } else lp = m;
      }
      /*if (arr1[v1 - 1][r] > arr2[v2 - 1][lp]) {
        std::cout << lp + 1 << "\n";
      }
      else std::cout << r + 1 << "\n";*/
      //std::cout << r << ' ' << lp << std::endl;
      if (std::max(arr1[v1 - 1][lp], arr2[v2 - 1][lp]) <= std::max(arr1[v1 - 1][r], arr2[v2 - 1][r])) std::cout << lp + 1 << "\n";
      else if ((r == l - 1)) std::cout << r + 1 << std::endl;
      else if (abs(arr1[v1 - 1][r] - arr2[v2 - 1][r]) <= abs(arr1[v1 - 1][r + 1] - arr2[v2 - 1][r + 1]))std::cout << r + 1 << "\n";
      else std::cout << r + 2 << "\n";
    }
  }
}