#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <map>
#include <set>

using namespace std;

const int N = 2e5 + 10;
int n, s;
int ql[N], qr[N];
void read(){
  cin >> n >> s;
  for (int i = 1; i <= n; i++){
    cin >> ql[i] >> qr[i];
  }
}
long long check(int l, int r){
  int A = 0;
  int B = 0;
  long long sum = 0;
  vector <int> L;
  for (int i = 1; i <= n; i++){
    if (qr[i] < l){
      A++;
      sum += ql[i];
    }else if (ql[i] > r){
      B++;
      sum += ql[i];
    }else{
      L.push_back(ql[i]);
    }
  }
  sort(L.begin(), L.end());
  if (A <= n / 2 && B <= n / 2){
    for (int i = 0; i < n / 2 - A; i++){
      sum += L[i];
    }
    long long oc = max(0ll, s - sum);
    long long c = oc / ((n / 2 + 1 - B));
    if (c >= r) c = r;
    sum += c * 1ll * (n / 2 + 1 - B);
    if (sum <= s && c >= l && c <= r) return c;
  }
  return -1;
}
void solver(){
  vector <int> a;
  for (int i = 1; i <= n; i++){
    a.push_back(ql[i]);
    a.push_back(qr[i]);
  }
  sort(a.begin(), a.end());
  a.erase(unique(a.begin(), a.end()), a.end());
  long long ans = -1;
  int mid = a.size() / 2;
  for (int del = -1; del <= 1 ; del++){
    int i = mid + del;
    if (i >= 1 && i <= n)
      ans = max(ans, check(a[i], a[i]));
  }

  for (int del = -1; del <= 1 ; del++){
    int i = mid + del;
    if (i >= 1 && i < n && i < size(a) - 1)
      ans = max(ans, check(a[i], a[i + 1]));
  }
  cout << ans << endl;
}

void solve(){
  read();
  solver();
}

int main(){
  ios::sync_with_stdio(NULL), cin.tie(0), cout.tie(0);
  solve();
}