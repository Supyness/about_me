#include <iostream>
#include <vector>
#include <string>

#define ll long long

struct Node {
  ll val;
  Node* prev;
  size_t size = 0;
};
Node* top = nullptr;

void push(Node &stack, ll x) {
  stack.size++;
  Node* n = new Node();
  n -> val = x;
  n -> prev = top;
  top = n;
}

bool is_empty() {
  return top == nullptr;
}

ll back() {
  return top -> val;
}

ll pop(Node &stack) {
    stack.size--;
    Node* del = top;
    top = top -> prev;
    ll t = del->val;
    delete del;
    return t;
}

int main() {
  Node stack;
  std::string query;
  std::vector<std::string> arr;
  //std::cin >> query;
  while (query != "exit") {
    if (query == "push") {
      ll num; std::cin >> num;
      push(stack, num);
      std::cout << "ok" << std::endl;
    }
    else if (query == "pop") {
      if (!is_empty()) {
        ll last = pop(stack);
        std::cout << last << std::endl;
      } else std::cout << "error" << std::endl;;
    }
    else if (query == "back") {
      if (!is_empty()) {
        std::cout << back() << std::endl;
      } else std::cout << "error" << std::endl;
    }
    else if (query == "clear") {
      while (!is_empty()) {
        pop(stack);
      }
      std::cout << "ok" << std::endl;
    }
    else if (query == "size") {
      std::cout << stack.size << std::endl;
    }
    std::cin >> query;
  }
  while (!is_empty()) pop(stack);
  std::cout << "bye";
}